$(document).ready(function() {
    $('select#atmgrp').change(function(){
        $.ajax({
                type: 'POST',
                url: 'ajax_branches',
                contentType: 'application/x-www-form-urlencoded',
                data: {groupid: $('select#atmgrp').val()},
                dataType: 'json',
                
                success: function(msg){
                    if(msg)
                    {
                        var toAppend = '';
                         $.each(msg, function(index, element) {
                                toAppend+= '<option>' + element.name + '</option>';
                            }  
                         );
                         
                         $('#branchselect').append(toAppend);
                    }
                },
                error: function(e) {
                    console.log(e.message);
                }   
         });
    });
});