$(document).ready(function () {
    $('#atmfield').hide();
    $('#bankfield').hide();
});

function fetch_branch(val)
    {
       $.ajax({
                type: 'POST',
                url: 'ajax_branches',
                data: {groupid: val},
                success: function(response){
                    if(response)
                    {
                        document.getElementById("branchselect").innerHTML=response;
                    }
                }   
         });
    }
    function fetch_atm(val)
    {
       $.ajax({
                type: 'POST',
                url: 'ajax_atms',
                data: {branchid: val},
                success: function(atm_response){
                    if(atm_response)
                    {
                        document.getElementById("atmselect").innerHTML=atm_response;
                    }
                    else 
                    {
                        alert("Failed");
                    }
                }   
         });
    }


