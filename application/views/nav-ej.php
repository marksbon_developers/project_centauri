<body class="nav-md">
  <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">
          <div class="navbar nav_title" style="">
            <a href="<?= base_url() ?>Dashboard" class="site_title">
              <img src="<?php print base_url(); ?>resources/images/app_logo.jpg" style="border-radius:5px;"/> 
              <span><strong style="font-size: 28px;">&alpha;</strong> Centauri</span>
            </a>
          </div>
          <div class="clearfix"></div>
          <!-- menu prile quick info 
          <div class="profile">
            <div class="profile_pic">
              <img src="<?php print base_url(); ?>resources/images/logo.jpg" class="img-circle profile_img"/>
            </div>
            <div class="profile_info">
              <h2 style="font-size:larger;">Company Name</h2>
            </div>
          </div> -->
          <!-- /menu prile quick info -->
          <div class="clearfix"></div>
          <!-- sidebar menu -->
          <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
              <ul class="nav side-menu">
                <li><a href="../Dashboard/"><i class="fa fa-home"></i>Dashboard</a></li>
              </ul>
            </div>
            <?php if(isset($_SESSION['username']) && ($_SESSION['role'] == 'Administrator' || $_SESSION['role'] == 'GreenSunAdmin') ) { ?>
            <div class="menu_section">
              <h3 class="nav-headings">EJ Manager </h3>
              <ul class="nav side-menu">
                <li><a href="<?= base_url() ?>Dashboard/Transaction"><i class="glyphicon glyphicon-tasks"></i> Transactions</a></li>
                <li><a href="<?= base_url() ?>Dashboard/Logs"><i class="fa fa-cogs"></i> Logs</a></li>
                <li><a href="<?= base_url() ?>Report"><i class="fa fa-bar-chart-o"></i> Report</a></li>
              </ul>
            </div>
            <div class="menu_section">
              <h3 class="nav-headings">Reconciliation </h3>
              <ul class="nav side-menu">
                <li><a href="<?= base_url() ?>Reconciliation/Upload"><i class="fa fa-file"></i> File Upload</a></li>
                <li><a href="<?= base_url() ?>Reconciliation/D_Comparism"><i class="fa fa-th-large"></i> 2-Way Recon</a></li>
                <li><a href="<?= base_url() ?>Reconciliation/T_Comparism"><i class="fa fa-th"></i> 3-Way Recon</a></li>
              </ul>
            </div>
            <div class="menu_section">
              <h3 class="nav-headings">ATM Eye </h3>
              <ul class="nav side-menu">
                <li><a href="<?= base_url() ?>Dashboard/ATM_Eye"><i class="fa fa-file"></i> ATM Eye </a></li>
              </ul>
            </div>
            <?php } ?>
          </div>
          <!-- /sidebar menu -->
        </div>
      </div>

            <!-- top navigation -->
            <div class="top_nav">

                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                   <?php print $_SESSION['fullname']; ?>
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    <li>
                                        <a href="" data-toggle="modal" data-target=".changepwd">  Change Password</a>
                                    </li>
                                    <li><a href="<?= base_url() ?>Access/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>

            </div>
            <!-- /top navigation -->
