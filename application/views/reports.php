<?php require_once('nav.php'); ?>

<!-- page content -->
            <div class="right_col" role="main">
              <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div class="x_panel">
                    <div class="x_title">
                      <div class="row">
                        <div class="x_title">
                        <div class="row">
                          <h2><b>Summary Report</b></h2>
                          <a href="Download" class="btn btn-primary pull-right">Print PDF</a>
                          <a href="Generate_Report" class="btn btn-primary pull-right">Generate Report</a>
                        </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                          <div class="x_panel">
                             <div class="x_title">
                                <h2 style="font-weight: bold;">Transactions Details</h2>
                                <div class="clearfix"></div>
                             </div>
                             <div class="x_content">
                                <div class="row">
                                   <div class="col-xs-4">
                                      <span><h4><i class="fa fa-bullseye red"></i> Total</h4></span>
                                   </div>
                                   <div class="col-xs-2 more_info">
                                      <span><h4><?= sizeof(@$tot_no_trans);?></h4></span>
                                   </div>
                                   <div class="col-xs-2 more_info">
                                      <span><h4>100%</h4></span>
                                   </div>
                                   <div class="col-xs-4 more_info" style="margin-top: 10px;">
                                      <div class="progress progress_sm">
                                         <div class="progress-bar bg-blue" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                                            <span class="sr-only">100% Complete</span>
                                         </div>
                                      </div>
                                   </div>
                                </div>
                                <div class="row">
                                   <div class="col-xs-4">
                                      <span><h4><i class="fa fa-check-circle green"></i> Successful</h4></span>
                                   </div>
                                   <div class="col-xs-2 more_info">
                                      <span><h4><?= @$tot_no_suc;?></h4></span>
                                   </div>
                                   <div class="col-xs-2 more_info">
                                      <span><h4 style="color: #3498db;"><?= @$tot_no_suc_cent."%"; ?></h4></span>
                                   </div>
                                   <div class="col-xs-4 more_info" style="margin-top: 10px;">
                                      <div class="progress progress_sm">
                                         <div class="progress-bar bg-blue" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: <?= @$tot_no_suc_cent."%"; ?>">
                                            <span class="sr-only">100% Complete</span>
                                         </div>
                                      </div>
                                   </div>
                                </div>
                                <div class="row">
                                   <div class="col-xs-4">
                                      <span><h4><i class="fa fa-times-circle red"></i> UnSuccessful</h4></span>
                                   </div>
                                   <div class="col-xs-2 more_info">
                                      <span><h4><?= @$tot_no_unsuc;?></h4></span>
                                   </div>
                                   <div class="col-xs-2 more_info">
                                      <span><h4 style="color: #3498db;"><?= @$tot_no_unsuc_cent."%"; ?></h4></span>
                                   </div>
                                   <div class="col-xs-4 more_info" style="margin-top: 10px;">
                                      <div class="progress progress_sm">
                                         <div class="progress-bar bg-blue" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: <?= @$tot_no_unsuc_cent."%"; ?>">
                                            <span class="sr-only">100% Complete</span>
                                         </div>
                                      </div>
                                   </div>
                                </div>
                                <hr />
                                <div class="row">
                                   <div class="col-xs-4">
                                      <span><h4><i class="fa fa-circle blue"></i> Withdrawals</h4></span>
                                   </div>
                                   <div class="col-xs-2 more_info">
                                      <span><h4><?= @$tot_with;?></h4></span>
                                   </div>
                                   <div class="col-xs-2 more_info">
                                      <span><h4 style="color: #3498db;"><?= @$tot_with_cent."%"; ?></h4></span>
                                   </div>
                                   <div class="col-xs-4 more_info" style="margin-top: 10px;">
                                      <div class="progress progress_sm">
                                         <div class="progress-bar bg-blue" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: <?= @$tot_with_cent."%"; ?>">
                                            <span class="sr-only">100% Complete</span>
                                         </div>
                                      </div>
                                   </div>
                                </div>
                                <div class="row">
                                   <div class="col-xs-4">
                                      <span><h4><i class="fa fa-square purple"></i> Enquiries</h4></span>
                                   </div>
                                   <div class="col-xs-2 more_info">
                                      <span><h4><?= @$tot_inq;?></h4></span>
                                   </div>
                                   <div class="col-xs-2 more_info">
                                      <span><h4 style="color: #3498db;"><?= @$tot_inq_cent."%"; ?></h4></span>
                                   </div>
                                   <div class="col-xs-4 more_info" style="margin-top: 10px;">
                                      <div class="progress progress_sm">
                                         <div class="progress-bar bg-blue" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: <?= @$tot_inq_cent."%"; ?>">
                                            <span class="sr-only">100% Complete</span>
                                         </div>
                                      </div>
                                   </div>
                                </div>
                                <div class="row">
                                   <div class="col-xs-4">
                                      <span><h4><i class="fa fa-exchange color1"></i> Transfer</h4></span>
                                   </div>
                                   <div class="col-xs-2 more_info">
                                      <span><h4><?= @$tot_trans;?></h4></span>
                                   </div>
                                   <div class="col-xs-2 more_info">
                                      <span><h4 style="color: #3498db;"><?= @$tot_trans_cent."%"; ?></h4></span>
                                   </div>
                                   <div class="col-xs-4 more_info" style="margin-top: 10px;">
                                      <div class="progress progress_sm">
                                         <div class="progress-bar bg-blue" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: <?= @$tot_trans_cent."%"; ?>">
                                            <span class="sr-only">100% Complete</span>
                                         </div>
                                      </div>
                                   </div>
                                </div>
                                <div class="row">
                                   <div class="col-xs-4">
                                      <span><h4><i class="fa fa-tasks green"></i> Mini Statement</h4></span>
                                   </div>
                                   <div class="col-xs-2 more_info">
                                      <span><h4><?= @$tot_statmt;?></h4></span>
                                   </div>
                                   <div class="col-xs-2 more_info">
                                      <span><h4 style="color: #3498db;"><?= @$tot_statmt_cent."%"; ?></h4></span>
                                   </div>
                                   <div class="col-xs-4 more_info" style="margin-top: 10px;">
                                      <div class="progress progress_sm">
                                         <div class="progress-bar bg-blue" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: <?= @$tot_statmt_cent."%"; ?>">
                                            <span class="sr-only">100% Complete</span>
                                         </div>
                                      </div>
                                   </div>
                                </div>
                                <div class="row">
                                   <div class="col-xs-4">
                                      <span><h4><i class="fa fa-times red"></i> Incomplete</h4></span>
                                   </div>
                                   <div class="col-xs-2 more_info">
                                      <span><h4><?= @$tot_inc;?></h4></span>
                                   </div>
                                   <div class="col-xs-2 more_info">
                                      <span><h4 style="color: #3498db;"><?= @$tot_inc_cent."%";?></h4></span>
                                   </div>
                                   <div class="col-xs-4 more_info" style="margin-top: 10px;">
                                      <div class="progress progress_sm">
                                         <div class="progress-bar bg-blue" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: <?= @$tot_inc_cent."%"; ?>">
                                            <span class="sr-only">100% Complete</span>
                                         </div>
                                      </div>
                                   </div>
                                </div>
                             </div>
                          </div>
                        </div>
                        
                       <div class="col-md-6 col-sm-12 col-xs-12">
                          <div class="x_panel">
                             <div class="x_title">
                                <h2 style="font-weight: bold;">Pie Graph</h2>
                                <div class="clearfix"></div>
                             </div>
                             <div class="x_content">
                                <div id="echart_pie" style="height: 345px; cursor: default; background-color: rgba(0, 0, 0, 0);font-size:11px !important" _echarts_instance_="1471564726818"><div style="position: relative; overflow: hidden; width: 272px; height: 350px;"><div width="272" height="350" data-zr-dom-id="bg" style="position: absolute; left: 0px; top: 0px; width: 272px; height: 350px; -webkit-user-select: none;"></div><canvas width="272" height="350" data-zr-dom-id="1" style="position: absolute; left: 0px; top: 0px; width: 272px; height: 350px; -webkit-user-select: none;"></canvas><canvas width="272" height="350" data-zr-dom-id="2" style="position: absolute; left: 0px; top: 0px; width: 272px; height: 350px; -webkit-user-select: none;"></canvas><canvas width="272" height="350" data-zr-dom-id="3" style="position: absolute; left: 0px; top: 0px; width: 272px; height: 350px; -webkit-user-select: none;"></canvas><canvas width="272" height="350" data-zr-dom-id="4" style="position: absolute; left: 0px; top: 0px; width: 272px; height: 350px; -webkit-user-select: none;"></canvas><canvas width="272" height="350" data-zr-dom-id="6" style="position: absolute; left: 0px; top: 0px; width: 272px; height: 350px; -webkit-user-select: none;"></canvas><canvas width="272" height="350" data-zr-dom-id="8" style="position: absolute; left: 0px; top: 0px; width: 272px; height: 350px; -webkit-user-select: none;"></canvas><canvas width="272" height="350" data-zr-dom-id="_zrender_hover_" style="position: absolute; left: 0px; top: 0px; width: 350px; height: 350px; -webkit-user-select: none;"></canvas></div></div>
                             </div>
                          </div>
                       </div>
                </div>
                <div class="clearfix"></div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="x_panel" style="height: 273px;">
                      <div class="x_title">
                        <h2 style="font-weight: bold;"><i class="fa fa-check-circle green"></i> Successful Transact.</h2>
                        <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
                        <div class="row">
                          <div class="col-xs-4">
                            <span><h4><i class="fa fa-circle blue"></i> Withdrawals</h4></span>
                          </div>
                          <div class="col-xs-2 more_info">
                            <span><h4><?= @$tot_suc_with;?></h4></span>
                          </div>
                          <div class="col-xs-2 more_info">
                             <span><h4 style="color: #26b99a;"><?= @$tot_succ_with_cent."%";?></h4></span>
                          </div>
                          <div class="col-xs-4 more_info" style="margin-top: 10px;">
                             <div class="progress progress_sm">
                                <div class="progress-bar bg-green" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: <?= @$tot_succ_with_cent."%"; ?>">
                                   <span class="sr-only">100% Complete</span>
                                </div>
                             </div>
                          </div>
                        </div>
                        <div class="row">
                           <div class="col-xs-4">
                              <span><h4><i class="fa fa-square purple"></i> Enquiries</h4></span>
                           </div>
                           <div class="col-xs-2 more_info">
                              <span><h4><?= @$tot_suc_inq;?></h4></span>
                           </div>
                           <div class="col-xs-2 more_info">
                             <span><h4 style="color: #26b99a;"><?= @$tot_succ_inq_cent."%";?></h4></span>
                          </div>
                          <div class="col-xs-4 more_info" style="margin-top: 10px;">
                             <div class="progress progress_sm">
                                <div class="progress-bar bg-green" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: <?= @$tot_succ_inq_cent."%"; ?>">
                                   <span class="sr-only">100% Complete</span>
                                </div>
                             </div>
                          </div>
                        </div>
                        <div class="row">
                           <div class="col-xs-4">
                              <span><h4><i class="fa fa-exchange color1"></i> Transfer</h4></span>
                           </div>
                           <div class="col-xs-2 more_info">
                              <span><h4><?= @$tot_suc_trans;?></h4></span>
                           </div>
                           <div class="col-xs-2 more_info">
                             <span><h4 style="color: #26b99a;"><?= @$tot_succ_trans_cent."%";?></h4></span>
                          </div>
                          <div class="col-xs-4 more_info" style="margin-top: 10px;">
                             <div class="progress progress_sm">
                                <div class="progress-bar bg-green" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: <?= @$tot_succ_trans_cent."%"; ?>">
                                   <span class="sr-only">100% Complete</span>
                                </div>
                             </div>
                          </div>
                        </div>
                        <div class="row">
                           <div class="col-xs-4">
                              <span><h4><i class="fa fa-tasks green"></i> Mini Statement</h4></span>
                           </div>
                           <div class="col-xs-2 more_info">
                              <span><h4><?= @$tot_suc_statmt;?></h4></span>
                           </div>
                           <div class="col-xs-2 more_info">
                             <span><h4 style="color: #26b99a;"><?= @$tot_succ_statmt_cent."%";?></h4></span>
                          </div>
                          <div class="col-xs-4 more_info" style="margin-top: 10px;">
                             <div class="progress progress_sm">
                                <div class="progress-bar bg-green" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: <?= @$tot_succ_statmt_cent."%"; ?>">
                                   <span class="sr-only">100% Complete</span>
                                </div>
                             </div>
                          </div>
                        </div>
                             </div>
                          </div>
                  </div>
                  <!-- Unsuccessful Transactions -->
                  <div class="col-md-6">
                    <div class="x_panel">
                      <div class="x_title">
                        <h2 style="font-weight: bold;"><i class="fa fa-times-circle red"></i> UnSuccessful Transact.</h2>
                        <div class="clearfix"></div>
                      </div>
                      <div class="x_content">
                        <div class="row">
                           <div class="col-xs-4">
                              <span><h4><i class="fa fa-circle blue"></i> Withdrawals</h4></span>
                           </div>
                           <div class="col-xs-2 more_info">
                              <span><h4><?= @$tot_unsuc_with;?></h4></span>
                           </div>
                           <div class="col-xs-2 more_info">
                             <span><h4 style="color: red;"><?= @$tot_unsucc_with_cent."%";?></h4></span>
                          </div>
                          <div class="col-xs-4 more_info" style="margin-top: 10px;">
                             <div class="progress progress_sm">
                                <div class="progress-bar bg-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: <?= @$tot_unsucc_with_cent."%"; ?>">
                                   <span class="sr-only">100% Complete</span>
                                </div>
                             </div>
                          </div>
                        </div>
                        <div class="row">
                           <div class="col-xs-4">
                              <span><h4><i class="fa fa-square purple"></i> Enquiries</h4></span>
                           </div>
                           <div class="col-xs-2 more_info">
                              <span><h4><?= @$tot_unsuc_inq;?></h4></span>
                           </div>
                           <div class="col-xs-2 more_info">
                             <span><h4 style="color: red;"><?= @$tot_unsucc_inq_cent."%";?></h4></span>
                          </div>
                          <div class="col-xs-4 more_info" style="margin-top: 10px;">
                             <div class="progress progress_sm">
                                <div class="progress-bar bg-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: <?= @$tot_unsucc_inq_cent."%"; ?>">
                                   <span class="sr-only">100% Complete</span>
                                </div>
                             </div>
                          </div>
                        </div>
                        <div class="row">
                           <div class="col-xs-4">
                              <span><h4><i class="fa fa-exchange color1"></i> Transfer</h4></span>
                           </div>
                           <div class="col-xs-2 more_info">
                              <span><h4><?= @$tot_unsuc_trans;?></h4></span>
                           </div>
                           <div class="col-xs-2 more_info">
                             <span><h4 style="color: red;"><?= @$tot_unsucc_trans_cent."%";?></h4></span>
                          </div>
                          <div class="col-xs-4 more_info" style="margin-top: 10px;">
                             <div class="progress progress_sm">
                                <div class="progress-bar bg-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: <?= @$tot_unsucc_trans_cent."%"; ?>">
                                   <span class="sr-only">100% Complete</span>
                                </div>
                             </div>
                          </div>
                        </div>
                        <div class="row">
                           <div class="col-xs-4">
                              <span><h4><i class="fa fa-tasks green"></i> Mini Statement</h4></span>
                           </div>
                           <div class="col-xs-2 more_info">
                              <span><h4><?= @$tot_unsuc_statmt;?></h4></span>
                           </div>
                           
                           <div class="col-xs-2 more_info">
                             <span><h4 style="color: red;"><?= @$tot_unsucc_statmt_cent."%";?></h4></span>
                          </div>
                          <div class="col-xs-4 more_info" style="margin-top: 10px;">
                             <div class="progress progress_sm">
                                <div class="progress-bar bg-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: <?= @$tot_unsucc_statmt_cent."%"; ?>">
                                   <span class="sr-only">100% Complete</span>
                                </div>
                             </div>
                          </div>
                        </div>
                        <div class="row">
                           <div class="col-xs-4">
                              <span><h4><i class="fa fa-times red"></i> Incomplete</h4></span>
                           </div>
                           <div class="col-xs-2 more_info">
                              <span><h4><?= @$tot_unsuc_inc;?></h4></span>
                           </div>
                           <div class="col-xs-2 more_info">
                             <span><h4 style="color: red;"><?= @$tot_unsucc_inc_cent."%";?></h4></span>
                          </div>
                          <div class="col-xs-4 more_info" style="margin-top: 10px;">
                             <div class="progress progress_sm">
                                <div class="progress-bar bg-red" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: <?= @$tot_unsucc_inc_cent."%"; ?>">
                                   <span class="sr-only">100% Complete</span>
                                </div>
                             </div>
                          </div>
                        </div>
                             </div>
                          </div>
                  </div>
                </div>
            </div>
                        <div class="x_content">
                            <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2 style="font-weight: bold;">Successful Transactions - Regionals</h2>
                                <div class="clearfix"></div>
                             </div>
                            <div class="x_content">
                                <table class="table table-striped responsive-utilities jambo_table" style="font-size: 14px;">
                        <thead>
                            <th> Regions</th>
                            <th style="color: aqua;">Successful</th>
                            <th style="color: aqua;">Withdrawals</th>
                            <th style="color: aqua;">Enquires</th>
                        </thead>
                        <tbody style="text-align: center;font-size: 12px;">
                            <!-- Greater Accra Region -->
                            <?php
                                if(!empty($total_ga))
                                {
                            ?>
                            <tr>
                            <td>Accra</td>
                            <td><?php if(!empty($total_ga_suc)){print $total_ga_suc;} else {print "0";};?></td>
                            <td><?php if(!empty($total_ga_suc_with)){print $total_ga_suc_with;} else {print "0";};?></td>
                            <td><?php if(!empty($total_ga_suc_inq)){print $total_ga_suc_inq;} else {print "0";};?></td>
                            </tr>
                            <?php
                                }
                            ?>
                            <!-- Greater Accra Region -->
                            <!-- Tema Region -->
                            <?php
                                if(!empty($total_tm))
                                {
                            ?>
                            <tr>
                            <td>Tema</td>
                            <td><?php if(!empty($total_tm_suc)){print $total_tm_suc;} else {print "0";};?></td>
                            <td><?php if(!empty($total_tm_suc_with)){print $total_tm_suc_with;} else {print "0";};?></td>
                            <td><?php if(!empty($total_tm_suc_inq)){print $total_tm_suc_inq;} else {print "0";};?></td>
                            </tr>
                            <?php
                                }
                            ?>
                            <!-- Tema Region -->
                            <!-- Eastern Region -->
                            <?php
                                if(!empty($total_er))
                                {
                            ?>
                            <tr>
                            <td>Eastern</td>
                            <td><?php if(!empty($total_er_suc)){print $total_er_suc;} else {print "0";};?></td>
                            <td><?php if(!empty($total_er_suc_with)){print $total_er_suc_with;} else {print "0";};?></td>
                            <td><?php if(!empty($total_er_suc_inq)){print $total_er_suc_inq;} else {print "0";};?></td>
                            </tr>
                            <?php
                                }
                            ?>
                            <!-- Eastern Region -->
                            <!-- Western Region -->
                            <?php
                                if(!empty($total_wr))
                                {
                            ?>
                            <tr>
                            <td>Western</td>
                            <td><?php if(!empty($total_wr_suc)){print $total_wr_suc;} else {print "0";};?></td>
                            <td><?php if(!empty($total_wr_suc_with)){print $total_wr_suc_with;} else {print "0";};?></td>
                            <td><?php if(!empty($total_wr_suc_inq)){print $total_wr_suc_inq;} else {print "0";};?></td>
                            </tr>
                            <?php
                                }
                            ?>
                            <!-- Western Region -->
                            <!-- Central Region -->
                            <?php
                                if(!empty($total_cr))
                                {
                            ?>
                            <tr>
                            <td>Central</td>
                            <td><?php if(!empty($total_cr_suc)){print $total_cr_suc;} else {print "0";};?></td>
                            <td><?php if(!empty($total_cr_suc_with)){print $total_cr_suc_with;} else {print "0";};?></td>
                            <td><?php if(!empty($total_cr_suc_inq)){print $total_cr_suc_inq;} else {print "0";};?></td>
                            </tr>
                            <?php
                                }
                            ?>
                            <!-- Central Region -->
                            <!-- Northern Region -->
                            <?php
                                if(!empty($total_nr))
                                {
                            ?>
                            <tr>
                            <td>Northern</td>
                            <td><?php if(!empty($total_nr_suc)){print $total_nr_suc;} else {print "0";};?></td>
                            <td><?php if(!empty($total_nr_suc_with)){print $total_nr_suc_with;} else {print "0";};?></td>
                            <td><?php if(!empty($total_nr_suc_inq)){print $total_nr_suc_inq;} else {print "0";};?></td>
                            </tr>
                            <?php
                                }
                            ?>
                            <!-- Northern Region -->
                            <!-- Volta Region -->
                            <?php
                                if(!empty($total_vr))
                                {
                            ?>
                            <tr>
                            <td>Volta</td>
                            <td><?php if(!empty($total_vr_suc)){print $total_vr_suc;} else {print "0";};?></td>
                            <td><?php if(!empty($total_vr_suc_with)){print $total_vr_suc_with;} else {print "0";};?></td>
                            <td><?php if(!empty($total_vr_suc_inq)){print $total_vr_suc_inq;} else {print "0";};?></td>
                            </tr>
                            <?php
                                }
                            ?>
                            <!-- Volta Region -->
                            <!-- Ashanti Region -->
                            <?php
                                if(!empty($total_ash))
                                {
                            ?>
                            <tr>
                            <td>Ashanti</td>
                            <td><?php if(!empty($total_ash_suc)){print $total_ash_suc;} else {print "0";};?></td>
                            <td><?php if(!empty($total_ash_suc_with)){print $total_ash_suc_with;} else {print "0";};?></td>
                            <td><?php if(!empty($total_ash_suc_inq)){print $total_ash_suc_inq;} else {print "0";};?></td>
                            </tr>
                            <?php
                                }
                            ?>
                            <!-- Ashanti Region -->
                            <!-- Brong Ahafo Region -->
                            <?php
                                if(!empty($total_ba))
                                {
                            ?>
                            <tr>
                            <td>Brong Ahafo</td>
                            <td><?php if(!empty($total_ba_suc)){print $total_ba_suc;} else {print "0";};?></td>
                            <td><?php if(!empty($total_ba_suc_with)){print $total_ba_suc_with;} else {print "0";};?></td>
                            <td><?php if(!empty($total_ba_suc_inq)){print $total_ba_suc_inq;} else {print "0";};?></td>
                            </tr>
                            <?php
                                }
                            ?>
                            <!-- Brong Ahafo Region -->
                            <!-- Upper East Region -->
                            <?php
                                if(!empty($total_ue))
                                {
                            ?>
                            <tr>
                            <td>Upper East</td>
                            <td><?php if(!empty($total_ue_suc)){print $total_ue_suc;} else {print "0";};?></td>
                            <td><?php if(!empty($total_ue_suc_with)){print $total_ue_suc_with;} else {print "0";};?></td>
                            <td><?php if(!empty($total_ue_suc_inq)){print $total_ue_suc_inq;} else {print "0";};?></td>
                            </tr>
                            <?php
                                }
                            ?>
                            <!-- Upper East Region -->
                            <!-- Upper West Region -->
                            <?php
                                if(!empty($total_uw))
                                {
                            ?>
                            <tr>
                            <td>Upper West</td>
                            <td><?php if(!empty($total_uw_suc)){print $total_uw_suc;} else {print "0";};?></td>
                            <td><?php if(!empty($total_uw_suc_with)){print $total_uw_suc_with;} else {print "0";};?></td>
                            <td><?php if(!empty($total_uw_suc_inq)){print $total_uw_suc_inq;} else {print "0";};?></td>
                            </tr>
                            <?php
                                }
                            ?>
                            <!-- Upper West Region -->
                        </tbody>
                    </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2 style="font-weight: bold;">UnSuccessful Transactions - Regionals</h2>
                                <div class="clearfix"></div>
                             </div>
                            <div class="x_content">
                                <table class="table table-striped responsive-utilities jambo_table" style="font-size: 14px;">
                        <thead>
                            <th> Regions</th>
                            <th style="color: red;">UnSuccessful</th>
                            <th style="color: red;">Withdrawals</th>
                            <th style="color: red;">Enquires</th>
                            <th style="color: red;">Incomplete</th>
                        </thead>
                        <tbody style="text-align: center;font-size: 12px;">
                            <!-- Greater Accra Region -->
                            <?php
                                if(!empty($total_ga))
                                {
                            ?>
                            <tr>
                            <td>Accra</td>
                            <td><?php if(!empty($total_ga_unsuc)){print $total_ga_unsuc;} else {print "0";};?></td>
                            <td style="color: red;"><?php if(!empty($total_ga_unsuc_with)){print $total_ga_unsuc_with;} else {print "0";};?></td>
                            <td style="color: red;"><?php if(!empty($total_ga_unsuc_inq)){print $total_ga_unsuc_inq;} else {print "0";};?></td>
                            <td style="color: red;"><?php if(!empty($total_ga_unsuc_inc)){print $total_ga_unsuc_inc;} else {print "0";};?></td>
                            </tr>
                            <?php
                                }
                            ?>
                            <!-- Greater Accra Region -->
                            <!-- Tema Region -->
                            <?php
                                if(!empty($total_tm))
                                {
                            ?>
                            <tr>
                            <td>Tema</td>
                            <td><?php if(!empty($total_tm_unsuc)){print $total_tm_unsuc;} else {print "0";};?></td>
                            <td style="color: red;"><?php if(!empty($total_tm_unsuc_with)){print $total_tm_unsuc_with;} else {print "0";};?></td>
                            <td style="color: red;"><?php if(!empty($total_tm_unsuc_inq)){print $total_tm_unsuc_inq;} else {print "0";};?></td>
                            <td style="color: red;"><?php if(!empty($total_tm_unsuc_inc)){print $total_tm_unsuc_inc;} else {print "0";};?></td>
                            </tr>
                            <?php
                                }
                            ?>
                            <!-- Tema Region -->
                            <!-- Eastern Region -->
                            <?php
                                if(!empty($total_er))
                                {
                            ?>
                            <tr>
                            <td>Eastern</td>
                            <td><?php if(!empty($total_er_unsuc)){print $total_er_unsuc;} else {print "0";};?></td>
                            <td style="color: red;"><?php if(!empty($total_er_unsuc_with)){print $total_er_unsuc_with;} else {print "0";};?></td>
                            <td style="color: red;"><?php if(!empty($total_er_unsuc_inq)){print $total_er_unsuc_inq;} else {print "0";};?></td>
                            <td style="color: red;"><?php if(!empty($total_er_unsuc_inc)){print $total_er_unsuc_inc;} else {print "0";};?></td>
                            </tr>
                            <?php
                                }
                            ?>
                            <!-- Eastern Region -->
                            <!-- Western Region -->
                            <?php
                                if(!empty($total_wr))
                                {
                            ?>
                            <tr>
                            <td>Western</td>
                            <td><?php if(!empty($total_wr_unsuc)){print $total_wr_unsuc;} else {print "0";};?></td>
                            <td style="color: red;"><?php if(!empty($total_wr_unsuc_with)){print $total_wr_unsuc_with;} else {print "0";};?></td>
                            <td style="color: red;"><?php if(!empty($total_wr_unsuc_inq)){print $total_wr_unsuc_inq;} else {print "0";};?></td>
                            <td style="color: red;"><?php if(!empty($total_wr_unsuc_inc)){print $total_wr_unsuc_inc;} else {print "0";};?></td>
                            </tr>
                            <?php
                                }
                            ?>
                            <!-- Western Region -->
                            <!-- Central Region -->
                            <?php
                                if(!empty($total_cr))
                                {
                            ?>
                            <tr>
                            <td>Central</td>
                            <td><?php if(!empty($total_cr_unsuc)){print $total_cr_unsuc;} else {print "0";};?></td>
                            <td style="color: red;"><?php if(!empty($total_cr_unsuc_with)){print $total_cr_unsuc_with;} else {print "0";};?></td>
                            <td style="color: red;"><?php if(!empty($total_cr_unsuc_inq)){print $total_cr_unsuc_inq;} else {print "0";};?></td>
                            <td style="color: red;"><?php if(!empty($total_cr_unsuc_inc)){print $total_cr_unsuc_inc;} else {print "0";};?></td>
                            </tr>
                            <?php
                                }
                            ?>
                            <!-- Central Region -->
                            <!-- Northern Region -->
                            <?php
                                if(!empty($total_nr))
                                {
                            ?>
                            <tr>
                            <td>Northern</td>
                            <td><?php if(!empty($total_nr_unsuc)){print $total_nr_unsuc;} else {print "0";};?></td>
                            <td style="color: red;"><?php if(!empty($total_nr_unsuc_with)){print $total_nr_unsuc_with;} else {print "0";};?></td>
                            <td style="color: red;"><?php if(!empty($total_nr_unsuc_inq)){print $total_nr_unsuc_inq;} else {print "0";};?></td>
                            <td style="color: red;"><?php if(!empty($total_nr_unsuc_inc)){print $total_nr_unsuc_inc;} else {print "0";};?></td>
                            </tr>
                            <?php
                                }
                            ?>
                            <!-- Northern Region -->
                            <!-- Volta Region -->
                            <?php
                                if(!empty($total_vr))
                                {
                            ?>
                            <tr>
                            <td>Volta</td>
                            <td><?php if(!empty($total_vr_unsuc)){print $total_vr_unsuc;} else {print "0";};?></td>
                            <td style="color: red;"><?php if(!empty($total_vr_unsuc_with)){print $total_vr_unsuc_with;} else {print "0";};?></td>
                            <td style="color: red;"><?php if(!empty($total_vr_unsuc_inq)){print $total_vr_unsuc_inq;} else {print "0";};?></td>
                            <td style="color: red;"><?php if(!empty($total_vr_unsuc_inc)){print $total_vr_unsuc_inc;} else {print "0";};?></td>
                            </tr>
                            <?php
                                }
                            ?>
                            <!-- Volta Region -->
                            <!-- Ashanti Region -->
                            <?php
                                if(!empty($total_ash))
                                {
                            ?>
                            <tr>
                            <td>Ashanti</td>
                            <td><?php if(!empty($total_ash_unsuc)){print $total_ash_unsuc;} else {print "0";};?></td>
                            <td style="color: red;"><?php if(!empty($total_ash_unsuc_with)){print $total_ash_unsuc_with;} else {print "0";};?></td>
                            <td style="color: red;"><?php if(!empty($total_ash_unsuc_inq)){print $total_ash_unsuc_inq;} else {print "0";};?></td>
                            <td style="color: red;"><?php if(!empty($total_ash_unsuc_inc)){print $total_ash_unsuc_inc;} else {print "0";};?></td>
                            </tr>
                            <?php
                                }
                            ?>
                            <!-- Ashanti Region -->
                            <!-- Brong Ahafo Region -->
                            <?php
                                if(!empty($total_ba))
                                {
                            ?>
                            <tr>
                            <td>Brong Ahafo</td>
                            <td><?php if(!empty($total_ba_unsuc)){print $total_ba_unsuc;} else {print "0";};?></td>
                            <td style="color: red;"><?php if(!empty($total_ba_unsuc_with)){print $total_ba_unsuc_with;} else {print "0";};?></td>
                            <td style="color: red;"><?php if(!empty($total_ba_unsuc_inq)){print $total_ba_unsuc_inq;} else {print "0";};?></td>
                            <td style="color: red;"><?php if(!empty($total_ba_unsuc_inc)){print $total_ba_unsuc_inc;} else {print "0";};?></td>
                            </tr>
                            <?php
                                }
                            ?>
                            <!-- Brong Ahafo Region -->
                            <!-- Upper East Region -->
                            <?php
                                if(!empty($total_ue))
                                {
                            ?>
                            <tr>
                            <td>Upper East</td>
                            <td><?php if(!empty($total_ue_unsuc)){print $total_ue_unsuc;} else {print "0";};?></td>
                            <td style="color: red;"><?php if(!empty($total_ue_unsuc_with)){print $total_ue_unsuc_with;} else {print "0";};?></td>
                            <td style="color: red;"><?php if(!empty($total_ue_unsuc_inq)){print $total_ue_unsuc_inq;} else {print "0";};?></td>
                            <td style="color: red;"><?php if(!empty($total_ue_unsuc_inc)){print $total_ue_unsuc_inc;} else {print "0";};?></td>
                            </tr>
                            <?php
                                }
                            ?>
                            <!-- Upper East Region -->
                            <!-- Upper West Region -->
                            <?php
                                if(!empty($total_uw))
                                {
                            ?>
                            <tr>
                            <td>Upper East</td>
                            <td><?php if(!empty($total_uw_unsuc)){print $total_uw_unsuc;} else {print "0";};?></td>
                            <td style="color: red;"><?php if(!empty($total_uw_unsuc_with)){print $total_uw_unsuc_with;} else {print "0";};?></td>
                            <td style="color: red;"><?php if(!empty($total_uw_unsuc_inq)){print $total_uw_unsuc_inq;} else {print "0";};?></td>
                            <td style="color: red;"><?php if(!empty($total_uw_unsuc_inc)){print $total_uw_unsuc_inc;} else {print "0";};?></td>
                            </tr>
                            <?php
                                }
                            ?>
                            <!-- Upper East Region -->
                        </tbody>
                    </table>
                            </div>
                        </div>
                    </div>
                </div>
                                    
                                    
                        </div>
                    </div>
                </div>
            </div>
                    
                
                <!-- footer content -->
