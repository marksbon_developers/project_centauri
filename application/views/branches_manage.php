<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h3>Manage Branches</h3>
            <div class="clearfix"></div>
            <form class="form-horizontal form-label-left" novalidate action="Branch_Search" method="post">
              <div class="row">
                <div class="col-md-7">
                  <div class="form-group">
                    <label class="control-label col-md-4" for="first-name">Select Group <span class="required">*</span></label>
                    <div class="col-md-8">
                      <select class="select2_single form-control txtbxvisible " tabindex="-1" name="group_id" required>
                        <option></option>
                        <?php 
                          $data = $this->EventsModel->atm_group_retrieve();
            
                          if(!empty($data)) 
                          {
                            $counter = 1;
                            foreach ($data as $value) 
                            {
                              if($value->atmGroupId == $group_id)
                                $option = "Selected";
                              else
                                $option = "";
                        ?>
                        <option <?php print $option; $option="";?> value="<?php print $value->atmGroupId; ?>"><?php print $value->GroupName; ?></option>
                        <?php
                              $counter++;
                            }                                     
                          }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="item form-group">
                    <div class="">
                      <input class="btn btn-success"  name="branch_search" placeholder="" type="submit" value="View Branches" style="margin-top: 2px;">
                    </div>
                  </div>   
                </div>
              </div>
            </div> 
          </form>
        <div class="clearfix"></div>
      </div>
      <?php
        $branchresult = $this->session->flashdata('result');
        if(!empty($branchresult)) {
      ?>
        <div class="x_content">
          <table id="example" class="table table-striped responsive-utilities jambo_table" style="font-size:14px;">
            <thead>
              <tr class="headings">
                <th><input type="checkbox" class="tableflat"></th>
                <th># </th>
                <th>Branch Name </th>
                <th>ATM Group Name / ATM Region </th>
                <th class=" no-link last"><span class="nobr">Action</span></th>
              </tr>
            </thead>
            <tbody>
            <?php
              $maincounter = 1;
              foreach ($branchresult as $value) {
            ?>
              <tr class="even pointer">
                <td class="a-center "><input type="checkbox" class="tableflat"></td>
                <td class=""><?php print $maincounter; ?></td>
                <td class=""><?php print $value->name; ?></td>
                <?php 
                  $grp_ret = $this->load->EventsModel->group_id_name_ret($value->group_id);
                  if(!empty($grp_ret))
                    foreach ($grp_ret as $ret) { $group_name = $ret->GroupName; }
                  else
                    $group_name = "";
                ?>
                <td class=""><?php print $group_name; ?></td>
                <td style="padding:6px">
                  <a href="" data-toggle="modal" data-target=".View<?php print $maincounter?>" title="Edit">
                    <button class="btn btn-primary" style="margin:0px;padding:3px;font-size:12px">
                      <i class="fa fa-pencil"></i> Edit
                    </button>
                  </a>
                  <a href="" data-toggle="modal" data-target=".Delete<?php print $maincounter?>" title="Delete">
                    <button class="btn btn-danger" style="margin:0px;padding:3px;font-size:12px">
                      <i class="fa fa-trash"></i> Delete
                    </button>
                  </a>
                </td>
              </tr>
              
              <!-- Edit Modal -->
              <div class="modal fade View<?php print $maincounter?>" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
                      <h4 class="modal-title" id="myModalLabel2">Electronic Journal</h4>
                    </div>
                    <form class="form-horizontal form-label-left" novalidate action="Update_Branch" method="post" >
                      <div class="modal-body">
                        <div class="item form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Branch Name <span class="required">*</span></label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input class="form-control col-md-7 col-xs-12 txtbxvisible"  name="branch_name" placeholder="Comm. 12" required="required" type="text" value="<?php print $value->name; ?>"/>
                          </div>
                        </div>
                        <div class="item form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">ATM Group Name / Region <span class="required">*</span></label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <select class="select2_single form-control txtbxvisible " tabindex="-1" name="group_id" style="width:100%" required>
                              <option></option>
                              <?php 
                                $data = $this->EventsModel->atm_group_retrieve();
            
                                if(!empty($data)) 
                                {
                                  foreach ($data as $val) 
                                  {
                                    if($val->atmGroupId == $value->group_id)
                                      $option = "Selected";
                                    else
                                      $option = "";
                              ?>
                                    <option <?php print $option; $option="";?> value="<?= $val->atmGroupId; ?>"><?= $val->GroupName; ?></option>
                              <?php
                                  }
                                }
                              ?>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button id="send" type="submit" class="btn btn-success" name="update_branch">Update</button>
                        <a><button type="button" class="btn btn-primary"data-dismiss="modal" >Cancel</button></a>
                      </div>
                      <input type="hidden" name="branch_id" value="<?php print $value->branch_id; ?>"/>
                    </form>
                  </div>
                </div>
              </div>
              <!-- Edit Modal -->
              <?php
                  $maincounter++;
                }
              ?>
              </tbody>
            </table>
          </div>
        <?php } ?>  
                                        <?php

                                            if (!empty($result)) {
                                                # code...
                                                $counter = 1;
                                                foreach ($result as $res) {
                                                    # code...  
                                        ?>

                                        <div class="modal fade Delete<?php print $counter?>" tabindex="-1" role="dialog" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span>
                                                        </button>
                                                        <h4 class="modal-title" id="myModalLabel2">Electronic Journal</h4>
                                                    </div>
                                                    <form class="form-horizontal form-label-left" novalidate action="Delete_Branch" method="post" >
                                                    <div class="modal-body">
                                                        Deleting A branch would <strong style="color: red;">Delete All ATM's Registered Under It.</strong><br/>Do You Want To Really Continue <?php echo "<strong><em>".$res->name."</em></strong>"; ?> ....
                                                            
                                                    </div>
                                                    <div class="modal-footer">
                                                        <a><button type="button" class="btn btn-primary" data-dismiss="modal" >Cancel</button></a>
                                                                    <button id="send" type="submit" class="btn btn-danger" name="delete_branch">Delete</button>
                                                    </div>
                                                    <input type="hidden" name="branch_id" value="<?php print $res->branch_id; ?>"/>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                                    $counter++;
                                                }
                                            }
                                        ?>
                            </div>
                        </div>

                        <br />
                        <br />
                        <br />

                    </div>
                </div>
                    <!-- footer content -->
                
