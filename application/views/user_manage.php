<?php require_once('nav-md.php'); ?>

<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>User Management </h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <table id="example" class="table table-striped responsive-utilities jambo_table" style="font-size:14px;">
            <thead>
              <tr class="headings">
                <th>#</th>
                <th>Name </th>
                <th>Bank Branch </th>
                <th>Username </th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php 
                if(!empty($allusers)) 
                {
                  $counter = 1;
                  foreach ($allusers as $value) 
                  {
              ?>
              <tr class="even pointer">
                <td class="a-center ">
                  <?= $counter ?>
                </td>
                  
                <td class=" "><?php print $value->fullname; ?></td>

                <td class=" "><?php ($value->branchname) ? print $value->branchname : print ""; ?></td>
                <td class=""><?php  print $value->username; ?></td>
                <td class="">
                  <?php 
                    if( $value->state == 1)
                      print "<span class='label label-success'>Active</span>";
                      
                    elseif( $value->state == 0)
                      print "<span class='label label-danger'>De-Actived</span>";

                    else
                      print "<span class='label label-warning'>Unknown</span>";
                  ?>
                </td>
              <td>
                <a class="edituser btn btn-primary btn-xs" data-userid="<?= base64_encode($value->userid) ?>" data-fullname="<?php print $value->fullname; ?>" data-branch="<?php print $value->branchid; ?>" data-username="<?php print $value->username;?>" data-password="<?php print $value->password;?>" data-state="<?php print $value->state;?>" data-roleid="<?php print $value->roleid;?>" title="Edit" >
                  <i class="fa fa-pencil"></i> Edit
                </a>
                <a class="deluser btn btn-danger btn-xs" data-userid="<?= base64_encode($value->userid) ?>" data-delname="<?php print $value->fullname; ?>" title="Delete">
                  <i class="fa fa-trash"></i> Delete
                </a>
                <?php if( $value->state == 1) : ?>
                  <form action="../Access/Deactivate_User" method="post" style="display:inline !important"><input type="hidden" name="userId" value="<?= base64_encode($value->userid) ?>" /><button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-lock"></i> Deactivate </button></form>
                
                <?php elseif( $value->state == 0) : ?>
                  <form action="../Access/Activate_User" method="post"  style="display:inline !important"><input type="hidden" name="userId" value="<?= base64_encode($value->userid) ?>" /><button class="btn btn-success btn-xs" type="submit"><i class="fa fa-lock"></i> Activate </button></form>
                <?php
                  endif;
                ?>
              </td>
              </tr>
              <?php
                    $counter++;
                  }
                }
              ?>
            </tbody>

              </table>
          </div>
      </div>
  </div>

  <div id="useredit" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="width:900px">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
          <h4 class="modal-title" id="myModalLabel2">Edit User Info</h4>
        </div><br>
        <form class="form-horizontal form-label-right" action="../Access/Update_User" method="post">
          <div class="row">
            <div class="col-md-6">
              <div class=" form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Full Name <span class="required">*</span></label>
                <div class="col-md-7 col-sm-6 col-xs-12">
                  <input class="form-control col-md-7 col-xs-12 txtbxvisible" name="fullname" required type="text"/>
                </div>
              </div>
              <div class=" form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="id">Bank Branch <span class="required">*</span></label>
                <div class="col-md-7 col-sm-6 col-xs-12">
                  <select class="form-control txtbxvisible" name="branch_id" id="branchid" required style="width:100%">
                    <option></option>
                    <?php 
                      if(!empty($bran_grp_rln)) :
                        foreach ($bran_grp_rln as $mainkey =>$value) :
                          print "<optgroup label='$mainkey' style='background:#477'></optgroup>";
                          foreach($value As $val) :
                            print "<option value=".$val['branchid'].">{$val['branchname']}</option>";
                          endforeach;
                        endforeach;
                      endif;
                    ?>
                  </select>
                </div>
              </div>
              <div class=" form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Username <span class="required">*</span></label>
                <div class="col-md-7 col-sm-6 col-xs-12">
                  <input class="form-control col-md-7 col-xs-12 txtbxvisible"  name="username" placeholder="" required type="text" value="">
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class=" form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Password <span class="required">*</span></label>
                <div class="col-md-7 col-sm-6 col-xs-12">
                  <input type="password" id="password" name="password" required class="form-control col-md-7 col-xs-12 txtbxvisible"/>
                </div>
              </div>
              <div class=" form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Confirm <span class="required">*</span> Password </label>
                <div class="col-md-7 col-sm-6 col-xs-12">
                  <input type="password" id="userpassword2" name="password_repeat" required class="form-control col-md-7 col-xs-12 txtbxvisible"/>
                </div>
              </div>
              <div class=" form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ip">Role <span class="required">*</span></label>
                <div class="col-md-7 col-sm-6 col-xs-12">
                  <select class="form-control" name="roleid" id="roleid" required>
                    <option></option>
                    <?php 
                      if(!empty($roles)) 
                      {
                        foreach($roles as $val) 
                        {
                          if($val->roleId == $value->roleId)
                            $option = "Selected";
                          else
                            $option="";

                          print "<option $option value='".$val->roleId."'>".$val->ROLE."</option>";
                        }
                      }
                    ?>
                  </select>
                </div>
              </div>
            </div>
            <input type="hidden" name="userId" value="">
          </div>
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-md-offset-5">
              <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
              <button type="submit" class="btn btn-success" name="Register"><i class="fa fa-database"></i> Update</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

            <div id="userdelete" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-dialog modal-lg" style="width:700px">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel2">Confirmation</h4>
                  </div>
                  <div class="modal-body">
                     Do You Want To Really Delete <strong><em id="delname"></em></strong> ....
                  </div>
                  <div class="modal-footer">
                    <form class="form-horizontal form-label-left" action="../Access/Delete_User" method="post">
                      <div class="form-group">
                        <div class="col-md-6 col-md-offset-2">
                          <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
                          <button type="submit" class="btn btn-danger" name="delete_user"><i class="fa fa-trash" ></i> Delete</button>
                        </div>
                      </div>
                      <input type="hidden" name="userId" value=""/>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- footer content -->
                
