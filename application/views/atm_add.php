<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>NEW ATM</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <form class="form-horizontal form-label-left" action="ATM_Create" method="post">
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">ATM Name <span class="required" style="color: red;">*</span></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input class="form-control col-md-7 col-xs-12 txtbxvisible"  name="atm_name" placeholder="" required type="text" />
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">ATM Type <span class="required" style="color: red;">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <select class="select2_single form-control txtbxvisible" name="atmtype" data-placeholder="Select Atm Type">
                    <option></option>
                    <option>Wincor </option>
                    <option>NCR </option>
                  </select>
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="id">Terminal ID <span class="required" style="color: red;">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" name="terminalid" required="required" class="form-control col-md-7 col-xs-12 txtbxvisible">
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ip">Terminal IP <span class="required" style="color: red;">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" name="terminalip" class="form-control col-md-7 col-xs-12 txtbxvisible" required/>
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Branch <span class="required" style="color: red;">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <select class="select2_single form-control txtbxvisible" tabindex="-1" name="branch_id" required>
                    <option></option>
                    <?php 
                      $data = $this->EventsModel->all_branches();
        
                      if(!empty($data)) 
                      {
                        #code
                        $counter = 1;
                        foreach ($data as $value) 
                        {
                          $group = $this->EventsModel->group_id_name_ret($value->group_id);
                          foreach ($group as $groupname) 
                          {
                            $grpname = $groupname->GroupName;
                          }
                                                    ?>
                    <option value="<?php print $value->branch_id; ?>">
                      <?php print $value->name; ?>
                        &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                      <?php print $grpname ?>
                    </option>
                    <?php
                          $counter++;
                        }
                      }
                      else
                        $_SESSION['warning'] = "No Branches Registed <br/>Hence Can't Register ATM"
                    ?>
                </select>
              </div>
            </div>
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Location <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" name="location" required="required" class="form-control col-md-7 col-xs-12 txtbxvisible">
              </div>
            </div>
            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-6 col-md-offset-5">
                <input type="submit" class="btn btn-success" name="addatm" value="Add New">
                <a href="../Dashboard"><button type="button" class="btn btn-primary">Cancel</button></a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- footer content -->
