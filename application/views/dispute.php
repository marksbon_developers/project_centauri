<?php require_once('nav.php'); ?>

<!-- page content -->
<div class="right_col" role="main">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>DISPUTE MANAGER</h2>
          <div class="" style="margin-left: 20em;">
            <a class="btn btn-default active" id="new_dis">New Dispute</a>
            <a class="btn btn-default" id="sec_dis">Search Dispute</a>
            <a class="btn btn-default" id="ass_dis">Assign Dispute</a>
            <a class="btn btn-default" id="man_dis">Manage Assign</a>
          </div>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <div class="col-md-12" id="New_Dispute">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <form action="New_Dispute" method="POST">
                    <div class="x_title">
                      <h2 style="color: brown;">Account Information</h2>
                      <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                      
                      <div class="col-md-6">
                        <div class="item form-group">
                          <label class="col-md-4 col-sm-3 col-xs-12" for="name">Account Number</label>
                          <div class="col-md-8 col-sm-6 col-xs-12">
                            <input class="form-control col-md-7 col-xs-12 txtbxvisible"  name="acc_no" placeholder="xxxxxxxxxxxxxx" required type="text" >
                          </div>
                        </div>
                        <div class="item form-group">
                          <label class="col-md-4 col-sm-3 col-xs-12" for="name">Cardholder Name <span class="required"></span>
                          </label>
                          <div class="col-md-8 col-sm-6 col-xs-12">
                            <input class="form-control col-md-7 col-xs-12 txtbxvisible"  name="custo_name" placeholder="..............................................................................." required="required" type="text" >
                          </div>
                        </div>
                        <div class="item form-group">
                          <label class="col-md-4 col-sm-3 col-xs-12" for="name">2nd Cardholder<br />(if Applicable)  <span class="required"></span>
                          </label>
                          <div class="col-md-8 col-sm-6 col-xs-12">
                            <input class="form-control col-md-7 col-xs-12 txtbxvisible"  name="acc_no" placeholder="..............................................................................." type="text" >
                          </div>
                        </div>
                      </div>
                      
                      <div class="col-md-6">
                        <div class="item form-group">
                            <label class="col-md-4 col-sm-3 col-xs-12" for="name">Date <span class="required"></span>
                            </label>
                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <input class="form-control col-md-7 col-xs-12 txtbxvisible"  name="contact" value="<?php print gmdate('D d M, Y') ?>" readonly type="text" >
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="col-md-4 col-sm-3 col-xs-12" for="name">Severity <span class="required"></span>
                            </label>
                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <select class="select2_single form-control col-md-7 txtbxvisible" data-placeholder="Select One">
                                    <option></option>
                                    <option>Very Low</option>
                                    <option>Low</option>
                                    <option>Normal</option>
                                    <option>High</option>
                                    <option>Emergency</option>
                                </select>
                            </div>
                        </div>
                        <div class="item form-group">
                            <label class="col-md-4 col-sm-3 col-xs-12" for="name">Telephone No.<span class="required"></span>
                            </label>
                            <div class="col-md-8 col-sm-6 col-xs-12">
                                <input class="form-control col-md-7 col-xs-12 txtbxvisible"  name="contact" placeholder="..............................................................................." required="required" type="text" >
                            </div>
                        </div>
                      </div>
                  </div>
                  <div class="x_title">
                    <h2 style="color: brown;">Transaction Details</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table class="table table-bordered jambo_table" style="font-size: 13px;">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Type</th>
                            <th>Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row"><input type="checkbox" name="" value="" class="" /></th>
                            <td>Unauthorised Transaction</td>
                            <td>Neither I nor anyone authorised to use my account participated in or authorised the transaction.
                            My card was (choose one of the following choices below):<br />
                            <input type="radio" name="unauth_trans" value="In My Possession" id="possess"/> In my possession at the time of fraudulent use <br />
                            <input type="radio" name="unauth_trans" value="Lost/Stolen" id="stolen"/> Lost/Stolen: 
                                    <input type="text" name="lost_stole_date" class="onewaydate" placeholder="Date" style="padding: 2px 10px;font-weight: 900;" id="dispute"/> 
                                    <input type="text" name="location" placeholder="Location" style="padding: 2px 10px;font-weight: 900;" id="loc"/> <br />
                            <input type="radio" name="unauth_trans" value="Never Recieved" id="nevrecv"/> Never Recieved
                            </td>
                        </tr>
                        <tr>
                            <th scope="row"><input type="checkbox" name="" value="" class="" /></th>
                            <td>Duplicate / Multiple Processing</td>
                            <td>I have been charged multiple times but have authorised only one transaction. 
                                The original amount appeared on
                                <input type="text" name="" placeholder=".................................................." style="padding: 2px 10px;border:0px;margin-top:10px;font-weight: 900;" id="multiple"/>  
                                Statement (indicate statement period), a copy of which is enclosed.<br />
                                Upload Doc <input type="file" name="" style="display: inline;" />
                            </td>
                        </tr>
                        <tr>
                            <th scope="row"><input type="checkbox" name="" value="" class="" /></th>
                            <td>Difference In Amount</td>
                            <td>The amount on my sales slip differs from the amount billed. 
                                Attached is my sales slip showing the correct amount 
                                The difference amount disputed is GHC 
                                <input type="text" name="" placeholder="............................................" style="padding: 2px 10px;border:0px;margin-top:10px;font-weight: 900;"/> </td>
                        </tr>
                        <tr>
                            <th scope="row"><input type="checkbox" name="" value="" class="" /></th>
                            <td>Cancelled Transaction</td>
                            <td>I had cancelled the transaction / services on
                                <input type="text" name="lost_stole_date" data-placeholder=".........................." style="border:0px;margin-top:10px;width:90px;font-weight: 900;" id="cantrans"/>.
                                The reason for cancellation: 
                                <input type="text" name="lost_stole_date" placeholder="......................................................." style="padding: 2px 0 2ppxx 0px;border:0px;margin-top:10px;width:200px;font-weight: 900;"/>
                                The cancellation numbers/confirmation provided by the merchant is
                                <input type="text" name="lost_stole_date" placeholder="......................................................." style="padding: 2px 0 2ppxx 0px;border:0px;margin-top:10px;width:200px;font-weight: 900;"/> (proof attached)</td>
                        </tr>
                        <tr>
                            <th scope="row"><input type="checkbox" name="" value="" class="" /></th>
                            <td>Paid By Other Means</td>
                            <td>I had paid for the transaction by <input type="text" name="lost_stole_date" placeholder="......................................................." style="padding: 2px 0 2ppxx 0px;border:0px;margin-top:10px;font-weight: 900;width:200px"/> 
                            (specify the mode) for an amount of GHC <input type="text" name="lost_stole_date" placeholder="......................................................." style="padding: 2px 0 2ppxx 0px;border:0px;margin-top:10px;width:80px;font-weight: 900;"/>.
                            Attached is a copy of proof of payment made by other means (Cheque/Cash/through other Credit or Debit Card)</td>
                        </tr>
                        <tr>
                            <th scope="row"><input type="checkbox" name="" value="" class="" /></th>
                            <td>Credit Not Received</td>
                            <td>I was given a credit slip / credit confirmation for an amount of 
                            GHC <input type="text" name="lost_stole_date" placeholder="......................................................." style="padding: 2px 0 2ppxx 0px;border:0px;margin-top:10px;width:80px;font-weight: 900;"/> 
                            on <input type="text" name="lost_stole_date" placeholder="......................................................." style="padding: 2px 0 2ppxx 0px;border:0px;margin-top:10px;width:80px;font-weight: 900;" id="credit"/>
                            by the merchant.<br />
                            <input type="radio" name="unauth_trans" value="In My Possession" id="possess"/> The above credit is not yet reflecting on my statement.<br />

(Please attach the copy of the credit slip/ confirmation provided by the merchant)</td>
                                                            </tr>
                                                            <tr>
                                                                <th scope="row"><input type="checkbox" name="" value="" class="" /></th>
                                                                <td>Atm Discrepancy </td>
                                                                <td>
                                                                    <input type="radio" name="unauth_trans" value="In My Possession" id="possess"/>  ATM did not dispense cash but my account is debited for the amount of 
                                                                    GHC <input type="text" name="lost_stole_date" placeholder="......................................................." style="padding: 2px 0 2ppxx 0px;border:0px;margin-top:10px;width:80px;font-weight: 900;"/> on 
                                                                    <input type="text" name="lost_stole_date" placeholder="......................................................." style="padding: 2px 0 2ppxx 0px;border:0px;margin-top:10px;width:80px;font-weight: 900;" id="atm"/><br />
                                                                    <input type="radio" name="unauth_trans" value="In My Possession" id="possess"/> ATM partially dispensed cash for 
                                                                    GHC <input type="text" name="lost_stole_date" placeholder="......................................................." style="padding: 2px 0 2ppxx 0px;border:0px;margin-top:10px;width:80px;font-weight: 900;"/>  
                                                                    but my account is debited for the amount of
                                                                     GHC <input type="text" name="lost_stole_date" placeholder="......................................................." style="padding: 2px 0 2ppxx 0px;border:0px;margin-top:10px;width:80px;font-weight: 900;"/> 
                                                                     on <input type="text" name="lost_stole_date" placeholder="......................................................." style="padding: 2px 0 2ppxx 0px;border:0px;margin-top:10px;width:80px;font-weight: 900;" id="atmdis"/>
(Attached is my copy of ATM slip.)</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    </div>
                                                    <div class="x-footer">
                                                        <div class="col-md-4"></div>
                                                        <div class="col-md-3">
                                                            <input type="submit" class="btn btn-primary btn-block pull-right" name="dispute_new"/>
                                                        </div>
                                                        <div class="col-md-4"></div> 
                                                    </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12" id="Search_Disp">
                                        <div class="x_panel">
                                            <div class="x_title">
                                                <h2>Search Dispute</h2>
                                                <ul class="nav navbar-right panel_toolbox">
                                                    <li><a href="#"><i class="fa fa-chevron-up"></i></a>
                                                    </li>
                                                    <li class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li><a href="#">Settings 1</a>
                                                            </li>
                                                            <li><a href="#">Settings 2</a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li><a href="#"><i class="fa fa-close"></i></a>
                                                    </li>
                                                </ul>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="x_content">
                                                <table id="example" class="table table-striped responsive-utilities jambo_table">
                                                    <thead>
                                                        <tr class="headings">
                                                            <th>ID</th>
                                                            <th>Dispute </th>
                                                            <th>Requested By </th>
                                                            <th>Branch</th>
                                                            <th>Person In-Charge </th>
                                                            <th>Status </th>
                                                            <th class=" no-link last"><span class="nobr">Action</span></th>
                                                        </tr>
                                                    </thead>
            
                                                    <tbody>
                                                        <tr class="even pointer">
                                                            <td class=" ">1210</td>
                                                            <td class=" ">Difference In Amount</td>
                                                            <td class=" ">Kelvin Bach </td> 
                                                            <td class=" ">Kasoa </td> 
                                                            <td class=" ">Micheal Danso </td>
                                                            <td class=" "><span class='label label-warning'>Pending</span> </td>
                                                            <td class=" ">
                                                                <a href="#" data-toggle="modal" data-target=".reassign" class="btn btn-danger btn-xs">Re-Assign</a>
                                                            </td>
                                                        </tr>
                                                    </tbody>
            
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12" id="Assign_Disp">
                                        <div class="x_panel">
                                            <div class="x_title">
                                                <h2>Assign Dispute</h2>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="x_content">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-9">
                                                <form class="form-horizontal form-label-left" novalidate action="ATM_Create" method="post">
                                                    <div class="item form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Select Dispute <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <select class="select2_single form-control txtbxvisible" tabindex="-1" data-placeholder="Select a Dispute" name="branch_id" required>
                                                                <option></option>
                                                                <option>Dispute 1</option>
                                                                <option>Dispute 2</option>
                                                                <option>Dispute 3</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Select User <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <select class="select2_single form-control txtbxvisible" tabindex="-1" data-placeholder="Select a User" name="branch_id" required>
                                                                <option></option>
                                                                <?php 
                                                                    $data = $this->EventsModel->hq_users();
                    
                                                                    if(!empty($data)) {
                                                                        #code
                                                                        foreach ($data as $value) {
                                                                ?>
                                                                <option value="<?php print $value->userId; ?>">
                                                                    <?php print $value->fullName; ?>
                                                                </option>
                                                                <?php
                                                                        }
                                                                        
                                                                    }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="ln_solid"></div>
                                                    <div class="form-group">
                                                        <div class="col-md-6 col-md-offset-4">
                                                            <a href="../Dashboard"><button type="button" class="btn btn-primary">Cancel</button></a>
                                                            <input type="submit" class="btn btn-danger" name="addatm" value="Assign">
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="col-md-1"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- --------------Re-Assign Modal----------------- -->
                                    <div class="modal fade reassign" tabindex="-1" role="dialog" aria-hidden="true">
                                      <div class="modal-dialog modal-sm" style="width:50%">
                                         <div class="modal-content">
                                            <form class="form-horizontal" action="Access/Update_Password" method="post" novalidate>
                                               <div class="modal-headaer">
                                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                     <span aria-hidden="true">×</span>
                                                  </button>
                                                  <h4 class="modal-title" id="myModalLabel2">Re-Assign Dispute</h4>
                                               </div>
                                               <div class="modal-body">
                                                  <table class="table table-striped">
                                                    <tbody>
                                                      <tr>
                                                        <th style="width: 10px"> #</th>
                                                        <th>Fields</th>
                                                        <th>Data.</th>
                                                      </tr>
                                                      <tr>
                                                        <td>1.</td>
                                                        <td style="color: #9c0808;">Dispute</td>
                                                        <td>Difference In Amount</td>
                                                      </tr>
                                                      <tr>
                                                        <td>2.</td>
                                                        <td style="color: #9c0808;">Requested By</td>
                                                        <td>Kelvin Bach</td>
                                                      </tr>
                                                      <tr>
                                                        <td>3.</td>
                                                        <td style="color: #9c0808;">Branch</td>
                                                        <td>Kasoa</td>
                                                      </tr>
                                                      <tr>
                                                        <td>4.</td>
                                                        <td style="color: #9c0808;">Status</td>
                                                        <td><span class='label label-warning'>Pending</span></td>
                                                      </tr>
                                                      <tr>
                                                        <td>5.</td>
                                                        <td style="color: #9c0808;">Assigned To</td>
                                                        <td>
                                                          <select class="select2_single form-control txtbxvisible" tabindex="-1" data-placeholder="Select a User">
                                                            <option></option>
                                                            <option>USER 1</option>
                                                            <option>USER 2</option>
                                                            <option selected>USER 3</option>
                                                            <option>USER 4</option>
                                                          </select>
                                                        </td>
                                                      </tr>
                                                    </tbody>
                                                  </table>
                                                  <input type="hidden" name="userid" value="<?php print $_SESSION['userid']; ?>">
                                               </div>
                                               <div class="modal-footer">
                                                  <div class="col-md-8 offset-5">
                                                  <button type="button" class="btn btn-primary00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000" data-dismiss="modal">Close</button>
                                                  <button type="submit" class="btn btn-success" name="Update_Pwd">Update </button>
                                                  </div>
                                               </div>
                                            </form>
                                         </div>
                                      </div>
                                   </div>
                                    <!-- --------------Re-Assign Modal----------------- -->
                                </div>
                            </div>
                        </div>
                    </div>
                    
                
                <!-- footer content -->
