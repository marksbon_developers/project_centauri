
<body class="nav-md">

    <div class="container body">

  <div class="main_container">

            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">

                    <div class="navbar nav_title" style="">
                        <a href="../Dashboard" class="site_title"> <span>Alpha Centauri</span></a>
                    </div>
                    <div class="clearfix"></div>


                    <!-- menu prile quick info -->
                    <div class="profile">
                        <div class="profile_pic">
                            <img src="<?php print base_url(); ?>resources/images/logo.jpg" alt="..." class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            <h2 style="font-size:larger;">Company Name</h2>
                        </div>
                    </div>
                    <!-- /menu prile quick info -->
                    <div class="clearfix"></div>
                   

                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                        <div class="menu_section">
                            <ul class="nav side-menu">
                                <li><a><i class="fa fa-home"></i> Administration <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                       <li><a href="ATM_Add">Add ATM</a></li>
                                       <li><a href="ATM_Manage">Manage ATM</a></li>
                                       <li><a href="Add_Branches">Add Branches</a></li>
                                       <li><a href="">Manage Branches</a></li>
                                       <li><a href="ATM_Groups">ATM Group</a></li>
                                       <li><a href="ATM_Manage_Groups">Manage ATM Group</a></li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-user"></i> User Management <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="User_Registration">User Registration</a></li>
                                        <li><a href="User_Management">User Management</a></li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-binoculars"></i> Audit <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="#">User Logs</a></li>
                                        <li><a href="#">System Logs</a></li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-bar-chart-o"></i> Reports <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="chartjs.html">Chart JS</a>
                                        </li>
                                        <li><a href="chartjs2.html">Chart JS2</a>
                                        </li>
                                        <li><a href="morisjs.html">Moris JS</a>
                                        </li>
                                        <li><a href="echarts.html">ECharts </a>
                                        </li>
                                        <li><a href="other_charts.html">Other Charts </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="menu_section">
                           <h3 style="font-size:15px;padding: 8px 5px;background-color: brown;">System </h3>
                           <ul class="nav side-menu">
                              <li><a href="#"><i class="fa fa-database"></i>Backup Data</a> 
                              </li>
                              <li><a><i class="fa fa-line-chart"></i> Upgrade <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="#" data-toggle="modal" data-target=".upgrade">Professional Version (Pro)</a>
                                        </li>
                                        <li><a href="#" data-toggle="modal" data-target=".upgrade">Full Version (Premium)</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>

                    </div>
                    <!-- /sidebar menu -->

                    <!-- /menu footer buttons -->
                    <div class="sidebar-footer hidden-small">
                        <a data-toggle="tooltip" data-placement="top" title="Settings">
                            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                            <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Lock">
                            <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Logout">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                        </a>
                    </div>
                    <!-- /menu footer buttons -->
                </div>
         </div>

            <!-- top navigation -->
            <div class="top_nav">

                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                   <?php print $_SESSION['fullname']; ?>
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    <li>
                                        <a href="" data-toggle="modal" data-target=".changepwd">  Change Password</a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">Help</a>
                                    </li>
                                    <li><a href="logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                    </li>
                                </ul>
                                <div class="modal fade changepwd" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog modal-sm" style="width:40%">
                                        <div class="modal-content">
                                            <form class="form-horizontal" action="Access/Update_Password" method="post" novalidate>
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">�</span>
                                                    </button>
                                                    <h4 class="modal-title" id="myModalLabel2">Change Password</h4>
                                                </div>
                                                <div class="modal-body">
                                                    
                                                        <div class="item form-group">
                                                            <label class="col-md-3 col-sm-3 col-xs-12" for="Current">Current Password <span class="required"></span>
                                                            </label>
                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                <input type="password" id="password" name="CurrentPwd" required="required" class="form-control col-md-7 col-xs-12 txtbxvisible">
                                                            </div>
                                                        </div>
                                                        <div class="item form-group">
                                                            <label class="col-md-3 col-sm-3 col-xs-12" for="password">Password <span class="required"></span>
                                                            </label>
                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                <input type="password" id="password" name="password" required="required" class="form-control col-md-7 col-xs-12 txtbxvisible">
                                                            </div>
                                                        </div>
                                                        <div class="item form-group">
                                                            <label class="col-md-3 col-sm-3 col-xs-12" for="password">Confirm Password <span class="required"></span>
                                                            </label>
                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                <input type="password" id="password2" name="confirm_password" data-validate-linked="password" required="required" class="form-control col-md-7 col-xs-12 txtbxvisible">
                                                            </div>
                                                        </div>
                                                        <input type="hidden" name="userid" value="<?php print $_SESSION['userid']; ?>">
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary" name="Update_Pwd">Save changes</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </nav>
                </div>

            </div>
            <!-- /top navigation -->

<!-- page content -->
            <div class="right_col" role="main">
                <div class="">

                    <div class="row">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h3>Manage Branches</h3>
                                    <div class="clearfix"></div>
                                    <form class="form-horizontal form-label-left" novalidate action="Branch_Search" method="post">
                                    <div class="row">
                                        <div class="col-md-7">
                                            <div class="form-group">
                                                <label class="control-label col-md-4" for="first-name">Group Name / Region <span class="required">*</span>
                                                </label>
                                                <div class="col-md-8">
                                                    <select class="select2_single form-control txtbxvisible " tabindex="-1" name="group_id" required>
                                                        <option></option>
                                                        <?php 
                                                            $data = $this->EventsModel->atm_group_retrieve();
            
                                                            if(!empty($data)) {
                                                                #code
                                                               $counter = 1;
                                                                foreach ($data as $value) {
                                                                    if($value->atmGroupId == $group_id)
                                                                        $option = "Selected";
                                                                    else
                                                                        $option = "";
            
                                                        ?>
                                                        <option <?php print $option; $option="";?> value="<?php print $value->atmGroupId; ?>"><?php print $value->GroupName; ?></option>
                                                        <?php
                                                                    $counter++;
                                                                }
                                                                
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                       <div class="col-md-2">
                                            <div class="item form-group">
                                                
                                                <div class="">
                                                    <input class="btn btn-success"  name="branch_search" placeholder="" type="submit" value="View Branches" style="margin-top: 2px;">
                                                </div>
                                            </div>   </div>
                                        </div>
                                    </div> 
                                    </form>
                                    <div class="clearfix"></div>
                                </div>
                                <?php

                                    if (!empty($result)) {
                                ?>
                                <div class="x_content">
                                    <table id="example" class="table table-striped responsive-utilities jambo_table" style="font-size:14px;">
                                        <thead>
                                            <tr class="headings">
                                                <th>
                                                    <input type="checkbox" class="tableflat">
                                                </th>
                                                <th>ID </th>
                                                <th>Branch Name </th>
                                                <th>ATM Group Name / ATM Region </th>
                                                <th class=" no-link last"><span class="nobr">Action</span>
                                                </th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                        <?php
                                                # code...
                                                $counter = 1;
                                                foreach ($result as $value) {
                                                    # code...
                                               
                                        ?>
                                            <tr class="even pointer">
                                                <td class="a-center ">
                                                    <input type="checkbox" class="tableflat">
                                                </td>
                                                
                                                <td class=""><?php print $counter; ?></td>
                                                <td class=""><?php print $value->name; ?></td>
                                                <?php 
                                                    $grp_ret = $this->load->EventsModel->group_id_name_ret($value->group_id);
                                                    foreach ($grp_ret as $ret) {
                                                        $group_name = $ret->GroupName;
                                                    }
                                                ?>
                                                <td class=""><?php print $group_name; ?></td>
                                                <td style="padding:6px">
                                                    <a href="" data-toggle="modal" data-target=".View<?php print $counter?>" title="Edit">
                                                        <button class="btn btn-primary" style="margin:0px;padding:3px;font-size:12px">
                                                            <i class="fa fa-pencil"></i> Edit
                                                        </button>
                                                    </a>
                                                    <a href="" data-toggle="modal" data-target=".View<?php print $counter?>" title="Delete">
                                                        <button class="btn btn-danger" style="margin:0px;padding:3px;font-size:12px">
                                                            <i class="fa fa-trash"></i> Delete
                                                        </button>
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php
                                                    $counter++;
                                                }
                                         ?>
                                        </tbody>
                                    </table>
                                </div>
                                <?php } ?>
                                <?php

                                            if (!empty($result)) {
                                                # code...
                                                $counter = 1;
                                                foreach ($result as $res) {
                                                    # code...  
                                        ?>

                                        <div class="modal fade View<?php print $counter?>" tabindex="-1" role="dialog" aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span>
                                                        </button>
                                                        <h4 class="modal-title" id="myModalLabel2">Electronic Journal</h4>
                                                    </div>
                                                    <form class="form-horizontal form-label-left" novalidate action="Update_Branch" method="post" >
                                                    <div class="modal-body">
                                                        
                                                        <div class="item form-group">
                                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Branch Name <span class="required">*</span>
                                                                </label>
                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                    <input class="form-control col-md-7 col-xs-12 txtbxvisible"  name="branch_name" placeholder="Comm. 12" required="required" type="text" value="<?php print $res->name; ?>"/>
                                                                </div>
                                                            </div>
                                                            <div class="item form-group">
                                                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">ATM Group Name / Region <span class="required">*</span>
                                                                </label>
                                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                    <select class="select2_single form-control txtbxvisible " tabindex="-1" name="group_id" style="width:100%" required>
                                                        <option></option>
                                                        <?php 
                                                            $data = $this->EventsModel->atm_group_retrieve();
            
                                                            if(!empty($data)) {
                                                                #code
                                                               $counter = 1;
                                                                foreach ($data as $value) {
                                                                    if($value->atmGroupId == $group_id)
                                                                        $option = "Selected";
                                                                    else
                                                                        $option = "";
            
                                                        ?>
                                                        <option <?php print $option; $option="";?> value="<?php print $value->atmGroupId; ?>"><?php print $value->GroupName; ?></option>
                                                        <?php
                                                                    $counter++;
                                                                }
                                                                
                                                            }
                                                        ?>
                                                    </select>
                                                                </div>
                                                            </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <a href=""><button type="button" class="btn btn-primary" >Cancel</button></a>
                                                                    <button id="send" type="submit" class="btn btn-success" name="update_branch">Update</button>
                                                    </div>
                                                    <input type="hidden" name="branch_id" value="<?php print $res->branch_id; ?>"/>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                                    $counter++;
                                                }
                                            }
                                        ?>
                            </div>
                        </div>

                        <br />
                        <br />
                        <br />

                    </div>
                </div>
                    <!-- footer content -->
                

