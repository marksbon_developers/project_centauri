<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <title><?php print $title; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?php print base_url(); ?>resources/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="<?php print base_url(); ?>resources/fonts/css/font-awesome.min.css" rel="stylesheet"/>
    <link href="<?php print base_url(); ?>resources/css/animate.min.css" rel="stylesheet"/>
    
    <!-- daterange picker -->
    <link href="<?php print base_url(); ?>resources/plugins/daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />
    
    <script src="<?php print base_url(); ?>resources/js/jquery.min.js"></script>

    <!-- Custom styling plus plugins -->
    <link href="<?php print base_url(); ?>resources/css/custom.css" rel="stylesheet"/>
    <link href="<?php print base_url(); ?>resources/css/icheck/flat/green.css" rel="stylesheet" />
    <link href="<?php print base_url(); ?>resources/css/floatexamples.css" rel="stylesheet" type="text/css" />
    <link href="<?php print base_url(); ?>resources/css/datatables/tools/css/dataTables.tableTools.css" rel="stylesheet"/>
    <!-- select2 -->
    <link href="<?php print base_url(); ?>resources/css/select/select2.min.css" rel="stylesheet"/>

    <script src="<?php print base_url(); ?>resources/js/nprogress.js"></script>
    <script>
        NProgress.start();
    </script>
    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    <style>
        .txtbxvisible{
            border-color: rgba(115, 127, 140, 0.68);
        }
        .hidden {
            display:none;
        }
        
        .select2-results__group {background-color: #c1c1c1; text-align: center}

        .result{
          width: 93% !important;
        position: absolute;        
        z-index: 999;
        top: 100%;
        left: 5;
    }
.result p{
        margin: 0;
        padding: 7px 10px;
        border: 1px solid #CCCCCC;
        border-top: none;
        cursor: pointer;
    }
    .result p:hover{
        background: #f2f2f2;
    }
    </style>

    <script src="<?php print base_url(); ?>resources/js/customized.js"></script>

    <script type="text/javascript">
      $(document).ready(function(){
          $('.searchbox').on("keyup", function(){
              /* Get input value on change */
              var term = $(this).val();
              var resultDropdown = $(this).siblings(".result");
              if(term.length){
                  $.get("<?= base_url() ?>Dashboard/CardSearch", {query: term}).done(function(data){
                      // Display the returned data in browser
                      resultDropdown.html(data);
                  });
              } else{
                  resultDropdown.empty();
              }
              //alert("ksjfnlns");
          });
          
          // Set search input value on click of result item
          $(document).on("click", ".result p", function(){
              $(".searchbox").val($(this).text());
              $(this).parent(".result").empty();
          });
      });
    </script>
    
</head>




      
