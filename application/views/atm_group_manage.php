<!-- page content -->
            <div class="right_col" role="main">
                <div class="">

                    <div class="row">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Manage ATM Groups </h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <table id="example" class="table table-striped responsive-utilities jambo_table" style="font-size: 14px;">
                                        <thead>
                                            <tr class="headings">
                                                <th>
                                                    <input type="checkbox" class="tableflat">
                                                </th>
                                                <th>ATM Group Name </th>
                                                <th>Description </th>
                                                <th class=" no-link last"><span class="nobr">Action</span>
                                                </th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php 
                                                $data = $this->EventsModel->atm_group_retrieve();

                                                if(!empty($data)) {
                                                    #code
                                                   $counter = 1;
                                                    foreach ($data as $value) {

                                            ?>
                                            <tr class="even pointer">
                                                <td class="a-center ">
                                                    <input type="checkbox" class="tableflat">
                                                </td>
                                                <td class=" "><?php print $value->GroupName; ?></td>
                                                <td class=" "><?php print $value->Description; ?> </td>
                                                <td style="padding:6px"><span class='label label-warning'>No Action Available</span></td>
                                            </tr>
                                            <?php
                                                        $counter++;
                                                    }
                                                    
                                                }
                                            ?>
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>

                        <?php 

                            $data = $this->EventsModel->atm_group_retrieve();

                                if(!empty($data)) {
                                    #code
                                    $counter = 1;
                                    foreach ($data as $value) {
                        ?>
                        <div class="modal fade Edit<?php print $counter; ?>" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" style="width:700px">
                                        <div class="modal-content">
                                            <form class="form-horizontal form-label-left" novalidate action="ATM_Group_Update" method="post">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                                                    </button>
                                                    <h4 class="modal-title" id="myModalLabel2">Edit ATM Group Info</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="item form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">ATM Group Name <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <input class="form-control col-md-7 col-xs-12"  name="group_name" placeholder="" required="required" type="text" value="<?php print $value->GroupName; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="id">Description <span class="required">*</span>
                                                        </label>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <input type="text" id="email" name="desc" required="required" class="form-control col-md-7 col-xs-12" value="<?php print $value->Description; ?>">
                                                        </div>
                                                    </div>
                                                    <input type="hidden" name="atmGroupId" value="<?php print $value->atmGroupId; ?>"/>
                                                    <input type="hidden" name="oldFolder" value="<?php print $value->GroupName; ?>"/>
                                                   
                                                </div>
                                                <div class="modal-footer">
                                                    <div class="form-group">
                                                        <div class="col-md-6 col-md-offset-2">
                                                            <a href="">
                                                                <button type="button" class="btn btn-primary" data-dismiss="modal">
                                                                <i class="fa-fa-times"></i> Cancel
                                                                </button>
                                                            </a>
                                                            <button id="send" type="submit" class="btn btn-success" name="update_atm_group"><i class="fa-fa-check-square-o"></i> Update</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                    <?php
                                $counter++;
                            }
                        }
                    ?>

                    <!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- Delete Modal ------------------------------------------------------------>

                   <?php 

                        $data = $this->EventsModel->atm_group_retrieve();

                            if(!empty($data)) {
                                #code
                                $counter = 1;
                                foreach ($data as $value) {
                    ?>

                    <div class="modal fade Delete<?php print $counter; ?>" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" style="width:700px">
                                        <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                                                    </button>
                                                    <h4 class="modal-title" id="myModalLabel2">Confirmation</h4>
                                                </div>
                                                <div class="modal-body">
                                                        
                                                        Do You Want To Really Delete <?php echo "<strong><em>".$value->GroupName."</em></strong>"; ?> ....
                                                </div>
                                                <div class="modal-footer">
                                                <form class="form-horizontal form-label-left" action="Group_Delete" method="post">
                                                    <div class="form-group">
                                                        <div class="col-md-6 col-md-offset-2">
                                                            <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
                                                            <button type="submit" class="btn btn-danger" name="delete_group">
                                                            <i class="fa fa-trash" ></i> Delete
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" name="atmGroupId" value="<?php print $value->atmGroupId; ?>"/>
                                                </form>
                                                </div>
                                            
                                        </div>
                                    </div>
                                </div>

                    <?php
                                $counter++;
                            }
                        }
                    ?> 

                    </div>
                </div>
                    <!-- footer content -->
                
