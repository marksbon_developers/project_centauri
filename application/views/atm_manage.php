<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Manage ATM's </h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <table id="datatable-buttons" class="table table-striped table-bordered jambo_table" style="font-size: 14px;" >
                <thead>
                  <tr class="headings">
                    <th>#</th>
                    <th>ATM Name </th>
                    <th>Type </th>
                    <th>Terminal ID </th>
                    <th>Terminal IP </th>
                    <th>Branch </th>
                    <th>Location </th>
                    <th class=" no-link last"><span class="nobr">Action</span>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                    $data = $this->EventsModel->atm_retrieve();

                    if(!empty($data)) 
                    {
                      $counter = 1;
                      foreach ($data as $value) 
                      {
                  ?>
                        <tr class="even pointer">
                          <td class="a-center "><?= $counter ?></td>
                            <td class=" "><?= $value->atmName; ?></td>
                            <td class=" "><?= $value->atmType; ?></td>
                            <td class=" "><?= $value->terminalid; ?> 
                          </td>
                          <td class=""><?= $value->terminalip; ?></td>
                          <?php 
                            $branchinfo['branch_id'] = $value->branch_id;
                            
                            $result = $this->EventsModel->branch_retrieve($branchinfo);

                            if(!empty($result)) 
                              $branch_name = $result->name;
                            else
                              $branch_name = "";
                          ?>
                          <td class=""><?php print @$branch_name; ?></td>
                          <td class=""><?php print @$value->location; ?></td>
                          <td style="padding:6px">
                            <a href="" data-toggle="modal" data-target=".Edit<?php print $counter; ?>" title="Edit">
                              <button class="btn btn-primary" style="margin:0px;padding:3px;font-size:12px">
                                <i class="fa fa-pencil"></i> Edit
                              </button>
                            </a>
                            <a href="" data-toggle="modal" data-target=".Delete<?php print $counter; ?>" title="Delete">
                              <button class="btn btn-danger" style="margin:0px;padding:3px;font-size:12px">
                                <i class="fa fa-trash"></i> Delete
                              </button>
                            </a>
                          </td>
                        </tr>
                        <!--- Edit Modal -->
                        <div class="modal fade Edit<?php print $counter; ?>" tabindex="-1" role="dialog" aria-hidden="true">
                          <div class="modal-dialog modal-lg" style="width:700px">
                            <div class="modal-content">
                            
                              <form class="form-horizontal form-label-left" novalidate action="ATM_Update" method="post">

                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                                    </button>
                                    <h4 class="modal-title" id="myModalLabel2">Edit ATM Info</h4>
                                </div>
                                <div class="modal-body">
                                  <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">ATM Name <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                      <input class="form-control col-md-7 col-xs-12 txtbxvisible"  name="atm_name" placeholder="" required="required" type="text" value="<?php print $value->atmName; ?>">
                                    </div>
                                  </div>
                                  <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">ATM Type <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                      <select class=" form-control txtbxvisible" name="atm_type" data-placeholder="Select Atm Type" style="width:100%">
                                        <option></option>
                                        <?php 
                                          if($value->atmType == "Wincor")
                                            $select = "Selected";
                                          elseif($value->atmType == "NCR")
                                            $select = "Selected";
                                          else 
                                            $selected = "Selected";
                                        ?>
                                        <option <?php print @$selected; unset($selected);?> > -- Select One --</option>
                                        <option <?php print @$select; unset($select);?> >Wincor</option>
                                        <option    <?php print @$select; unset($select);?> >NCR </option>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="id">Terminal ID <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                      <input type="text" id="email" name="terminalid" required="required" class="form-control col-md-7 col-xs-12 txtbxvisible" value="<?php print $value->terminalid; ?>">
                                    </div>
                                  </div>
                                  <div class="item form-group">
                                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ip">Terminal IP <span class="required">*</span>
                                      </label>
                                      <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input type="text" name="terminalip" required="required" class="form-control col-md-7 col-xs-12 txtbxvisible"  value="<?php print $value->terminalip; ?>"/>
                                      </div>
                                  </div>
                                  <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Branch <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                      <select class="select2_single form-control txtbxvisible" tabindex="-1" name="branch_id" required style="width:100%">
                                        <option></option>
                                        <?php 
                                          $data = $this->EventsModel->all_branches();
                                          
                                          if(!empty($data)) 
                                          {
                                            $count = 1;
                                            foreach ($data as $val) 
                                            {
                                              $group = $this->EventsModel->group_id_name_ret($val->group_id);
                                              foreach ($group as $groupname) 
                                              {
                                                $grpname = $groupname->GroupName;
                                              }
                                              if($val->branch_id == $value->branch_id)
                                                $option = "Selected";
                                              else 
                                                $option = "";
                                        ?>
                                        <option <?php print $option; $option="";?> value="<?php print $val->branch_id; ?>"><?php print $val->name; ?>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<?php print $grpname ?></option>
                                        <?php
                                              $count++;
                                            }
                                          }
                                        ?>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="item form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Location <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                      <input type="text" name="location" required="required" class="form-control col-md-7 col-xs-12 txtbxvisible" value="<?php print $value->location; ?>">
                                    </div>
                                  </div>
                                  <input type="hidden" name="atm_id" value="<?php print $value->atmId; ?>"/>
                                  <div class="ln_solid"></div>
                                </div>
                                <div class="modal-footer">
                                  <div class="form-group">
                                    <div class="col-md-6 col-md-offset-2">
                                      <input type="submit" class="btn btn-success" name="update_atm" value="Update" />
                                      <a href="">
                                        <button type="button" class="btn btn-primary" data-dismiss="modal">
                                          <i class="fa-fa-times"></i> Cancel
                                        </button>
                                      </a>
                                    </div>
                                  </div>
                                </div>
                              </form>
                            </div>
                          </div>
                        </div>
                        <!--- End of Edit Modal -->
                        
                        <!--- Delete Modal -->
                        <div class="modal fade Delete<?php print $counter; ?>" tabindex="-1" role="dialog" aria-hidden="true">
                          <div class="modal-dialog modal-lg" style="width:700px">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                                </button>
                                <h4 class="modal-title" id="myModalLabel2">Confirmation</h4>
                              </div>
                              <div class="modal-body">
                                <input type="hidden" name="employee_id" value="<?php echo $value->atmId; ?>">
                                      
                                Do You Want To Really Delete <?php echo "<strong><em>".$value->atmName."</em></strong>"; ?> ....
                              </div>
                              <div class="modal-footer">
                                <form class="form-horizontal form-label-left" action="ATM_Delete" method="post">
                                  <div class="form-group">
                                    <div class="col-md-6 col-md-offset-2">
                                      <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
                                      <button type="submit" class="btn btn-danger" name="delete_atm">
                                      <i class="fa fa-trash" ></i> Delete
                                      </button>
                                    </div>
                                  </div>
                                  <input type="hidden" name="atmId" value="<?php print $value->atmId; ?>"/>
                                </form>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!--- End of Delete Modal -->
                  <?php
                        $counter++;
                      }
                    }
                    else
                    {
                      $_SESSION['warning'] = "No ATM's Registed";
                    }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
     </div>
                    <!-- footer content -->
                
