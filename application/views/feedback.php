<?php require_once('nav-md.php'); ?>

            <!-- page content -->
            <div class="right_col" role="main">

                <div class="">

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>SEND US A FEEDBACK</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">

                                    <form class="form-horizontal form-label-left" novalidate action="Send_Feedback" method="post">
                                        <div class="item form-group">
                                            <label class="col-md-2 col-sm-5 col-xs-12" for="name">Name of Company <span class="required"></span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input class="form-control col-md-7 col-xs-12 txtbxvisible"  name="companyname" value="Company Name" required type="text" readonly>
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="col-md-2 col-sm-3 col-xs-12" for="name">Name of Complaint <span class="required"></span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input class="form-control col-md-7 col-xs-12 txtbxvisible"  name="complaintname" value="<?php print $_SESSION['fullname']; ?>" required type="text" readonly>
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="col-md-2 col-sm-3 col-xs-12" for="id">Message <span class="required"></span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                               <textarea class="form-control txtbxvisible" name="complain" placeholder="Type Your Complain Here" style="resize: none;" rows="5" required></textarea>
                                            </div>
                                        </div>
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-3">
                                                <input type="submit" class="btn btn-block btn-success" name="send_complain" value="Send">
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- footer content -->
