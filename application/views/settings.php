  <!-- page content -->
  <div class="right_col" role="main">
    <div class="">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Server's IP Address & Port</h2>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <div class="col-xs-12">
                <!-- Tab panes -->
                <div class="tab-content">
                  <div class="tab-pane active" id="setserver">
                    <div class="row form-horizontal form-label-right">
                      <div class="col-md-1"></div>
                      <div class="col-md-5">
                        <div class="x_title">
                          <h2>Current Settings</h2>
                          <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                          <label class="col-md-4" style="padding-top:8px;">Current Server IP</label>
                          <div class="col-md-7">
                            <input class="form-control col-md-7 col-xs-12 txtbxvisible" value="<?= $serverinfo->serverip?>" readonly type="text" >
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-md-4" style="padding-top:8px;">Current Server Port <span class="required">*</span>
                          </label>
                          <div class="col-md-7">
                              <input class="form-control col-md-4 col-xs-12 txtbxvisible"  value="<?= $serverinfo->serverport?>" readonly type="text" >
                          </div>
                        </div>
                      </div>
                      <div class="col-md-5">
                        <form class="form-horizontal form-label-right" action="Set_Server"  method="POST">
                          <div class="x_title">
                            <h2>New Settings</h2>
                            <div class="clearfix"></div>
                          </div>
                          <div class="form-group">
                            <label class="col-md-4" style="padding-top:8px;">New Server IP</label>
                            <div class="col-md-7">
                              <input class="form-control col-md-7 col-xs-12 txtbxvisible" name="serverip" type="text" data-inputmask="'mask': '999.999.999.999'" required/>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-md-4" style="padding-top:8px;">New Server Port</label>
                            <div class="col-md-7">
                              <input class="form-control col-md-7 col-xs-12 txtbxvisible" name="serverport" type="number" required />
                            </div>
                          </div>
                          <div class="ln_solid"></div>
                          <div class="form-group">
                            <div class="col-md-6 col-md-offset-5">
                              <input type="submit" class="btn btn-success" name="serverupdate" value="Update">
                                <a href="<?= base_url(); ?>Dashboard"><button type="button" class="btn btn-primary">Cancel</button></a>
                            </div>
                          </div>
                        </form>
                      </div>
                      <div class="col-md-1"></div>
                    </div>
                  </div>
                  <div class="tab-pane" id="profile">Profile Tab.</div>
                  <div class="tab-pane" id="messages">Messages Tab.</div>
                  <div class="tab-pane" id="settings">Settings Tab.</div>
                </div>
              </div>
              <div class="col-xs-3" style="display:none">
                <ul class="nav nav-tabs tabs-left">
                  <li class="active"><a href="#setserver" data-toggle="tab">Server Settings</a>
                  </li>
                  <li><a href="#profile" data-toggle="tab">Settings 2</a>
                  </li>
                  <li><a href="#messages" data-toggle="tab">Settings 3</a>
                  </li>
                  <li><a href="#settings" data-toggle="tab">Settings 4</a>
                  </li>
                </ul>
              </div>
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- footer content -->
