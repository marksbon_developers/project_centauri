
<body class="nav-md">
  <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">
          <div class="navbar nav_title" style="">
            <a href="" class="site_title">
              <img src="<?php print base_url(); ?>resources/images/app_logo.jpg" style="border-radius:5px;"/> 
              <span><strong style="font-size: 28px;">&alpha;</strong> Centauri</span>
            </a>
          </div>
          <div class="clearfix"></div>
          <!-- menu prile quick info --
          <div class="profile">
            <div class="profile_pic">
              <img src="<?php print base_url(); ?>resources/images/logo.jpg" class="img-circle profile_img"/>
            </div>
            <div class="profile_info">
              <h2 style="font-size:larger;">Company Name</h2>
            </div>
          </div>-->
          <!-- /menu prile quick info -->
          <div class="clearfix"></div>
          <!-- sidebar menu -->
          <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
              <ul class="nav side-menu">
                <h3 class="nav-headings">General </h3>
                <li><a href="<?= base_url() ?>Dashboard/Dispute_Manager"><i class="fa fa-comments"></i>Dispute Manager</a></li>
                
                <?php if(isset($_SESSION['username']) && ($_SESSION['role'] == 'Administrator' || $_SESSION['role'] == 'GreenSunAdmin') ) { ?>
                
                <li><a><i class="fa fa-bar-chart-o"></i> Reports <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: none">
                    <li><a href="<?= base_url() ?>Report">EJ Manager</a></li>
                    <li><a href="<?= base_url() ?>Report">Reconciliation</a></li>
                    <li><a href="<?= base_url() ?>Report">Cash Manager</a></li>
                  </ul>
                </li>
                <li><a><i class="fa fa-binoculars"></i> Audit <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: none">
                    <li><a href="<?= base_url() ?>Report/User_Logs">User Logs</a></li>
                    <li><a href="<?= base_url() ?>Report/System_Logs">System Logs</a></li>
                  </ul>
                </li>
                <li><a href="Report/Feedback"><i class="fa fa-comment"></i>Feedback</a></li>
              </ul>
            </div>
            <div class="menu_section">
              <h3 class="nav-headings">System Settings </h3>
              <ul class="nav side-menu">
                <li><a><i class="fa fa-home"></i> ATM Management <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: none">
                    <li><a href="<?= base_url() ?>Administration/New_ATM">Add ATM</a></li>
                    <li><a href="<?= base_url() ?>Administration/Manage_ATM">Manage ATM</a></li>
                    <li><a href="<?= base_url() ?>Administration/New_Branch">Add Branches</a></li>
                    <li><a href="<?= base_url() ?>Administration/Manage_Branch">Manage Branches</a></li>
                    <li><a href="<?= base_url() ?>Administration/New_Group">ATM Group</a></li>
                    <li><a href="<?= base_url() ?>Administration/Manage_Groups">Manage ATM Group</a></li>
                  </ul>
                </li>
                <li><a><i class="fa fa-user"></i> User Management <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: none">
                    <li><a href="<?= base_url() ?>Administration/User_Registration">Register User</a></li>
                    <li><a href="<?= base_url() ?>Administration/User_Management">Manage Users</a></li>
                  </ul>
                </li>
              </ul>
            </div>
            <div class="menu_section">
              <h3 class="nav-headings">System </h3>
              <ul class="nav side-menu">
                <li><a><i class="fa fa-line-chart"></i> Upgrade <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: none">
                    <li><a data-toggle="modal" data-target=".upgrade">Premium Package</a></li>
                  </ul>
                </li>
              <li><a href="<?= base_url() ?>Administration/Settings"><i class="fa fa-gears"></i> Settings</a></li>
              <?php } ?>
              
              </ul>
            </div>
          </div>
          <!-- /sidebar menu -->
        </div>
      </div>
      <!-- top navigation -->
            <div class="top_nav">

                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                   <?php print $_SESSION['fullname']; ?>
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    <li><a href="#"><i class="fa fa-book pull-right"></i> Help</a>
                                    </li>
                                    <li>
                                        <a data-toggle="modal" data-target=".changepwd"><i class="fa fa-key pull-right"></i> Change Password</a>
                                    </li>
                                    <li><a href="<?= base_url() ?>/Access/Logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>

            </div>
            <!-- /top navigation -->

<!-- page content -->
<!-- page content -->
<div class="right_col" role="main">
  <div class="row">
    <div class="col-md-3">
      <h2 style="text-align: center;">EJ Manager</h2>
      <a href="<?= base_url() ?>Dashboard/E_Journal">
        <img class="img-thumbnail pull-xs-right " src="<?php print base_url(); ?>resources/images/atm.jpg" alt="Chania" width="300" height="236" display="block" />
      </a>
    </div>
    <?php 
      # Roles & Priviledges
      if($_SESSION['role'] != "User") :
    ?>
      <div class="col-md-3">
          <h2 style="text-align: center;">Reconciliation</h2>
          <a href="<?= base_url() ?>Reconciliation">
              <img src="<?php print base_url(); ?>resources/images/recon.jpg" class="img-thumbnail pull-xs-left"
              alt="Reconciliation" width="304" height="236" display="block" />
          </a>
      </div>
      <div class="col-md-3">
          <h2 style="text-align: center;">Cash Management</h2>
          <a href="#">
              <img src="<?php print base_url(); ?>resources/images/cash_mgmt.jpg" class="img-thumbnail pull-xs-left"
              alt="Cash Management" width="304" height="236" display="block" />
          </a>
      </div>
      <div class="col-md-3">
          <h2 style="text-align: center;">ATM Eye</h2>
          <a href="<?= base_url() ?>Dashboard/ATM_Eye">
              <img src="<?php print base_url(); ?>resources/images/atm-security-camera.jpg" class="img-thumbnail pull-xs-left"
              alt="Cash Management" width="304" height="236" display="block" />
          </a>
      </div>
      <?php endif; ?>
    </div>
  <div class="row tile_count">
</div>

                
