
<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
      <?php
          # Reassigning Data to Local Variable
          $report_result = $this->session->flashdata('report_result');

          if(empty($report_result))
          {
      ?>
        <div class="x_panel">
          <form class="form-horizontal form-label-left" action="Generating_Report" method="POST">
            <div class="row x_title">
              <h2 class="col-md-2"> Generate Report</h2>
              <h6 class="col-md-2">Fields with <b style="color: red;">*</b> are Required.</h6>
              <div class="col-md-3 pull-right"></div>
              <br/><hr/>
              <div class="clearfix"></div> 
              <div class="row">
                <div class="col-md-10">
                  <div class="col-md-4">
                    <div class="item form-group">
                      <label  for="name">ATM Region <span class="required" style="color: red;">*</span></label>
                      <div class="">
                        <select class="form-control txtbxvisible" name="group_id" id="atmgrp" data-placeholder="Select One" onchange="fetch_branch(this.value);" required>
                          <option value="">---- Select One ----</option>
                          <option value="All">All</option>
                          <?php 
                            $data = $this->EventsModel->atm_group_retrieve();
                    
                            if(!empty($data)) 
                            {
                              $counter = 1;
                              foreach ($data as $value) 
                              {
                          ?>
                          <option value="<?php print $value->atmGroupId; ?>"><?php print $value->GroupName; ?></option>
                          <?php
                                $counter++;
                              }
                            }
                          ?>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="item form-group">
                      <label  for="name">Branch Name </label>
                      <div class="">
                        <select class="select2_single form-control txtbxvisible" id="branchselect" name="branch_id" data-placeholder="Select One" onchange="fetch_atm(this.value);">
                          <option value="" selected></option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="item form-group">
                      <label  for="name">ATM Name </label>
                      <div class="">
                        <select class="select2_single form-control txtbxvisible" id="atmselect" name="atm_id" data-placeholder="Select One"></select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="item form-group">
                      <label  for="name">ATM Type 
                      </label>
                      <div class="">
                          <select class="form-control txtbxvisible" name="atmtype" data-placeholder="Select One">
                              <option value="">---- Select One ----</option>
                              <option>All </option>
                              <option>Wincor </option>
                              <option>NCR </option>
                          </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="item form-group">
                      <label  for="name">Report Type <span class="required" style="color: red;">*</span></label>
                      <div class="">
                        <select class="form-control " name="report_type" data-placeholder="Select One" required>
                          <option value="">---- Select One ----</option>
                          <optgroup style="color: chocolates;" label="TRANSACTIONS">
                          <option value="TransStat~1">Successful </option>
                          <option value="TransStat~0">Unsuccessful </option>
                          <option value="TransType~">Incomplete </option>
                          <option value="TransType~WITHDRAWAL">Withdrawals Only</option>
                          <option value="TransType~INQUIRES">Inquires Only</option>
                          <option value="TransType~TRANSFER">Transfer Only</option>
                          <option value="TransType~MINI STATEMENT">Mini Statement Only</option>
                          </optgroup>
                          
                          <optgroup label="SUCCESSFUL TRANSACTIONS">
                          <option value="TransStat~1/TransType~WITHDRAWAL">Successful Withdrawals </option>
                          <option value="TransStat~1/TransType~INQUIRES">Successful Inquires </option>
                          <option value="TransStat~1/TransType~TRANSFER">Successul Transfer</option>
                          <option value="TransStat~1/TransType~MINI STATEMENT">Successful Mini Statement</option>
                          </optgroup>
                          
                          <optgroup label="UNSUCCESSFUL TRANSACTIONS">
                          <option value="TransStat~0/TransType~WITHDRAWAL">Unsuccessful Withdrawals </option>
                          <option value="TransStat~0/TransType~INQUIRES">Unsuccessful Inquiries </option>
                          <option value="TransStat~0/TransType~TRANSFER">Unsuccessful Transfer</option>
                          <option value="TransStat~0/TransType~MINI STATEMENT"> Unsuccessful Mini Statement</option>
                          </optgroup>
                          
                          <optgroup label="EJ UPLOADS">
                          <option value="file~1">Successful EJ Uploads </option>
                          <option value="file~0">Unsuccessful EJ Uploads </option>
                          </optgroup>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="item form-group">
                      <label  for="name">Date <span class="required" style="color: red;">*</span></label>
                      <div class="">
                        <input type="text" class="form-control twowaydate" name="daterange" style="background: #fff; padding: 5px 10px; border: 1px solid #ccc" value="<?php print @$_SESSION['date']; ?>"/>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="item form-group">
                    <label  for="name"> <span class="required">*</span></label>
                    <div class="">
                        <input class="btn btn-success btn-lg" type="submit" name="gen_report" value="Generate Report" style="width: 100%;height: 100;">
                    </div>
                  </div>   
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
          </form>
        </div>
        <?Php
            }
           //print_r($report_result['report_result']);
           if(!empty($report_result))
           {
        ?>
        <div class="x_panel">
                               <div class="row x_title">
                                  <div class="col-md-1"><small>ATM Region : </small> &nbsp;&nbsp;&nbsp;</div>
                                  <div class="col-md-2"> <h2><?= $report_result['AtmRegion']; ?></h2> </div>
                                  <div class="pull-right"><a href="Generate_Report" class="btn btn-primary"><i class="fa fa-plus"></i> New Report</a></div>
                               </div>
                               <div>
                                 <!-- <a href="#" class="btn btn-info" onClick ="$('#example').tableExport({type:'pdf',escape:'false',orientation: 'p'});">Print PDF</a> 
                                 <a href="#" class="btn btn-info" onClick ="$('#example').tableExport({type:'excel',escape:'false'});">Print Excel</a>
                                 <a href="#" class="btn btn-info" onClick ="$('#example').tableExport({type:'doc',escape:'false'});">Print Word</a>-->
                               </div>
                               <table id="datatable-buttons" class="table table-striped responsive-utilities jambo_table" style="font-size: 14px;">
                                 <thead>
                                    <?php
                                        if(@$_SESSION['region'] == "All")
                                           print "<th>Region</th>";
                                    ?>
                                    <th>Branch</th>
                                    <th>Atm Name</th>
                                    <th>Terminal ID</th>
                                    <th>Terminal IP</th>
                                    <th>Type</th>
                                    <th>Start Time</th>
                                    <th>End Time</th>
                                    <th>Trans. Type</th>
                                    <th>Status</th>
                                 </thead>
                                 <tbody>
                                 <?php
                                    # Looping Through Result Obtained
                                    foreach( $report_result['report_result'] As $res )
                                    {
                                 ?>
                                    <tr>
                                       <?php
                                            if($_SESSION['region'] == "All")
                                               print "<td>$res->AtmRegion</td>";
                                       ?>
                                       <td><?= $res->Branch_name; ?></td>
                                       <td><?= $res->AtmName; ?></td>
                                       <td><?= $res->AtmId; ?></td>
                                       <td><?= $res->AtmIp; ?></td>
                                       <td><?= $res->Atmtype ; ?></td>
                                       <td><?= $res->AtmSTime; ?></td>
                                       <td><?= $res->AtmETime; ?></td>
                                       <td><?= $res->TransType; ?></td>
                                       <td>
                                       <?php                                            
                                          # Styling Result
                                          if($res->TransStat == 0)
                                             print "<span class='label label-danger'>Unsucessful</span>"; 
                                          if($res->TransStat == 1)
                                             print "<span class='label label-success'>Sucessful</span>"; 
                                          if($res->TransStat == '')
                                             print "<span class='label label-warning'>Incomplete</span>";       
                                       ?>
                                       </td>                                                                                                                                                                                
                                    </tr>
                                 <?php
                                    }
                                 ?>
                                 </tbody>
                               </table>
        </div>
        <?php
          }
        ?>
      </div>
      <?php
                                        
        unset($_SESSION['region']);
        unset($_SESSION['name']);
        unset($_SESSION['cardno']);
        unset($_SESSION['date']);
      ?>
    </div>
  </div>
  <br/><br/><br/>
</div>
</div>
<!-- footer content -->

<!-- html to pdf, excel,etc -->
<script type="text/javascript" src="<?php print base_url(); ?>resources/htmlPdf/tableExport.js"></script>
<script type="text/javascript" src="<?php print base_url(); ?>resources/htmlPdf/jquery.base64.js"></script>
<script type="text/javascript" src="<?php print base_url(); ?>resources/htmlPdf/html2canvas.js"></script>
<script type="text/javascript" src="<?php print base_url(); ?>resources/htmlPdf/jspdf/libs/sprintf.js"></script>
<script type="text/javascript" src="<?php print base_url(); ?>resources/htmlPdf/jspdf/jspdf.js"></script>
<script type="text/javascript" src="<?php print base_url(); ?>resources/htmlPdf/jspdf/libs/base64.js"></script>
<!-- end html pdf -->
                
