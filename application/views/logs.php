<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Log Search</h2></br> <hr/>
            <div class="clearfix"></div>
            <form class="form-horizontal form-label-left" action="#" method="post">
              <div class="row">
                <div class="col-md-4">
                  <div class="item form-group">
                    <label  for="name">ATM Name / Terminal (ID / IP) <span class="required" style="color:red">*</span></label>
                      <!-- <input class="form-control txtbxvisible"  name="search_text" placeholder="ATM Name / Terminal ID / Termianl IP" type="text" value="<?php print @$_SESSION['name']; ?>" required> -->
                    <select class="select2_single form-control txtbxvisible" name="search_text" data-placeholder="----            Select ATM Name / IP / ID       ----">
                        <option></option>
                        <?php if(!empty($atminfo_display)) : foreach($atminfo_display As $display) : ?>
                        <option><?= $display ?></option>
                        <?php endforeach; endif; ?>
                      </select>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="item form-group">
                    <label  for="name">Date <span class="required" style="color:red">*</span></label>
                    <div class="input-prepend input-group" >
                      <span class="add-on input-group-addon" style="background: none;"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                      <input type="text" name="daterange" class="form-control twowaydate" required value="<?php print @$_SESSION['date']; ?>">
                    </div>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="item form-group">
                    <label  for="name"> <span class="required">*</span></label>
                    <div class="input-prepend input-group" >
                      <input id="log_retrieve" class="btn btn-success" type="button" value="Search" onclick="LogRetrieve()">
                    </div>
                  </div>   
                </div>
              </div>
              <input type="hidden" name="request_type" value="Log">  
            </form>
          <div class="clearfix"></div>
        </div>
        
        

        <div class="x_content" style="display:none"id="tablecontainer" >
          <table name="logstbl" class="table table-striped table-bordered jambo_table" style="font-size: 14px;" >
            <thead>
              <tr class="headings">
                <th><input type="checkbox" class="tableflat"></th>
                <th>Start Time </th>
                <th>End Time </th>
                <th>Card Number </th>
                <th class=" no-link last"><span class="nobr">Action</span>
                </th>
              </tr>
            </thead>
            <tbody></tbody>
          </table>
        </div>

        <!-- **************** View Details Modal ********************** -->
        <div id="viewlogmodal" class="modal fade View" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog modal-sm">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel2">Electronic Journal</h4>
              </div>
              <div class="modal-body">
                <form enctype="multipart/form-data">
                  
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <br/><br/><br/>
    </div>
  </div>
  <!-- footer content -->
                
