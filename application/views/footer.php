  <footer>
    <div class="">
      <p style="text-align:center">
        <span class="lead"><img src="<?php print base_url(); ?>resources/images/logos.jpg"> GreenSun IT Engineering!</span> All Rights Reserved 
      </p>
    </div>
    <div class="clearfix"></div>
  </footer>
  <!-- /footer content -->
  <!-- Change Password Modal -->
  <div class="modal fade changepwd" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm" style="width:50%">
      <div class="modal-content">
        <form class="form-horizontal" action="Access/Update_Password" method="post">
          <div class="modal-headaer">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title" id="myModalLabel2">Change Password</h4>
          </div>
          <div class="modal-body">
            <div class="item form-group">
               <label class="col-md-3 col-sm-3 col-xs-12" for="Current">Current Password 
                  <span class="required"></span>
               </label>
               <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="password" id="password" name="CurrentPwd" required="required" class="form-control col-md-7 col-xs-12 txtbxvisible">
               </div>
            </div>
            <div class="item form-group">
               <label class="col-md-3 col-sm-3 col-xs-12" for="password">Password 
                  <span class="required"></span>
               </label>
               <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="password" id="password" name="password" required="required" class="form-control col-md-7 col-xs-12 txtbxvisible">
               </div>
            </div>
            <div class="item form-group">
               <label class="col-md-3 col-sm-3 col-xs-12" for="password">Confirm Password 
                  <span class="required"></span>
               </label>
               <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="password" id="password2" name="confirm_password" data-validate-linked="password" required="required" class="form-control col-md-7 col-xs-12 txtbxvisible">
               </div>
            </div>
            <input type="hidden" name="userid" value="<?php print $_SESSION['userid']; ?>">
          </div>
          <div class="modal-footer">
             <div class="col-md-8 offset-5">
             <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
             <button type="submit" class="btn btn-success" name="Update_Pwd">Update </button>
             </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- End of Modal -->
         
  <!-- Upgrade Modal -->
  <div class="modal fade upgrade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm" style="width:40%">
      <div class="modal-content">
        <form class="form-horizontal" action="" method="post" novalidate>
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title" id="myModalLabel2" style="color:red">Upgrade</h4>
          </div>
          <div class="modal-body">
            <div class="x_panel fixed_height_300">
              <div class="x_content">
                <div class="dashboard-widget-content">
                  <div class="row">
                    <div class="col-md-6">
                      <ul class="quick-list" style="width: 100%;">
                        <li><i class="fa fa-check-square-o" style="color:red"></i>Improved Security</li>
                        <li><i class="fa fa-check-square-o" style="color:red"></i>Improved Performance</li>
                        <li><i class="fa fa-check-square-o" style="color:red"></i>Cash Management</li>
                        <li><i class="fa fa-check-square-o" style="color:red"></i>Improved Security</li>
                      </ul>
                    </div>
                    <div class="col-md-6" >
                      <ul class="quick-list" style="width: 100%;">
                        <li><i class="fa fa-check-square-o" style="color:red"></i>Cash Management</li>
                        <li><i class="fa fa-check-square-o" style="color:red"></i>ATM Locator App</li>
                        <li><i class="fa fa-check-square-o" style="color:red"></i>Free Maintenance for 1 Year</li>
                        <li><i class="fa fa-check-square-o" style="color:red"></i>Improved Security</li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="item form-group has-success">
              <label class=" col-md-3 col-sm-6 col-xs-12 has-success" for="inputWarning">License Key</label>
              <div class=" col-md-9 col-sm-6 col-xs-12">
                <input type="email" class="form-control" id="inputWarning" placeholder="Enter ..." name="comp_name"  required/>
              </div>
            </div>
            <input type="hidden" name="userid" value="<?php print $_SESSION['userid']; ?>">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary" name="Update_Pwd">Confirm</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <!-- End of Modal -->
  <!-- Edit Modal -->
  <div class="modal fade disputeassign" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm" style="width:40%">
      <div class="modal-content">
        <form class="form-horizontal" action="" method="post" novalidate>
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title" id="myModalLabel2" style="color:red">Upgrade</h4>
          </div>
          <div class="modal-body">
            <div class="x_panel fixed_height_300">
              <div class="x_content">
                <div class="dashboard-widget-content">
                  <div class="row">
                    <div class="col-md-6">
                      <ul class="quick-list" style="width: 100%;">
                        <li><i class="fa fa-check-square-o" style="color:red"></i>Improved Security</li>
                        <li><i class="fa fa-check-square-o" style="color:red"></i>Improved Performance</li>
                        <li><i class="fa fa-check-square-o" style="color:red"></i>Cash Management</li>
                        <li><i class="fa fa-check-square-o" style="color:red"></i>Improved Security</li>
                      </ul>
                    </div>
                    <div class="col-md-6" >
                      <ul class="quick-list" style="width: 100%;">
                        <li><i class="fa fa-check-square-o" style="color:red"></i>Cash Management</li>
                        <li><i class="fa fa-check-square-o" style="color:red"></i>ATM Locator App</li>
                        <li><i class="fa fa-check-square-o" style="color:red"></i>Free Maintenance for 1 Year</li>
                        <li><i class="fa fa-check-square-o" style="color:red"></i>Improved Security</li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="item form-group has-success">
              <label class=" col-md-3 col-sm-6 col-xs-12 has-success" for="inputWarning">License Key</label>
              <div class=" col-md-9 col-sm-6 col-xs-12">
                <input type="email" class="form-control" id="inputWarning" placeholder="Enter ..." name="comp_name"  required/>
              </div>
            </div>
            <input type="hidden" name="userid" value="<?php print $_SESSION['userid']; ?>">
          </div>
          <div class="modal-footer">
             <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
             <button type="submit" class="btn btn-primary" name="Update_Pwd">Confirm</button>
          </div>
        </form>
      </div>
    </div>
  </div> 
         <!-- / Edit Model -->
  <!-- All Fields Required Modal -->
      <div class="modal fade danger" id='emptymodal' role='dialog' aria-hidden='true' >
        <div class="modal-dialog">
          <div class="modal-content">
          <form id="form_url" action="" method="post">
            <input type="hidden" name="resulturl" />
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
              <h4 class="modal-title">Notification</h4>
            </div>
            <div class="modal-body">
                  All Fields Required. 
            </div>
            <div class="modal-footer">
              <div class="col-md-2"></div>
              <div class="col-md-6">
                <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Ok</button>
              </div>
              <div class="col-md-4"></div>
            </div>
            </form>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
      <div class="modal fade danger" id='logviewmodal' role='dialog' aria-hidden='true' >
        <div class="modal-dialog">
          <div class="modal-content"  style="width: 320px;" >
          <form id="form_url" action="" method="post">
            <input type="hidden" name="resulturl" />
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x</span></button>
              <h4 class="modal-title">Log View</h4>
            </div>
            <div class="modal-body" id="logview_view">
               
            </div>
            <div class="modal-footer">
              <div class="col-md-2"></div>
              <div class="col-md-6">
                <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Ok</button>
              </div>
              <div class="col-md-4"></div>
            </div>
            </form>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
      <!-- All Delete Modals -->
</div>
      <!-- /page content -->

<div id="custom_notifications" class="custom-notifications dsp_none">
  <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group"></ul>
  <div class="clearfix"></div>
  <div id="notif-group" class="tabbed_notifications"></div>
</div>
  <!-- JAvascript Links -->
    <!-- Jquery plugin -->
    <script src="<?php print base_url(); ?>resources/js/jquery.min.js"></script>
    <!-- Bootstrap js -->
    <script src="<?php print base_url(); ?>resources/js/bootstrap.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="<?php print base_url(); ?>resources/js/progressbar/bootstrap-progressbar.min.js"></script>
    <!-- custom js -->
    <script src="<?php print base_url(); ?>resources/js/custom.js"></script>
    <!-- NiceScroll js -->
    <script src="<?php print base_url(); ?>resources/js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- dropzone -->
    <script src="<?php print base_url(); ?>resources/js/dropzone/dropzone.js"></script> 
    <!-- select2 js -->
    <script src="<?php print base_url(); ?>resources/js/select/select2.full.js"></script>
    <!-- input mask -->
    <script src="<?php print base_url(); ?>resources/js/input_mask/jquery.inputmask.js"></script> 
    <!-- Moments -->
    <script src="<?php print base_url(); ?>resources/plugins/daterangepicker/moment.min.js"></script> 
    <!-- date range picker -->
    <script src="<?php print base_url(); ?>resources/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- form validation  -->
    
  <!-- ############################### Javascript Links and Function ########################## -->
    <script type="text/javascript" src="<?php print base_url(); ?>resources/js/notify/pnotify.core.js"></script>
    <script type="text/javascript" src="<?php print base_url(); ?>resources/js/notify/pnotify.buttons.js"></script>
    <script type="text/javascript" src="<?php print base_url(); ?>resources/js/notify/pnotify.nonblock.js"></script>
    <!-- Error -->
    <?php if (isset($_SESSION['error'])) : ?>
    <script type="text/javascript">
      var permanotice, tooltip, _alert;
      $(function() {
        new PNotify({
          title: "An Error Occured",
          type: "error",
          text: "<?php print $_SESSION['error']; ?>",
          nonblock: {
            nonblock: true
          }
        });
      });
    </script>
    <?php unset($_SESSION['error']); endif; ?>
      <!-- Warning -->
    <?php if (isset($_SESSION['warning'])) : ?>
    <script type="text/javascript">
        var permanotice, tooltip, _alert;
        $(function() {
          new PNotify({
            title: "Notice",
            type: "warning",
            text: "<?php print $_SESSION['warning']; ?>",
            nonblock: {
              nonblock: true
            }
          });
        });
    </script>
    <?php unset($_SESSION['warning']); endif; ?>
    <!-- Success -->
    <?php if (isset($_SESSION['success'])) : ?>
    <script type="text/javascript">
        var permanotice, tooltip, _alert;
        $(function() {
          new PNotify({
            title: "Process Successful",
            type: "success",
            text: "<?php print $_SESSION['success']; ?>",
            nonblock: {
              nonblock: true
            }
          });
        });
    </script>
    <?php unset($_SESSION['success']); endif; ?>
  <!-- @@@@@@@@@@@@@@@@@@@@@ End Of notification @@@@@@@@@@@@@@@@@@@@@@@ -->
    <!--<script src="<?php print base_url(); ?>resources/js/datatables/js/jquery.dataTables.js"></script>
    <script src="<?php print base_url(); ?>resources/js/datatables/tools/js/dataTables.tableTools.js"></script>-->
    <script src="<?php print base_url(); ?>resources/js/datatables/jquery.dataTables.min.js"></script>
    <!--<script src="<?php print base_url(); ?>resources/js/datatables/dataTables.bootstrap.js"></script>-->
    <script src="<?php print base_url(); ?>resources/js/datatables/dataTables.buttons.min.js"></script>
    <script src="<?php print base_url(); ?>resources/js/datatables/buttons.bootstrap.min.js"></script>
    <script src="<?php print base_url(); ?>resources/js/datatables/jszip.min.js"></script>
    <script src="<?php print base_url(); ?>resources/js/datatables/pdfmake.min.js"></script>
    <script src="<?php print base_url(); ?>resources/js/datatables/vfs_fonts.js"></script>
    <script src="<?php print base_url(); ?>resources/js/datatables/buttons.html5.min.js"></script>
    <script src="<?php print base_url(); ?>resources/js/datatables/buttons.print.min.js"></script>
    <script src="<?php print base_url(); ?>resources/js/datatables/dataTables.fixedHeader.min.js"></script>
    <script src="<?php print base_url(); ?>resources/js/datatables/dataTables.keyTable.min.js"></script>
    <script src="<?php print base_url(); ?>resources/js/datatables/dataTables.responsive.min.js"></script>
    <script src="<?php print base_url(); ?>resources/js/datatables/responsive.bootstrap.min.js"></script>
    <script src="<?php print base_url(); ?>resources/js/datatables/dataTables.scroller.min.js"></script>
    <script src="<?php print base_url(); ?>resources/js/pace/pace.min.js"></script>
    <script>
      var handleDataTableButtons = function() {
          "use strict";
          0 !== $("#datatable-buttons").length && $("#datatable-buttons").DataTable({
            dom: "Bfrtip",
            buttons: [{
              extend: "csv",
              className: "btn-sm"
            }, {
              extend: "excel",
              className: "btn-sm"
            }, {
              extend: "pdf",
              className: "btn-sm"
            }, {
              extend: "print",
              className: "btn-sm"
            }],
            responsive: !0
          })
        },
        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons()
            }
          }
        }();
    </script>
    <script type="text/javascript">
      $(document).ready(function() {
        
        $('[name="resulturl"]').val(location.href);

        $('#datatable').dataTable();
        $('#datatable-keytable').DataTable({
          keys: true
        });
        $('#datatable-responsive').DataTable();
        $('#datatable-scroller').DataTable({
          ajax: "js/datatables/json/scroller-demo.json",
          deferRender: true,
          scrollY: 380,
          scrollCollapse: true,
          scroller: true
        });
        var table = $('#datatable-fixed-header').DataTable({
          fixedHeader: true
        });
      });
      TableManageButtons.init();
    </script>

  <!-- @@@@@@@@@@@@@@@@@@@@@ End Of Jquery DataTables @@@@@@@@@@@@@@@@@@@@@@@ -->
    <script src="<?php print base_url(); ?>resources/js/icheck/icheck.min.js"></script>
    <script type="text/javascript">
      $(document).ready(function() {
        $('input.tableflat').iCheck({
          checkboxClass: 'icheckbox_flat-green',
          radioClass: 'iradio_flat-green'
        });

        $(".table").on("click", ".logview", function(){
          $('#logview_view').html($(this).data('ejfile'));
          $('#logviewmodal').modal('show');
        });
      });

    </script>
  <!-- @@@@@@@@@@@@@@@@@@@@@ End Of Jquery icheck @@@@@@@@@@@@@@@@@@@@@@@ -->
  <script>
    NProgress.done();
  </script>
  <!-- @@@@@@@@@@@@@@@@@@@@@ End Of NProgress @@@@@@@@@@@@@@@@@@@@@@@ --> 
  <script type="text/javascript">
    $(document).ready(function () 
      {
        /*********** Select2 ***********/
        $(".select2_single").select2({
          placeholder: "Select a Group",
          valid:true,
          allowClear: true
        });
        $(".select2_multiple").select2({
          maximumSelectionLength: 4,
          placeholder: "With Max Selection limit 4",
          allowClear: true
        });
        /*********** Select2 ***********/

        /*********** Inputmask ***********/
        $(":input").inputmask();
        /*********** Inputmask ***********/

        /*********** Dispute Manager ***********/
          /********** Dispute Lost / Stolen Card ********/
            $('#dispute').css("display","none");
            $('#loc').css("display","none");
            
            $("#possess").click(function () {
                $('#dispute').css("display","none");
                $('#loc').css("display","none");
            });
            $("#nevrecv").click(function () {
                $('#dispute').css("display","none");
                $('#loc').css("display","none");
            });
            $("#stolen").click(function () {
                $('#dispute').css("display","inherit");
                $('#loc').css("display","inherit");
            });
          
          /****************** Default **************/
            //Disabling other tabs
                $('#new_dis').addClass('active');
                $('#sec_dis').removeClass('active');
                $('#ass_dis').removeClass('active');
                $('#man_dis').removeClass('active');
            //Disabling other views
                $('#New_Dispute').css("display","block");
                $('#Search_Disp').css("display","none");
                $('#Assign_Disp').css("display","none");
                $('#Manage_Disp').css("display","none");
            /****************** Default **************/
            
            /****************** New Dispute **************/
            $("#new_dis").click(function () {
                //Disabling other tabs
                $('#new_dis').addClass('active');
                $('#sec_dis').removeClass('active');
                $('#ass_dis').removeClass('active');
                $('#man_dis').removeClass('active');
                //Disabling other views
                $('#New_Dispute').css("display","block");
                $('#Search_Disp').css("display","none");
                $('#Assign_Disp').css("display","none");
                $('#Manage_Disp').css("display","none");
            });
            /****************** New Dispute **************/
            
            /****************** Search Dispute **********/
            $("#sec_dis").click(function () {
                //Disabling other tabs
                $('#sec_dis ').addClass('active');
                $('#new_dis').removeClass('active');
                $('#ass_dis').removeClass('active');
                $('#man_dis').removeClass('active');
                //Disabling other views
                $('#Search_Disp ').css("display","block");
                $('#New_Dispute').css("display","none");
                $('#Assign_Disp').css("display","none");
                $('#Manage_Disp').css("display","none");
            });
            /****************** Search Dispute **********/
            
            /****************** Assign Dispute **********/
            $("#ass_dis ").click(function () {
                //Disabling other tabs
                $('#ass_dis ').addClass('active');
                $('#new_dis').removeClass('active');
                $('#sec_dis').removeClass('active');
                $('#man_dis').removeClass('active');
                //Disabling other views
                $('#Assign_Disp   ').css("display","block ");
                $('#Search_Disp').css("display","none");
                $('#New_Dispute').css("display","none");
                $('#Manage_Disp').css("display","none");
            });
            /****************** Assign Dispute **********/

            /****************** Edit User Modal **************/
            $('.edituser').click(function(){
              $('[name="fullname"]').val($(this).data('fullname'));
              $('[name="username"]').val($(this).data('username'));
              $('[name="password"]').val($(this).data('password'));
              $('[name="password_repeat"]').val($(this).data('password'));
              $('[name="userId"]').val($(this).data('userid'));
              $('#branchid option[value="'+ $(this).data('branch') + '"]').attr('selected', 'selected');
              $('#roleid option[value="'+ $(this).data('roleid') + '"]').attr('selected', 'selected');
              $('#branchid ').addClass('select2_single');
              $(".select2_single").select2({
                placeholder: "Select a Group",
                valid:true,
                allowClear: true
              });
              $('#useredit').modal('show');
            });
            /****************** Edit User Modal **************/
            /****************** Delete User Modal ************/
            $('.deluser').click(function(){
              $('#delname').text($(this).data('delname'));
              $('[name="userId"]').val($(this).data('userid'));
              $('#userdelete').modal('show');
            });
            /****************** Delete User Modal ************/
        /*********** Dispute Manager ***********/

        /****************** icheck ************/
        
        /****************** icheck ************/
        /** EJ ATM / BANK Switch Files Upload */
        $("#atmuploadimg").click(function(){
           $("#atmuploadFile").click();
        });
        $("#bankuploadimg").click(function(){
           $("#bankuploadfile").click();
        });
        $("#startbtnimg").click(function(){
           $("#start2way").click();
        });
        /** EJ ATM / BANK Switch Files Upload */
        
        /****************** date range ************/
        $('.twowaydate').daterangepicker({
          locale: {
            format: 'YYYY-MM-DD',
            "separator": " to ",
            "applyLabel": "Apply",
            "cancelLabel": "Cancel",
            "fromLabel": "From",
            "toLabel": "To",
            "customRangeLabel": "Custom",
            "daysOfWeek": [
                "Su",
                "Mo",
                "Tu",
                "We",
                "Th",
                "Fr",
                "Sa"
            ],
            "monthNames": [
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
            ],
            "firstDay": 1,
          },
          "linkedCalendars": false,
          "showDropdowns": true,
          "autoApply": true
        });
        $('.onewaydate').daterangepicker({
          "singleDatePicker": true,
          "showDropdowns": true,
          "showWeekNumbers": true,
          "autoApply": true,
          "locale": {
              "format": "YYYY-MM-DD",
              "separator": " - ",
              "applyLabel": "Apply",
              "cancelLabel": "Cancel",
              "fromLabel": "From",
              "toLabel": "To",
              "customRangeLabel": "Custom",
              "daysOfWeek": [
                  "Su",
                  "Mo",
                  "Tu",
                  "We",
                  "Th",
                  "Fr",
                  "Sa"
              ],
              "monthNames": [
                  "January",
                  "February",
                  "March",
                  "April",
                  "May",
                  "June",
                  "July",
                  "August",
                  "September",
                  "October",
                  "November",
                  "December"
              ],
              "firstDay": 1
          },
          "linkedCalendars": false,
          "opens": "left"
          }, function(start, end, label) {
            console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
        });
        $('.twowayWTime').daterangepicker({
          "showDropdowns": true,
          "autoApply": true,
          "timePicker": true,
          "timePicker24Hour": true,
          "timePickerSeconds": true,
          "locale": {
              "format": "YYYY-MM-DD HH:mm:ss",
              "separator": " To ",
              "applyLabel": "Apply",
              "cancelLabel": "Cancel",
              "fromLabel": "From",
              "toLabel": "To",
              "customRangeLabel": "Custom",
              "daysOfWeek": [
                  "Su",
                  "Mo",
                  "Tu",
                  "We",
                  "Th",
                  "Fr",
                  "Sa"
              ],
              "monthNames": [
                  "January",
                  "February",
                  "March",
                  "April",
                  "May",
                  "June",
                  "July",
                  "August",
                  "September",
                  "October",
                  "November",
                  "December"
              ],
              "firstDay": 1
          },
          "linkedCalendars": false,
          "alwaysShowCalendars": true
          }, function(start, end, label) {
            console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
          });
        /****************** date range ************/
      });
  </script>
  <!-- @@@@@@@@@@@@@@@@@@@@@ End Of NProgress @@@@@@@@@@@@@@@@@@@@@@@ --> 
  <script src="<?php print base_url(); ?>resources/js/echart/echarts-all.js"></script>
  <script src="<?php print base_url(); ?>resources/js/echart/green.js"></script>
  <script>
    var myChart = echarts.init(document.getElementById('echart_pie'), theme);
    myChart.setOption({
    tooltip: {
      trigger: 'item',
      formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    legend: {
      orient: 'vertical',
      //x: 'left',
      x: 'right',
      y: 'top',
      data: [
        <?php  if(@$total_ga) { print "'Accra',"; } ?>
        <?php  if(@$total_tm) { print "'Tema',"; } ?>
        <?php  if(@$total_er) { print "'Eastern',"; } ?>
        <?php  if(@$total_wr) { print "'Western',"; } ?>
        <?php  if(@$total_cr) { print "'Central',"; } ?>
        <?php  if(@$total_nr) { print "'Northern',"; } ?>
        <?php  if(@$total_vr) { print "'Volta',"; } ?>
        <?php  if(@$total_ash) { print "'Ashanti',"; } ?>
        <?php  if(@$total_ba) { print "'Brong Ahafo',"; } ?>
        <?php  if(@$total_ue) { print "'Upper East',"; } ?>
        <?php  if(@$total_uw) { print "'Upper West'"; } ?>
      ]
    },
    toolbox: {
      show: true,
      x:'left',
      y: 'bottom',
      feature: {
        restore: {
          show: true
        },
        saveAsImage: {
          show: true
        }
      }
    },
    calculable: true,
    series: [
    {
      name: 'ATM Group',
      type: 'pie',
      radius: '50%',
      center: ['40%', '40%'], //left,top
      data: [
        <?php  if(@$total_ga) { print "{ value: '$total_ga', name: 'Accra'},"; } else { } ?>
        <?php  if(@$total_tm) { print "{value: '$total_tm',  name: 'Tema'},"; } else { } ?>
        <?php  if(@$total_er) { print "{value: '$total_er',  name: 'Eastern'},"; } else { } ?>
        <?php  if(@$total_wr) { print "{value: '$total_wr',  name: 'Western'},"; } else { } ?>
        <?php  if(@$total_cr) { print "{value: '$total_cr',  name: 'Central'},"; } else { } ?>
        <?php  if(@$total_nr) { print "{value: '$total_nr',  name: 'Northern'},"; } else { } ?>
        <?php  if(@$total_vr) { print "{value: '$total_vr',  name: 'Volta'},"; } else { } ?>
        <?php  if(@$total_ash) { print "{value: '$total_ash', name: 'Ashanti'}"; } else { } ?>
        <?php  if(@$total_ba) { print "{value: '$total_ba',   name: 'Brong Ahafo},'"; } else { } ?>
        <?php  if(@$total_ue) { print "{value: '$total_ue',   name: 'Upper East'},"; } else { } ?>
        <?php  if(@$total_uw) { print "{value: '$total_uw',   name: 'Upper West'}"; } else { } ?>
      ]
    }]
  });


        var dataStyle = {
            normal: {
                label: {
                    show: false
                },
                labelLine: {
                    show: false
                }
            }
        };
        var placeHolderStyle = {
            normal: {
                color: 'rgba(0,0,0,0)',
                label: {
                    show: false
                },
                labelLine: {
                    show: false
                }
            },
            emphasis: {
                color: 'rgba(0,0,0,0)'
            }
        };
  </script>
  <!-- @@@@@@@@@@@@@@@@@@@@@ End Of Report Chart @@@@@@@@@@@@@@@@@@@@@@@ -->
  <script type="text/javascript">
    function LogRetrieve() 
    {
      searchText = $('[name="search_text"]').val();
      dateRanger = $('[name="daterange"]').val();

      if(searchText == "" || dateRanger == "")
      {
        $('#emptymodal').modal('show');
      }

      else
      {
        daterange = dateRanger.replace(/ /g,'x');

        $.ajax({
          type: 'POST',
          url: "<?php echo base_url().'Dashboard/Log_Search/"+searchText+"/"+daterange+"'; ?>",
          success: function(response)
          {
            if(response)
            {
              $('#tablecontainer').show();

              $('[name="logstbl"]').DataTable({
                "ajax": {
                    url: "<?php echo base_url().'Dashboard/Log_Search/"+searchText+"/"+daterange+"'; ?>",
                    dataSrc: ''
                },
                "columns": [
                  { "defaultContent": searchText},
                  { "data": "hostStartTime" },
                  { "data": "hostEndTime" },
                  { "data": "hostCardNumber" },
                  { sortable: false,
                    "render": function ( data, type, log, meta ) 
                    {
                      return '<a class="btn btn-primary btn-xs edit-sup-btn logview" data-sup_id="'+log.transactionId+'" data-name="'+log.hostStartTime+'" data-addr="'+log.hostEndTime+'" data-tel1="'+log.hostCardNumber+'" data-ejfile="'+log.file+'"><i class="fa fa-th"></i> View</a>';
                    }
                  }   
                ],
              });
              //alert(response);
            }
            else
            {
              $('#tablecontainer').hide()
              alert("No Logs Found");
            }
          }   
        });

        /**/
      }
    }

    function ReportRetrieve() 
    {
      searchText = $('[name="search_text"]').val();
      dateRanger = $('[name="daterange"]').val();

      if(searchText == "" || dateRanger == "")
      {
        $('#emptymodal').modal('show');
      }

      else
      {
        daterange = dateRanger.replace(/ /g,'x');

        $.ajax({
          type: 'POST',
          url: "<?php echo base_url().'Dashboard/Log_Search/"+searchText+"/"+daterange+"'; ?>",
          success: function(response)
          {
            if(response)
            {
              $('#tablecontainer').show();

              $('[name="logstbl"]').DataTable({
                "ajax": {
                    url: "<?php echo base_url().'Dashboard/Log_Search/"+searchText+"/"+daterange+"'; ?>",
                    dataSrc: ''
                },
                "columns": [
                  { "defaultContent": searchText},
                  { "data": "hostStartTime" },
                  { "data": "hostEndTime" },
                  { "data": "hostCardNumber" },
                  { sortable: false,
                    "render": function ( data, type, log, meta ) 
                    {
                      return '<a class="btn btn-primary btn-xs edit-sup-btn" data-sup_id="'+log.transactionId+'" data-name="'+log.hostStartTime+'" data-addr="'+log.hostEndTime+'" data-tel1="'+log.hostCardNumber+'"><i class="fa fa-pencil"></i> Edit</a>';
                    }
                  }   
                ],
              });
            }
            else
            {
              $('#tablecontainer').hide()
              alert("No Logs Found");
            }
          }   
        });

        /**/
      }
    }
  </script>
    
<!-- /footer content -->
</body>

</html>
