<!-- page content -->
  <div class="right_col" role="main">
    <div class="">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>New User</h2>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <form class="form-horizontal form-label-right" action="../Access/Register_User"  method="post">
                <div class="row">
                  <div class="col-md-6">
                    <div class=" form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Full Name <span class="required">*</span></label>
                      <div class="col-md-7 col-sm-6 col-xs-12">
                        <input class="form-control col-md-7 col-xs-12 txtbxvisible"  name="fullname" placeholder="" required type="text" >
                      </div>
                    </div>
                    <div class=" form-group">
                      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="id">Bank Branch <span class="required">*</span></label>
                        <div class="col-md-7 col-sm-6 col-xs-12">
                          <select class="select2_single form-control txtbxvisible" tabindex="-1" name="branch_id" required>
                            <option></option>
                            <?php 
                              $data = $this->EventsModel->all_branches();
                              if(!empty($data)) {
                                #code
                                $counter = 1;
                                foreach ($data as $value) 
                                {
                                  $group = $this->EventsModel->group_id_name_ret($value->group_id);
                                  foreach ($group as $groupname) 
                                  {
                                    $grpname = $groupname->GroupName;
                                  }
                            ?>
                            <option value="<?php print $value->branch_id; ?>"><?php print $value->name; ?>&emsp;&emsp;&emsp;<?php print $grpname ?></option>
                            <?php
                                  $counter++;
                                }
                              }
                            ?>
                          </select>
                        </div>
                      </div>
                      <!--<div class=" form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ip">Department <span class="required">*</span>
                          </label>
                          <div class="col-md-7 col-sm-6 col-xs-12">
                              <input type="text" id="email2" name="dept" required class="form-control col-md-7 col-xs-12 txtbxvisible"  placeholder=""value="">
                          </div>
                      </div>-->
                      <div class=" form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Username <span class="required">*</span>
                          </label>
                          <div class="col-md-7 col-sm-6 col-xs-12">
                              <input class="form-control col-md-7 col-xs-12 txtbxvisible"  name="username" placeholder="" required type="text" >
                          </div>
                      </div>
                    </div>
                                            <div class="col-md-6">
                                                <div class=" form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Password <span class="required">*</span>
                                                    </label>
                                                    <div class="col-md-7 col-sm-6 col-xs-12">
                                                        <input type="password" id="password" name="password" required class="form-control col-md-7 col-xs-12 txtbxvisible" >
                                                    </div>
                                                </div>
                                                <div class=" form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Confirm <span class="required">*</span> Password 
                                                    </label>
                                                    <div class="col-md-7 col-sm-6 col-xs-12">
                                                        <input type="password" id="userpassword2" name="password_repeat" required class="form-control col-md-7 col-xs-12 txtbxvisible">
                                                    </div>
                                                </div>
                                                <div class=" form-group">
                                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ip">Role <span class="required">*</span>
                                                    </label>
                                                    <div class="col-md-7 col-sm-6 col-xs-12">
                                                        <select class="form-control" name="roleid" required>
                                                            <option></option>
                                                            <?php 
                                                                if(!empty($roles)) {
                                                                    foreach($roles as $value) {
                                                            
                                                                        print "<option value='".$value->roleId."'>".$value->ROLE."</option>";
                                                                    }
                                                                }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-md-offset-5">
                                                <input type="submit" class="btn btn-success" name="Register" value="Create">
                                                <a href="../Dashboard"><button type="button" class="btn btn-primary">Cancel</button></a>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                
                <!-- footer content -->
