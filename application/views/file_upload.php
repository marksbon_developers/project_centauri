            <!-- page content -->
            <div class="right_col" role="main">
                <div class="">
                     <div class="row">
                     <div class="col-md-6 ">
                        <div class="">
                            <div class="x_panel fixed_height_390">
                            <form action="Uploading" method="post" enctype="multipart/form-data">
                                <div class="x_title">
                                <div class="item form-group">
                                    <label  for="name">ATM Name <span class="required"></span>
                                    </label>
                                    <div class="">
                                        <select class="select2_single form-control txtbxvisible" tabindex="-1" name="atmId" required style="width:100%" data-placeholder="Select ATM">
                                            <option label="Select Atm"></option>
                                            <?php 
                                                $data = $this->EventsModel->atm_retrieve();
                
                                                if(!empty($data)) {
                                                    #code
                                                    $count = 1;
                                                    foreach ($data as $val) {
                                                        $atmname = $val->atmName;
                                            ?>
                                            <option value="<?php print $val->terminalid; ?>"><?php print $val->atmName; ?></option>
                                            <?php
                                                    $count++;
                                                    }
                                                                    
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <br />
                                    <h2 style="color:red">ATM Switch File</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <div class="dashboard-widget-content">
                                        <ul class="quick-list">
                                            <li><a style="color:red;font-size: 16px;text-decoration:none">NOTICE</a>
                                            </li>
                                            <li><i class="fa fa-check-square-o" style="color:red"></i><a>.CSV File Only</a>
                                            </li>
                                            <li><i class="fa fa-check-square-o" style="color:red"></i><a>Size Less Than 10MB</a> </li>
                                            <li><i class="fa fa-line-chart"></i><a>Achievements</a>
                                            </li>
                                            
                                            </li>
                                        </ul>

                                       <div class="sidebar-widget">
                                       <input type="file" class="hidden" id="atmuploadFile" name="csvfile" />
                                            <img class="" src="<?php echo base_url();?>/resources/images/upload.gif" width="59%" style="cursor: pointer" id="atmuploadimg">
                                             <input type="submit" name="atm" value="Save" class="btn btn-primary">
                                       </div>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="">
                            <div class="x_panel fixed_height_390">
                            <form action="Uploading" method="post" enctype="multipart/form-data">
                                <div class="x_title">
                                <div class="item form-group">
                                    <label  for="name">ATM Name <span class="required"></span>
                                    </label>
                                    <div class="">
                                        <select class="select2_single form-control txtbxvisible" tabindex="-1" name="atmId" required style="width:100%" data-placeholder="Select ATM">
                                            <option label="Select Atm"></option>
                                            <?php 
                                                $data = $this->EventsModel->atm_retrieve();
                
                                                if(!empty($data)) {
                                                    #code
                                                    $count = 1;
                                                    foreach ($data as $val) {
                                                        $atmname = $val->atmName;
                                            ?>
                                            <option value="<?php print $val->atmId; ?>"><?php print $val->atmName; ?></option>
                                            <?php
                                                    $count++;
                                                    }
                                                                    
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <br />
                                    <h2 style="color:red">BANK Switch File</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <div class="dashboard-widget-content">
                                        <ul class="quick-list">
                                            <li><a style="color:red;font-size: 16px;text-decoration:none">NOTICE</a>
                                            </li>
                                            <li><i class="fa fa-check-square-o" style="color:red"></i><a>.CSV File Only</a>
                                            </li>
                                            <li><i class="fa fa-check-square-o" style="color:red"></i><a>Size Less Than 10MB</a> </li>
                                            <li><i class="fa fa-line-chart"></i><a>Achievements</a>
                                            </li>
                                            
                                            </li>
                                        </ul>

                                       <div class="sidebar-widget">
                                       <input type="file" class="hidden" id="bankuploadfile" name="csvfile" />
                                            <img class="" src="<?php echo base_url();?>/resources/images/upload.gif" width="59%" style="cursor: pointer" id="bankuploadimg">
                                             <input type="submit" name="bank" value="Save" class="btn btn-primary">
                                       </div>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                     </div>
                    </div>

                </div>
                
