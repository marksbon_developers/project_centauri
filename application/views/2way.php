<!-- page content -->
<div class="right_col" role="main">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
    <?php
      if(isset($header))
        $display = "none";
      else
        $display = "block";
    ?>
      <div class="x_panel" style="display:<?php print $display; ?>">
        <div class="x_title">
          <h2> 2 Way Reconciliation</h2><br/><hr/>
          <div class="clearfix"></div>
          <form class="form-horizontal form-label-left"  action="Start" method="post" id="twoway">
            <input type="hidden" name="resulturl">
            <div class="row">
              <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="item form-group">
                  <label  for="name">Select Comparism <span class="required"></span></label>
                  <select class=" form-control txtbxvisible" data-placeholder="E-Journal Vs ATM Switch Vs Bank Host" name="comparism">
                    <option></option>
                    <option value="EJournal">E-Journal Vs ATM Switch</option>
                    <option value="Bank">Bank Host Vs ATM Switch</option>
                  </select>
                </div>
              </div>
              <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="item form-group">
                  <label  for="name">ATM Name <span class="required"></span></label>
                  <div class="">
                    <select class="select2_single form-control txtbxvisible" tabindex="-1" name="atmId" required style="width:100%" data-placeholder="Select ATM">
                      <option label="Select Atm"></option>
                      <?php 
                        $data = $this->EventsModel->atm_retrieve();
                                
                        if(!empty($data)) 
                        {
                          $count = 1;
                                                                    
                          foreach ($data as $val) 
                          {
                            $atmname = $val->atmName;
                                                                        
                                                                        
                            if($atmId == (int)$val->atmId)
                              $option = "selected";
                      ?>
                      <option value="<?php print $val->terminalid; ?>" <?php print @$option; @$option=""; ?>><?= $val->atmName; ?></option>
                      <?php
                            $count++;
                          }
                        }
                      ?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="item form-group">
                  <label  for="name">Date <span class="required"></span></label>
                  <div id="datetimepicker1" class="input-append date">
                    <input type="text" class="twowayWTime form-control txtbxvisible" name="searchtime"/>
                  </div>
                </div>
              </div>
              <div class="col-md-1">
                <div class="item form-group">
                  <label  for="name"> <span class="required" style="color: white;">*</span></label>
                  <div class="begin">
                    <input type="hidden" name="recon_type" value="2Way" />
                    <input class="btn btn-success"  name="startrecon" placeholder="" type="submit" style="display: none;" id="start2way"/>
                    <img class="" src="<?php echo base_url();?>/resources/images/start.png"   id="startbtnimg" style="width: 80px;margin-top: -38px;"/>
                  </div>
                </div>   
              </div>
            </div>
          </form>
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
      <?php
          if (!empty($result)) 
          {
            $counter = 1;
            
            foreach ($result as $value) 
            {
      ?>
                                        <div class="col-md-3 col-xs-12 widget widget_tally_box">
                                            <div class="x_panel fixed_height_390">
                                                <div class="x_title">
                                                <?php 

                                                    //retrieving atmName by TerminalIp
                                                    $data['search_text'] =  $value->terminalip; 
                                                    $result = $this->events_model->terminal_id_ip_retrieve($data);
                                                    foreach ($result as $name) {
                                                        # code...
                                                        $atmName = $name->atmName;
                                                    }

                                                ?>
                                                    <h2><?php print $atmName; ?></h2>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="x_content">
                                                <?php print $value->file; ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php
                                                    $counter++;
                                                }
                                            }
                                        ?>

                                </div>
                                <?php

                                            if (!empty($result)) {
                                                # code...
                                                $counter = 1;
                                                foreach ($result as $value) {
                                                    # code...
                                               
                                        ?>

                                        <div class="modal fade View<?php print $counter?>" tabindex="-1" role="dialog" aria-hidden="true">
                                            <div class="modal-dialog modal-sm">
                                                <div class="modal-content">

                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                                                        </button>
                                                        <h4 class="modal-title" id="myModalLabel2">Electronic Journal</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                    <form enctype="multipart/form-data">
                                                        <?php 
                                                        print $value->file; ?>
                                                    </form>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                                    $counter++;
                                                }
                                            }
                                        ?>
                            </div>
                        </div>
                        
                        <?php 
                            #code
                            if(!empty($recon_res)) {
                                
                        ?>

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Recon Result Summary</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <table id="example" class="table table-striped responsive-utilities jambo_table bulk_action" style="font-size: 14px;">
                                       <thead>
                                          <tr class="headings">
                                             <th class="column-title">Card No. </th>
                                             <th class="column-title">Acct No. </th>
                                             <th class="column-title">Start Time </th>
                                             <th class="column-title">End Time </th>
                                             <th class="column-title">Debit </th>
                                             <th class="column-title no-link last"><span class="nobr">Action</span>
                                             </th>
                                             <th class="bulk-actions" colspan="7">
                                                <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                             </th>
                                          </tr>
                                       </thead>
                                    <tbody>
                                    <?php 
                                        $counter = 1;
                                        foreach ($recon_res As $res) {
                                            #Checking For Errors and Highlighting
                                                #BANK 
                                            if(isset($Bank)) {
                                                
                                                $needle = "BANK-1";
                                                $haystack1 = strpos($res->results,$needle);
                                            
                                                $needle = "BANK-2";
                                                $haystack2 = strpos($res->results,$needle);
                                                
                                                if($haystack1 === false && $haystack2 === false)
                                                    $color = "";
                                                else
                                                $color="#dd4b39;color:white";
                                            }
                                            
                                            else {
                                                #E-Journal
                                                $needle = "EJ-1";
                                                $haystack1 = strpos($res->results,$needle);
                                                
                                                $needle = "EJ-2";
                                                $haystack2 = strpos($res->results,$needle);
                                                
                                                if( ($haystack1 === false && $haystack2 === false) ) 
                                                    $color = "";
                                                else
                                                    $color="#dd4b39;color:white";
                                            }
                                    ?>
                                       <tr class="even pointer" style="background-color: <?php print @$color;?>">
                                          <td class=" "><?php print substr_replace($res->atmcardnumber, str_repeat("*", 8), 4, 8); ?></td>
                                          <td class=""><?php print $res->atmaccountnumber; ?></td>
                                          <td class=""><?php print $res->atmstarttime; ?></td>
                                          <td class=""><?php print $res->atmendtime; ?></td>
                                          <td class=""><?php print $res->atmamountwithdrawn; ?></td>
                                          <td class=" last"><a style="color:#12673D;" href="" data-toggle="modal"  data-target=".details<?php print $counter; ?>"><i class="fa fa-th-list"></i> Details</a></td>
                                       </tr>
                                    <?php
                                            $counter++;
                                            }
                                    ?>
                                    </tbody>
                                 </table>
                              </div>
                            </div>
                        </div>
                        <?php
                            }
                        ?>
                        <br />
                        <br />
                        <br />
                        
                        <!-- ##################### Details Modal ########################## -->
                        <?php 
                        
                            if(!empty($recon_res)) {
                                
                                $counter = 1;
                                foreach ($recon_res As $res) {
                        ?>
                        
                        <div class="modal fade details<?php print $counter; ?>" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                        <h4 class="modal-title" id="myModalLabel">Reconcilation Results
                                        <div style="float: right;padding-right: 10px;font-size: 13;margin-right: 40px;margin-top: 4px;">
                                        <a href='#'><i class='fa fa-times red '></i></a> - Data Mismatch | 
                                        <a href='#'><i class='fa fa-warning yellow ' style='color: #73730D;'></i></a> - Unavailable Data | 
                                        <a href='#'><i class='fa fa-check green '></i></a> - Data Match
                                        </div></h4>
                                    </div>
                                            <div class="modal-body"> 
                                               
                                    <table class="table table-bordered responsive-utilities jambo_table bulk_action" style="font-size: 14px;text-align:center">
                                       <thead>
                                            <tr class="headings">
                                             <th class="column-title" style="text-align: center;font-size: 15px;">Fields</th>
                                             <?php if(isset($EJ)) { ?>
                                             <th class="column-title" style="text-align: center;">E-Journal Data</th>
                                             <th class="column-title" style="width: 5px;background-color:rgba(38, 185, 154, 0.07);">Status</th>
                                             <?php } ?>
                                             <th class="column-title" style="text-align: center;">ATM Switch Data</th>
                                             <?php if(isset($Bank)) { ?>
                                             <th class="column-title" style="width: 5px;background-color:rgba(38, 185, 154, 0.07);">Status</th>
                                             <th class="column-title" style="text-align: center;">Bank Switch Data</th>
                                             <?php } ?>
                                          </tr>
                                       </thead>
                                    <tbody>
                                        <?php 
                                            /*
                                            CardNumber:ATM-0#EJ-0#BANK-2;AccountNumber:ATM-0#EJ-0#BANK-2;StartTime:ATM-0#EJ-0#BANK-2;EndTime:ATM-0#EJ-0#BANK-2;AmountWithdrawn:ATM-0#EJ-0#BANK-2
                                            */
                                            #making Result an Array
                                            $resulte = explode(';',$res->results);
                                            /*
                                                [0]CardNumber:ATM-0#EJ-0#BANK-2
                                                [1]AccountNumber:ATM-0#EJ-0#BANK-2
                                                [2]StartTime:ATM-0#EJ-0#BANK-2
                                                [3]EndTime:ATM-0#EJ-0#BANK-2
                                                [4]AmountWithdrawn:ATM-0#EJ-0#BANK-2
                                            */
                                                
                                            foreach ($resulte As $col) {
                                            
                                                # Splitting
                                                $colname = explode(":",$col);
                                                /*
                                                    [0] => CardNumber
                                                    [1] => ATM-0#EJ-0#BANK-2
                                                */
                                                                                                
                                                #Creating Table-Row-1st Data
                                                #Code 
                                                switch($colname[0]) {
                                                        
                                                    case 'Card Number':
                                                        $edata = substr_replace($res->ejcardnumber, str_repeat("*", 8), 4, 8);
                                                        break;
                                                    case 'Account Number':
                                                        $edata = $res->ejaccountnumber;
                                                        break;
                                                    case 'Start Time':
                                                        $edata = $res->ejstarttime;
                                                            break;
                                                    case 'End Time':
                                                        $edata = $res->ejendtime;
                                                            break;
                                                    case 'Amount Withdrawn':
                                                        $edata = $res->ejamountwithdrawn;
                                                            break;
                                                }
                                                    
                                                print "
                                                    <tr class='even pointer'>
                                                        <td><strong>$colname[0]</strong></td>";
                                                if(isset($EJ)) {
                                                            print "<td>$edata</td>";
                                                        }
                                                
                                                # Splitting
                                                $fields = explode("#",$colname[1]);
                                                /*
                                                    [0] => ATM-0
                                                    [1] => EJ-0
                                                    [2] => BANK-2
                                                */
                                                
                                                #E-J Against ATM
                                                if($fields[1]) {
                                                    
                                                    $ejresult = substr($fields[1],'-1');
                                                    
                                                    #Code 
                                                    switch($ejresult) {
                                                        
                                                        case '0':
                                                            $ejcolorcode = "<a href='#' title='EJ & Switch Match'><i class='fa fa-check green fa-2x'></i></a>";
                                                            break;
                                                        case '1':
                                                            $ejcolorcode = "<a href='#' title='EJ MisMatch With Switch'><i class='fa fa-times red fa-2x'></i></a>";
                                                            break;
                                                        case '2':
                                                            $ejcolorcode = "<a href='#' title='UnAvailable'><i class='fa fa-warning yellow fa-2x' style='color: #73730D;'></i></a>";
                                                            break;
                                                    }
                                                }
                                                #Code 
                                                switch($colname[0]) {
                                                        
                                                    case 'Card Number':
                                                        $atmdata = substr_replace($res->atmcardnumber, str_repeat("*", 8), 4, 8);
                                                        break;
                                                    case 'Account Number':
                                                        $atmdata = $res->atmaccountnumber;
                                                        break;
                                                    case 'Start Time':
                                                        $atmdata = $res->atmstarttime;
                                                            break;
                                                    case 'End Time':
                                                        $atmdata = $res->atmendtime;
                                                            break;
                                                    case 'Amount Withdrawn':
                                                        $atmdata = $res->atmamountwithdrawn;
                                                            break;
                                                }
                                                    if(isset($EJ)) {
                                                            print "<td style='background-color:rgba(38, 185, 154, 0.07);;'>$ejcolorcode</td>";
                                                        }
                                                    print "
                                                    <td>$atmdata</td>
                                                    ";
                                                    
                                                    #Bank Against ATM
                                                    if($fields[2]) {
                                                    
                                                    $bankresult = substr($fields[2],'-1');
                                                    
                                                    #Code 
                                                    switch($bankresult) {
                                                        
                                                        case '0':
                                                            $bankcolorcode = "<a href='#' title='Bank Matches With Switch'><i class='fa fa-check green fa-2x'></i></a>";
                                                            break;
                                                        case '1':
                                                            $bankcolorcode = "<a href='#' title='Bank MisMatch With Switch'><i class='fa fa-times red fa-2x'></i></a>";
                                                            break;
                                                        case '2':
                                                            $bankcolorcode = "<a href='#' title='UnAvailable'><i class='fa fa-warning yellow fa-2x' style='color: #73730D;'></i></a>";
                                                            break;
                                                    }
                                                }
                                                #Code 
                                                switch($colname[0]) {
                                                        
                                                    case 'Card Number':
                                                        $bankdata = substr_replace($res->bankcardnumber, str_repeat("*", 8), 4, 8);
                                                        break;
                                                    case 'Account Number':
                                                        $bankdata = $res->bankaccountnumber;
                                                        break;
                                                    case 'Start Time':
                                                        $bankdata = $res->bankstarttime;
                                                            break;
                                                    case 'End Time':
                                                        $bankdata = $res->bankendtime;
                                                            break;
                                                    case 'Amount Withdrawn':
                                                        $bankdata = $res->bankamountwithdrawn;
                                                            break;
                                                }
                                                    if(isset($Bank)) { 
                                                        print "
                                                            <td style='background-color: rgba(38, 185, 154, 0.07);'>$bankcolorcode</td>;
                                                    
                                                            <td>$bankdata</td></tr>";
                                                    }
                                                
                                            }
                                            
                                        ?>
                                       
                                          
                                       <?php
                                       
                                       ?>
                                       
                                    </tbody>
                                 </table>
                              </div>
                              <div class="modal-footer">
                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                              </div>

                                        </div>
                                    </div>
                                </div>
                        <?php 
                                    $counter++;
                                }
                            }
                            

                        ?>



                </div>
            </div>
                    <!-- footer content -->
                
