<body class="nav-md">

    <div class="container body">

  <div class="main_container">

            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                    <div class="navbar nav_title" style="height: 0px;">
                        
                    </div>


                    <!-- menu prile quick info -->
                    <div class="profile">
                        <div class="profile_pic">
                            <img src="<?php print base_url(); ?>resources/images/logo.jpg" alt="..." class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            <h2 style="font-size:larger;">Company Name</h2>
                        </div>
                    </div>
                    <!-- /menu prile quick info -->
                    <div class="clearfix"></div>
                   

                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                            <ul class="nav side-menu">
                            <h3 class="nav-headings">General </h3>
                                <li><a><i class="fa fa-bar-chart-o"></i> Reports <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="Reports">Reports</a>
                                        </li>
                                        
                                    </ul>
                                </li>
                                <li><a href="Dashboard/Dispute_Manager"><i class="fa fa-comments"></i>Dispute Manager</a></li>
                            </ul>
                        </div>
                        <div class="menu_section">
                           <h3 class="nav-headings">Administration </h3>
                           <ul class="nav side-menu">
                                <li><a><i class="fa fa-home"></i> ATM Management <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="Dashboard/ATM_Add">Add ATM</a></li>
                                        <li><a href="Dashboard/ATM_Manage">Manage ATM</a></li>
                                        <li><a href="Dashboard/Add_Branches">Add Branches</a></li>
                                        <li><a href="Dashboard/Manage_Branches">Manage Branches</a></li>
                                        <li><a href="Dashboard/ATM_Groups">ATM Group</a></li>
                                        <li><a href="Dashboard/ATM_Manage_Groups">Manage ATM Group</a></li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-user"></i> User Management <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="Dashboard/User_Registration">Register User</a></li>
                                        <li><a href="Dashboard/User_Management">Manage Users</a></li>
                                    </ul>
                                </li>
                                <li><a><i class="fa fa-gears"></i> Settings <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="">Organization</a></li>
                                    </ul>
                                </li>
                                <li><a href="Dashboard/Feedback"><i class="fa fa-comment"></i>Feedback</a></li>
                            </ul>
                        </div>
                        <div class="menu_section">
                           <h3 class="nav-headings">System </h3>
                           <ul class="nav side-menu">
                              <!--<li><a><i class="fa fa-database"></i>Backup Data</a> 
                              </li>-->
                              <li><a><i class="fa fa-binoculars"></i> Audit <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="User_Logs">User Logs</a></li>
                                        <li><a href="System_Logs">System Logs</a></li>
                                    </ul>
                                </li>
                              <li><a><i class="fa fa-line-chart"></i> Upgrade <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a data-toggle="modal" data-target=".upgrade">Premium Package</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>

                    </div>
                    <!-- /sidebar menu -->

                    <!-- /menu footer buttons -->
                    <div class="sidebar-footer hidden-small">
                        <a data-toggle="tooltip" data-placement="top" title="Settings">
                            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                            <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Lock">
                            <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Logout">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                        </a>
                    </div>
                    <!-- /menu footer buttons -->
                </div>
         </div>

            <!-- top navigation -->
            <div class="top_nav">

                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                   <?php print $_SESSION['fullname']; ?>
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    <li>
                                        <a href="" data-toggle="modal" data-target=".changepwd">  Change Password</a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">Help</a>
                                    </li>
                                    <li><a href="logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                    </li>
                                </ul>
                                <div class="modal fade changepwd" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog modal-sm" style="width:40%">
                                        <div class="modal-content">
                                            <form class="form-horizontal" action="Access/Update_Password" method="post" novalidate>
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">�</span>
                                                    </button>
                                                    <h4 class="modal-title" id="myModalLabel2">Change Password</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="item form-group">
                                                        <label class="col-md-3 col-sm-3 col-xs-12" for="Current">Current Password <span class="required"></span>
                                                        </label>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <input type="password" id="password" name="CurrentPwd" required="required" class="form-control col-md-7 col-xs-12 txtbxvisible">
                                                        </div>
                                                    </div>
                                                    <div class="item form-group">
                                                        <label class="col-md-3 col-sm-3 col-xs-12" for="password">Password <span class="required"></span>
                                                        </label>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <input type="password" id="password" name="password" required="required" class="form-control col-md-7 col-xs-12 txtbxvisible">
                                                        </div>
                                                    </div>
                                                        <div class="item form-group">
                                                            <label class="col-md-3 col-sm-3 col-xs-12" for="password">Confirm Password <span class="required"></span>
                                                            </label>
                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                <input type="password" id="password2" name="confirm_password" data-validate-linked="password" required="required" class="form-control col-md-7 col-xs-12 txtbxvisible">
                                                            </div>
                                                        </div>
                                                        <input type="hidden" name="userid" value="<?php print $_SESSION['userid']; ?>">
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary" name="Update_Pwd">Save changes</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </nav>
                </div>

            </div>
            <!-- /top navigation -->

<!-- page content -->
            <div class="right_col" role="main">
                <div class="">

                    <div class="row">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2> Organization Info</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <form class="form-horizontal form-label-left col-md-9"  action="<?php print $_SESSION['form_link']; ?>" method="post" enctype="multipart/form-data">

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Company Name <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input class="form-control col-md-7 col-xs-12 txtbxvisible"  name="comp_name" placeholder="GreenSun Bank"  type="text" value="<?php echo @$_SESSION['compinfo']['name']; ?>">
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="id">Telephone 1 <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" name="comp_tel1" class="form-control col-md-7 col-xs-12 txtbxvisible" placeholder="(+233) 541-786-220" value="<?php echo @$_SESSION['compinfo']['tel1']; ?>">
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ip">Telephone 2 <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" name="comp_tel2" class="form-control txtbxvisible" id=""  placeholder="(+233) 541-786-221" value="<?php echo @$_SESSION['compinfo']['tel2']; ?>"/>
                                                
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ip">Fax <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" name="comp_fax" class="form-control txtbxvisible" id=""  placeholder="(+233) 541-786-220" value="<?php echo @$_SESSION['compinfo']['fax']; ?>"/>
                                                
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ip">Email <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" name="comp_email" class="form-control txtbxvisible" id=""  placeholder="info@domain.com" value="<?php echo @$_SESSION['compinfo']['email']; ?>"/>
                                                
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Location <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" name="comp_loc"  class="form-control col-md-7 col-xs-12 txtbxvisible" placeholder="2nd Floor, Afua Pokuaa House" value="<?php echo @$_SESSION['compinfo']['location']; ?>">
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Postal Address <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" name="comp_address" class="form-control col-md-7 col-xs-12 txtbxvisible" placeholder="P.O.Box TM 47, Dawenya - Tema." value="<?php echo @$_SESSION['compinfo']['address']; ?>">
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="number">Company Logo <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="file" name="comp_logo" class="form-control col-md-7 col-xs-12 txtbxvisible" value="<?php echo @$_SESSION['compinfo']['logo']; ?>">
                                            </div>
                                        </div>
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-md-offset-5">
                                                <a href="../Dashboard"><button type="button" class="btn btn-primary">Cancel</button></a>
                                                <input type="submit" class="btn btn-success" name="organization" value="<?php print $_SESSION['button_name']?>">
                                            </div>
                                        </div>
                                    </form>
                                    
                                    <div class="col-md-3">
                                        <?php 
                                            if(!empty($companyinfo)){
                                                        
                                                foreach($companyinfo as $company){
                                        ?>
                                        <?php  print '<img src="data:image/jpeg;base64,'.base64_encode($company->logo).' height="150" width="150" alt="Company Logo" style="margin-left: 7%;/>'; ?>
                                        <small class="pull-right" style="font-size:14PX;">
                                          <address>
                                            <strong><?php echo $company->name; ?></strong><br>
                                            <?php echo $company->address; ?><br>
                                            Phone: <?php echo $company->tel_1; ?><br/>
                                            Phone: <?php echo $company->tel_1; ?><br/>
                                            Fax  : <?php echo $company->fax; ?><br/>
                                            Email: <?php echo $company->email; ?>
                                            Location: <?php echo $company->location; ?>
                                          </address>
                                        </small>
                                        <?php  
                                            }
                                          }
                                          unset($_SESSION['compinfo']);
                                        ?>
                                    </div>
                                    <div class="clearfix"></div>

                                </div>
                            </div>
                        </div>
                        
                        

                        <br />
                        <br />
                        <br />

                    </div>
                </div>
                    <!-- footer content -->
                
