<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2> Search Transaction</h2><br><hr>
            <div class="clearfix"></div>
              <form class="form-horizontal form-label-left" action="Transaction_Search" method="post">
                <div class="row">
                  <div class="col-md-3">
                    <div class="item form-group">
                      <label  for="name">ATM Name / TerminalIP / Terminal ID <span class="required">*</span></label>
                      <!--<input class="form-control txtbxvisible"  name="search_text" placeholder="Name / Terminal ID / Termianl IP" type="text" value="<?php print @$_SESSION['name']; ?>" required>-->
                      <select class="select2_single form-control txtbxvisible" name="search_text" data-placeholder="--- Select One ----">
                        <option></option>
                        <?php if(!empty($atminfo_display)) : foreach($atminfo_display As $display) : ?>
                        <option><?= $display ?></option>
                        <?php endforeach; endif; ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="item form-group">
                      <label  for="name">Card No <span class="required">*</span></label>
                      <div class="">
                        <input class="form-control txtbxvisible searchbox" autocomplete="off"  name="CardNo" placeholder="" type="text" value="<?php print @$_SESSION['cardno']; ?>" required>
                        <div class="result" style="margin-top:-10px" style="width:100% !important"></div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="item form-group">
                      <label  for="name">Date <span class="required">*</span></label>
                      <div class="input-prepend input-group" >
                        <span class="add-on input-group-addon" style="background: none;"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                        <input type="text" id="reportrange" name="daterange" class="form-control twowaydate" value="<?php print @$_SESSION['date']; ?>" required>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="item form-group">
                      <label  for="name"> <span class="required">*</span></label>
                      <div class="">
                        <input class="btn btn-success"  name="search" placeholder="" type="submit">
                      </div>
                    </div>   
                  </div>
                </div>
                <input type="hidden" name="request_type" value="transaction">
              </form>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <?php
              @$result = $_SESSION['result'];

              if (!empty($result)) 
              {
                $counter = 1;
                foreach($result as $value) 
                {
                      # code...
            ?>
            <div class="col-md-3 col-xs-12 widget widget_tally_box">
              <div class="x_panel ">
                <div class="x_title">
                  <?php 
                    $data['search_text'] =  $value->terminalip; 
                    
                    $result = $this->EventsModel->terminal_id_ip_retrieve($data);
                                                    
                    if(!empty($result)) 
                      $atmName = $result->atmName;                                  
                    
                    else 
                      $atmName = "Not Set";
                  ?>
                  <h2><?php print @$atmName; ?></h2>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="x_content">
                                                <?php print $value->file; ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php
                                                    $counter++;
                                                }
                                            }
                                        ?>

                                </div>
                                <?php

                                            if (!empty($result)) {
                                                # code...
                                                $counter = 1;
                                                foreach ($result as $value) {
                                                    # code...
                                               
                                        ?>

                                        <div class="modal fade View<?php print $counter?>" tabindex="-1" role="dialog" aria-hidden="true">
                                            <div class="modal-dialog modal-sm">
                                                <div class="modal-content">

                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                                                        </button>
                                                        <h4 class="modal-title" id="myModalLabel2">Electronic Journal</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                    <form enctype="multipart/form-data">
                                                        <?php 
                                                        print $value->file; ?>
                                                    </form>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                                    $counter++;
                                                }
                                            }
                                            //unsetting Sesion Variables
                                            unset($_SESSION['name']);
                                		 	unset($_SESSION['cardno']);
                                		 	unset($_SESSION['date']);
                                      unset($_SESSION['result']);
                                        ?>
                            </div>
                        </div>

                        <br />
                        <br />
                        <br />

                    </div>
                </div>
                    <!-- footer content -->
                
