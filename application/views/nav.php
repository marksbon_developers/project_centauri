<body class="nav-sm">
  <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col">
        <div class="left_col ">
          <div class="navbar nav_title" style="">
            <a href="<?= base_url() ?>Dashboard" class="site_title">
              <img src="<?php print base_url(); ?>resources/images/app_logo.jpg" style="border-radius:5px;"/> 
              <span><strong style="font-size: 28px;">&alpha;</strong> Centauri</span>
            </a>
          </div>
          <div class="clearfix"></div>
          <!-- sidebar menu -->
          <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
              <ul class="nav side-menu">
                <li><a href="<?= base_url() ?>Dashboard"><i class="fa fa-dashboard"></i>Dashboard</a></li>
                <h3 class="nav-headings">General </h3>
                <li><a href="<?= base_url() ?>Dashboard/Dispute_Manager"><i class="fa fa-comments"></i>Dispute Manager</a></li>
                
                <?php if(isset($_SESSION['username']) && ($_SESSION['role'] == 'Administrator' || $_SESSION['role'] == 'GreenSunAdmin') ) { ?>
                
                <li><a><i class="fa fa-bar-chart-o"></i> Reports <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: none">
                    <li><a href="<?= base_url() ?>Report">EJ Manager</a></li>
                    <li><a href="#">Reconciliation</a></li>
                    <li><a href="#">Cash Manager</a></li>
                  </ul>
                </li>
                <!--<li><a><i class="fa fa-binoculars"></i> Audit <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: none">
                    <li><a href="User_Logs">User Logs</a></li>
                    <li><a href="System_Logs">System Logs</a></li>
                  </ul>
                </li>-->
                <li><a href="<?= base_url() ?>Report/Feedback"><i class="fa fa-comment"></i>Feedback</a></li>
              </ul>
            </div>
            <div class="menu_section">
              <h3 class="nav-headings">EJ Manager </h3>
              <ul class="nav side-menu">
                <li><a href="<?= base_url() ?>Dashboard/Transaction"><i class="glyphicon glyphicon-tasks"></i> Transactions</a></li>
                <li><a href="<?= base_url() ?>Dashboard/Logs"><i class="fa fa-cogs"></i> Logs</a></li>
                <li><a href="<?= base_url() ?>Report"><i class="fa fa-bar-chart-o"></i> Report</a></li>
              </ul>
            </div>
            <div class="menu_section">
              <h3 class="nav-headings">Reconciliation </h3>
              <ul class="nav side-menu">
                <li><a href="<?= base_url() ?>Reconciliation/Upload"><i class="fa fa-file"></i> File Upload</a></li>
                <li><a href="<?= base_url() ?>Reconciliation/D_Comparism"><i class="fa fa-th-large"></i> 2-Way Recon</a></li>
                <li><a href="<?= base_url() ?>Reconciliation/T_Comparism"><i class="fa fa-th"></i> 3-Way Recon</a></li>
              </ul>
            </div>
            <div class="menu_section">
              <h3 class="nav-headings">System Settings </h3>
              <ul class="nav side-menu">
                <li><a><i class="fa fa-home"></i> ATM Management <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: none">
                    <li><a href="<?= base_url() ?>Administration/New_ATM">Add ATM</a></li>
                    <li><a href="<?= base_url() ?>Administration/Manage_ATM">Manage ATM</a></li>
                    <li><a href="<?= base_url() ?>Administration/New_Branch">Add Branches</a></li>
                    <li><a href="<?= base_url() ?>Administration/Manage_Branch">Manage Branches</a></li>
                    <li><a href="<?= base_url() ?>Administration/New_Group">ATM Group</a></li>
                    <li><a href="<?= base_url() ?>Administration/Manage_Groups">Manage ATM Group</a></li>
                  </ul>
                </li>
                <li><a><i class="fa fa-user"></i> User Management <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: none">
                    <li><a href="<?= base_url() ?>Administration/User_Registration">Register User</a></li>
                    <li><a href="<?= base_url() ?>Administration/User_Management">Manage Users</a></li>
                  </ul>
                </li>
              </ul>
            </div>
            <div class="menu_section">
              <h3 class="nav-headings">System </h3>
              <ul class="nav side-menu">
                <li><a><i class="fa fa-line-chart"></i> Upgrade <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu" style="display: none">
                    <li><a data-toggle="modal" data-target=".upgrade">Premium Package</a></li>
                  </ul>
                </li>
                <li><a href="<?= base_url() ?>Administration/Settings"><i class="fa fa-gears"></i> Settings</a></li>
              <?php } ?>
              
              </ul>
            </div>
          </div>
          <!-- /sidebar menu -->
        </div>
      </div>

            <!-- top navigation -->
            <div class="top_nav">

                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                   <?php print $_SESSION['fullname']; ?>
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    <li>
                                        <a href="" data-toggle="modal" data-target=".changepwd">  Change Password</a>
                                    </li>
                                    <li><a href="logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>

            </div>
            <!-- /top navigation -->
