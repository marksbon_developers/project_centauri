<body class="nav-md">

    <div class="container body">

  <div class="main_container">

            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">

                    <div class="navbar nav_title" style="">
                        <a href="../Dashboard" class="site_title"><img src="<?php print base_url(); ?>resources/images/app_logo.jpg" alt="..." class="" style="margin:4% 0 0 4%;border-radius:5px;"/> <span><strong style="font-size: 28px;">&alpha;</strong> Centauri</span></a>
                    </div>
                    <div class="clearfix"></div>


                    <!-- menu prile quick info -->
                    <div class="profile">
                        <div class="profile_pic">
                            <img src="<?php print base_url(); ?>resources/images/logo.jpg" alt="..." class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            <h2 style="font-size:larger;">Company Name</h2>
                        </div>
                    </div>
                    <!-- /menu prile quick info -->

                    <br />

                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                        <?php 
                            //User Priviledges Check
                            if($_SESSION['role'] != "User") {
                        ?>
                        <div class="menu_section">
                            <br><br><br>
                            <ul class="nav side-menu">
                                <li><a href="Upload"><i class="fa fa-file"></i> File Upload
                                    </a>
                                 </li>
                                  <li><a href="#"><i class="fa fa-th-large"></i> 2-Way Recon
                                    </a>
                                 </li>
                                 <li><a href="3_Way"><i class="fa fa-th"></i> 3-Way Recon
                                    </a>
                                 </li>
                            </ul>
                        </div>
                        <?php 
                            }
                        ?>
                        

                    </div>
                    <!-- /sidebar menu -->

                    <!-- /menu footer buttons -->
                    <div class="sidebar-footer hidden-small">
                        <a data-toggle="tooltip" data-placement="top" title="Settings">
                            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                            <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Lock">
                            <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Logout">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                        </a>
                    </div>
                    <!-- /menu footer buttons -->
                </div>
            </div>

            <!-- top navigation -->
            <div class="top_nav">

                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                   <?php print $_SESSION['fullname']; ?>
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    <li>
                                        <a href="" data-toggle="modal" data-target=".changepwd">  Change Password</a>
                                    </li>
                                    <li>
                                        <a href="javascript:;">Help</a>
                                    </li>
                                    <li><a href="logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                    </li>
                                </ul>
                                <div class="modal fade changepwd" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog modal-sm" style="width:40%">
                                        <div class="modal-content">
                                            <form class="form-horizontal" action="Access/Update_Password" method="post" novalidate>
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">�</span>
                                                    </button>
                                                    <h4 class="modal-title" id="myModalLabel2">Change Password</h4>
                                                </div>
                                                <div class="modal-body">
                                                    
                                                        <div class="item form-group">
                                                            <label class="col-md-3 col-sm-3 col-xs-12" for="Current">Current Password <span class="required"></span>
                                                            </label>
                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                <input type="password" id="password" name="CurrentPwd" required="required" class="form-control col-md-7 col-xs-12 txtbxvisible">
                                                            </div>
                                                        </div>
                                                        <div class="item form-group">
                                                            <label class="col-md-3 col-sm-3 col-xs-12" for="password">Password <span class="required"></span>
                                                            </label>
                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                <input type="password" id="password" name="password" required="required" class="form-control col-md-7 col-xs-12 txtbxvisible">
                                                            </div>
                                                        </div>
                                                        <div class="item form-group">
                                                            <label class="col-md-3 col-sm-3 col-xs-12" for="password">Confirm Password <span class="required"></span>
                                                            </label>
                                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                                <input type="password" id="password2" name="confirm_password" data-validate-linked="password" required="required" class="form-control col-md-7 col-xs-12 txtbxvisible">
                                                            </div>
                                                        </div>
                                                        <input type="hidden" name="userid" value="<?php print $_SESSION['userid']; ?>">
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary" name="Update_Pwd">Save changes</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </nav>
                </div>

            </div>
            <!-- /top navigation -->

<!-- page content -->
            <div class="right_col" role="main">
                <div class="">

                    <div class="row">
                        <form action="Uploading" method="post" enctype="multipart/form-data">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                <h2> 2 Way Reconciliation</h2><br>
                                <hr>
                                <div class="clearfix"></div>
                                    <form class="form-horizontal form-label-left" novalidate action="Transaction_Search" method="post">
                                        <div class="row">
                                          <div class="col-md-4">
                                              <div class="item form-group">
                                                  <label  for="name">Comparism <span class="required">*</span>
                                                  </label>
                                                  <select id="comparism" class="select2_single form-control txtbxvisible" tabindex="-1" name="recon_type" required style="width:100%" data-placeholder="Select Comparism" onchange="change(this)">
                                                    <option></option>
                                                    <option value="atm">E-JOURNAL Vs ATM SWITCH</option>
                                                    <option value="bank">ATM SWITCH Vs BANK SWITCH</option>
                                                  </select>
                                              </div>
                                              <div class="item form-group">
                                                    <label  for="name">ATM Name/ IP / ID <span class="required"></span>
                                                    </label>
                                                    <div class="">
                                                        <select class="select2_single form-control txtbxvisible" tabindex="-1" name="atm_id" required style="width:100%" data-placeholder="Select ATM">
                                                            <option label="Select Atm"></option>
                                                            <?php 
                                                                $data = $this->EventsModel->atm_retrieve();
                
                                                                if(!empty($data)) {
                                                                    #code
                                                                   $count = 1;
                                                                    foreach ($data as $val) {
                                                                        $atmname = $val->atmName;
                                                            ?>
                                                            <option value="<?php print $val->atmId; ?>"><?php print $val->atmName; ?></option>
                                                            <?php
                                                                        $count++;
                                                                    }
                                                                    
                                                                }
                                                            ?>
                                                    </select>
                                                    </div>
                                                </div>
                                                <div class="item form-group">
                                                    <label  for="name">Date <span class="required">*</span>
                                                    </label>
                                                    <div class="input-prepend input-group txtbxvisible" >
                                                        <span class="add-on input-group-addon" style="background: none;"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                        <input type="text"  name="daterange" id="reportrange" class="form-control txtbxvisible" value="<?php print @$_SESSION['date']; ?>">
                                                    </div>
                                                </div>
                                          </div>
                                            <div class="col-md-5">
                                                <!----------------File Upload Section ------------>
                                                <div class="row">
                                                     <div class="" id="uploadnotice">
                                                        <div class="">
                                                            <div class="x_panel fixed_height_320">
                                                                <div class="x_title">
                                                                    <h2 style="color:red">File Type Upload Pending</h2>
                                                                    <div class="clearfix"></div>
                                                                </div>
                                                                <div class="x_content">
                                                                    <div class="dashboard-widget-content">
                                                                        <ul class="quick-list">
                                                                            <li><i class="fa fa-calendar-o"></i><a href="#" style="color:red">NOTICE</a>
                                                                            </li>
                                                                            <li><i class="fa fa-check-square-o" style="color:red"></i><a href="#">.CSV or Excel File Only</a>
                                                                            </li>
                                                                            <li><i class="fa fa-check-square-o" style="color:red"></i><a href="#">File Size Less Than 5MB</a> </li>
                                                                            <li><i class="fa fa-line-chart"></i><a href="#">Achievements</a>
                                                                            </li>
                                                                            <li><i class="fa fa-bar-chart"></i><a href="#">Auto Renewal</a> </li>
                                                                            <li><i class="fa fa-line-chart"></i><a href="#">Achievements</a>
                                                                            </li>
                                                                            <li><i class="fa fa-area-chart"></i><a href="#">Logout</a>
                                                                            </li>
                                                                        </ul>
                                
                                                                       <div class="sidebar-widget">
                                                                          <img class="" src="<?php echo base_url();?>/resources/images/upload.png" width="59%" >
                                                                       </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                     </div>
                                                     <div class="" id="atmfield" style="display: none;">
                                                        <div class="">
                                                            <div class="x_panel fixed_height_320">
                                                                <div class="x_title">
                                                                    <h2 style="color:red">ATM Switch File</h2>
                                                                    <div class="clearfix"></div>
                                                                </div>
                                                                <div class="x_content">
                                                                    <div class="dashboard-widget-content">
                                                                        <ul class="quick-list">
                                                                            <li><i class="fa fa-calendar-o"></i><a href="#" style="color:red">NOTICE</a>
                                                                            </li>
                                                                            <li><i class="fa fa-check-square-o" style="color:red"></i><a href="#">.CSV or Excel File Only</a>
                                                                            </li>
                                                                            <li><i class="fa fa-check-square-o" style="color:red"></i><a href="#">File Size Less Than 5MB</a> </li>
                                                                            <li><i class="fa fa-line-chart"></i><a href="#">Achievements</a>
                                                                            </li>
                                                                            <li><i class="fa fa-bar-chart"></i><a href="#">Auto Renewal</a> </li>
                                                                            <li><i class="fa fa-line-chart"></i><a href="#">Achievements</a>
                                                                            </li>
                                                                            <li><i class="fa fa-area-chart"></i><a href="#">Logout</a>
                                                                            </li>
                                                                        </ul>
                                
                                                                       <div class="sidebar-widget">
                                                                          <input type="file" class="hidden" id="uploadFilee" name="atmcsvfile" />
                                                                          <img class="" src="<?php echo base_url();?>/resources/images/upload.gif" width="59%" style="cursor: pointer" id="uploadimgg">
                                                                       </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                     </div>
                                                     <div class="" id="bankfield" style="display: none;">
                                                        <div class="">
                                                            <div class="x_panel fixed_height_320">
                                                                <div class="x_title">
                                                                    <h2 style="color:red">Bank Host File </h2>
                                                                    <div class="clearfix"></div>
                                                                </div>
                                                                <div class="x_content">
                                                                    <div class="dashboard-widget-content">
                                                                        <ul class="quick-list">
                                                                            <li><i class="fa fa-calendar-o"></i><a href="#" style="color:red">NOTICE</a>
                                                                            </li>
                                                                            <li><i class="fa fa-check-square-o" style="color:red"></i><a href="#">.CSV or Excel File Only</a>
                                                                            </li>
                                                                            <li><i class="fa fa-check-square-o" style="color:red"></i><a href="#">File Size Less Than 5MB</a> </li>
                                                                            <li><i class="fa fa-line-chart"></i><a href="#">Achievements</a>
                                                                            </li>
                                                                            <li><i class="fa fa-bar-chart"></i><a href="#">Auto Renewal</a> </li>
                                                                            <li><i class="fa fa-line-chart"></i><a href="#">Achievements</a>
                                                                            </li>
                                                                            <li><i class="fa fa-area-chart"></i><a href="#">Logout</a>
                                                                            </li>
                                                                        </ul>
                                
                                                                       <div class="sidebar-widget">
                                                                          <input type="file" class="hidden" id="uploadFile" name="csvfile" />
                                                                          <img class="" src="<?php echo base_url();?>/resources/images/upload.gif" width="59%" style="cursor: pointer" id="uploadimg">
                                                                       </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                     </div>
                                                </div>
                                                <!----------------/File Upload Section ------------>
                                            </div>
                                            <div class="col-md-1">
                                                <div class="item form-group">
                                                    <label  for="name"> <span class="required" style="color: #fff;">*</span>
                                                    </label>
                                                    <div class="begin">
                                                        <input type="hidden" class="btn btn-success"  name="search" placeholder="" type="submit" value="Begin Process">
                                                        <img class="" src="<?php echo base_url();?>/resources/images/start.png"   id="uploadimg">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="request_type" value="transaction">
                                    </form>
                                    
                                    
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            
                            
                                <div class="x_content">
                                    <?php

                                        if (!empty($result)) {
                                            # code...
                                            $counter = 1;
                                            foreach ($result as $value) {
                                                # code...
                                               
                                    ?>
                                        <div class="col-md-3 col-xs-12 widget widget_tally_box">
                                            <div class="x_panel fixed_height_390">
                                                <div class="x_title">
                                                <?php 

                                                    //retrieving atmName by TerminalIp
                                                    $data['search_text'] =  $value->terminalip; 
                                                    $result = $this->events_model->terminal_id_ip_retrieve($data);
                                                    foreach ($result as $name) {
                                                        # code...
                                                        $atmName = $name->atmName;
                                                    }

                                                ?>
                                                    <h2><?php print $atmName; ?></h2>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="x_content">
                                                <?php print $value->file; ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php
                                                    $counter++;
                                                }
                                            }
                                        ?>

                                </div>
                                <?php

                                            if (!empty($result)) {
                                                # code...
                                                $counter = 1;
                                                foreach ($result as $value) {
                                                    # code...
                                               
                                        ?>

                                        <div class="modal fade View<?php print $counter?>" tabindex="-1" role="dialog" aria-hidden="true">
                                            <div class="modal-dialog modal-sm">
                                                <div class="modal-content">

                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">�</span>
                                                        </button>
                                                        <h4 class="modal-title" id="myModalLabel2">Electronic Journal</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                    <form enctype="multipart/form-data">
                                                        <?php 
                                                        print $value->file; ?>
                                                    </form>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                                    $counter++;
                                                }
                                            }
                                        ?>
                            </div>
                            </form>
                        </div>

                        <br />
                        <br />
                        <br />

                        <div class="modal fade details" tabindex="-1" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">

                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">�</span>
                                                </button>
                                                <h4 class="modal-title" id="myModalLabel">InConstitencies</h4>
                                            </div>
                                            <div class="modal-body">
                                               
                                    <table class="table table-striped responsive-utilities jambo_table bulk_action">
                                       <thead>
                                          <tr class="headings">
                                             <th class="column-title">File 1</th>
                                             <th class="column-title">File 1 Field</th>
                                             <th class="column-title">File 2</th>
                                             <th class="column-title">Amount </th>
                                             <th class="column-title no-link last"><span class="nobr">Description</span>
                                             </th>
                                             <th class="bulk-actions" colspan="7">
                                                <a class="antoo" style="color:#fff; font-weight:500;">Bulk Actions ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
                                             </th>
                                          </tr>
                                       </thead>
                                    <tbody>
                                       <tr class="even pointer">
                                          <td class=" ">Paid</td>
                                          <td class="a-right a-right ">$7.45</td>
                                          <td class=" last"><a href="#">View</a></td>
                                       </tr>
                                       <tr class="odd pointer">
                                          <td class=" ">Paid</td>
                                          <td class="a-right a-right ">$741.20</td>
                                          <td class=" last"><a href="#">View</a></td>
                                       </tr>
                                       <tr class="even pointer">
                                          <td class=" ">Paid</td>
                                          <td class="a-right a-right ">$432.26</td>
                                          <td class=" last"><a href="#">View</a></td>
                                       </tr>
                                    </tbody>
                                 </table>
                              </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-primary">Save changes</button>
                                            </div>

                                        </div>
                                    </div>
                                </div>



                </div>
            </div>
                    <!-- footer content -->
                
