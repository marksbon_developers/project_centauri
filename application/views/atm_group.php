

            <!-- page content -->
            <div class="right_col" role="main">

                <div class="">

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>NEW ATM GROUP</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">

                                    <form class="form-horizontal form-label-left" novalidate action="" method="post" >

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Group Name <span class="required" style="color: red;">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input class="form-control col-md-7 txtbxvisible" readonly name="group_name" placeholder="Tema Region" type="text" >
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="id">Description <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <textarea class="form-control col-md-7 col-xs-12 txtbxvisible" readonly placeholder="All (Contains All ATM's In Tema Region)" name="desc" required="required" style="resize:none;" rows="4" ></textarea>
                                            </div>
                                        </div>
                                        
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-md-offset-5">
                                                <a href="../Dashboard"><button type="button" class="btn btn-primary">Cancel</button></a>
                                                <!--<button id="send" type="submit" class="btn btn-success" name="create_grp">Create</button> -->
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- footer content -->
