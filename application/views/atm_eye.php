<body class="nav-md">
  <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col">
        <div class="left_col scroll-view">
            <div class="navbar nav_title" style="">
                <a href="../Dashboard" class="site_title"><img src="<?php print base_url(); ?>resources/images/app_logo.jpg" /> 
                  <span><strong style="font-size: 28px;">&alpha;</strong> Centauri</span>
                </a>
            </div>
            <div class="clearfix"></div>
              <!-- /menu prile quick info -->
              <div class="clearfix"></div>
              <!-- sidebar menu -->
              <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                <div class="menu_section">
                  <ul class="nav side-menu">
                    <form action="ATM_Eye_Search" method="post">
                      <h3 class="nav-headings">ATM Images Search </h3>
                     <li style="margin: 10px;">
                      <select class="select2_single form-control txtbxvisible" name="atmid" data-placeholder="Select Atm" required="required">
                        <option></option>
                        <?php 
                          $data = $this->EventsModel->atm_retrieve();
                          if(!empty($data)) 
                          {
                            $counter = 1;
                            foreach ($data as $value) 
                            {
                              print "<option value='{$value->terminalid}'>{$value->atmName}</option>";
                            }
                          }
                        ?>
                      </select>
                    </li>
                    <li style="margin: 10px;">
                      <select class="select2_single form-control txtbxvisible" name="atmtype" data-placeholder="Select Atm Type">
                       <option></option>
                       <option>All </option>
                       <option>Wincor </option>
                       <option>NCR </option>
                      </select>
                    </li>
                    <li style="margin: 10px;">
                      <select class="select2_single form-control txtbxvisible" name="mediatype" data-placeholder="Select Media Type">
                        <option></option>
                        <option>Pictures </option>
                        <option>Video </option>
                      </select>
                    </li>
                    <li style="margin: 10px;"><input class="form-control txtbxvisible"  name="CardNo" placeholder="Enter Card Number" type="text" style="border-radius: 5px;"/>
                    </li>
                    <li style="margin: 10px;"><input class="form-control txtbxvisible"  name="daterange" placeholder="Enter ATM Name" type="text" style="border-radius: 5px;" required="required"/>
                    </li>
                    <input class="btn btn-success"  name="search_image" type="submit" value="Search" style="margin-left: 30px;width: 160px;"/>
                  </form>
                </ul>
              </div>
            </div>
            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                  <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                  <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                  <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout">
                  <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
          </div>
        </div>
        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav class="" role="navigation">
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>
              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <?php print $_SESSION['fullname']; ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                    <li>
                      <a href="" data-toggle="modal" data-target=".changepwd">  Change Password</a>
                    </li>
                    <li>
                      <a href="javascript:;">Help</a>
                    </li>
                    <li><a href="logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                    </li>
                  </ul>
                  <div class="modal fade changepwd" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm" style="width:40%">
                      <div class="modal-content">
                        <form class="form-horizontal" action="Access/Update_Password" method="post" novalidate>
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                            </button>
                            <h4 class="modal-title" id="myModalLabel2">Change Password</h4>
                          </div>
                          <div class="modal-body">
                            <div class="item form-group">
                              <label class="col-md-3 col-sm-3 col-xs-12" for="Current">Current Password <span class="required"></span>
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="password" id="password" name="CurrentPwd" required="required" class="form-control col-md-7 col-xs-12 txtbxvisible">
                              </div>
                            </div>
                            <div class="item form-group">
                              <label class="col-md-3 col-sm-3 col-xs-12" for="password">Password <span class="required"></span>
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="password" id="password" name="password" required="required" class="form-control col-md-7 col-xs-12 txtbxvisible">
                              </div>
                            </div>
                            <div class="item form-group">
                              <label class="col-md-3 col-sm-3 col-xs-12" for="password">Confirm Password <span class="required"></span>
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="password" id="password2" name="confirm_password" data-validate-linked="password" required class="form-control col-md-7 col-xs-12 txtbxvisible">
                              </div>
                            </div>
                            <input type="hidden" name="userid" value="<?php print $_SESSION['userid']; ?>">
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary" name="Update_Pwd">Save changes</button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
               </li>
            </ul>
          </nav>
        </div>
      </div>
      <!-- /top navigation -->

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2> ATM IMAGES SEARCH 
                  <img src="file://c:/yes/jy.jpg" /?
                  </h2>
                  <br/>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">
                  <?php
                    if($this->session->flashdata('trans_result')  )
                    {
                      $result       = $this->session->flashdata('trans_result');

                      $oth_rev_info = $this->session->flashdata('oth_rev_info');

                      $media_res    = $this->session->flashdata('media_result');

                      $counter = 1;
                  ?>
                  <table class="table table-striped responsive-utilities jambo_table" style="font-size: 14px;">
                    <thead>
                      <th>No. #</th>
                      <th>Start Time</th>
                      <th>End Time</th>
                      <th>Terminal ID</th>
                      <th>Card No.</th>
                      <th>No. Of Images</th>
                      <th>Result</th>
                    </thead>
                    <tbody>
                      <?php
                        foreach ($result as $value) 
                        {
                      ?>
                          <tr>
                            <td><?= $counter; ?></td>
                            <td><?= $value->AtmSTime;?></td>
                            <td><?= $value->AtmETime;?></td>
                            <td><?= $oth_rev_info['terminalid'];?></td>
                            <td><?= $oth_rev_info['CardNo'];?></td>
                            <td>Pending</td>
                            <td>
                              <?php 
                                $url = base_url()."Dashboard/load_atm_media/".$value->AtmSTime."/".$value->AtmETime."/".$oth_rev_info['terminalid']."/". rtrim(base64_encode($oth_rev_info['CardNo']));
                                if($oth_rev_info['mediatype'] == "Pictures"){ ?>
                                <a onclick="load_atm_media('<?php echo $url; ?>')" href='#' data-toggle='modal' data-target="#atmMedia" class='btn btn-xs btn-primary'>View Image(s)</a>
                              <?php }elseif($oth_rev_info['mediatype'] == "Video"){
                                  print "<a href='' data-toggle='modal' data-target='.Vid".$counter."' class='btn btn-xs btn-success'>View Video(s)</a>";
                              } ?>
                            </td>
                          </tr>
                      <?php
                          $counter++; 
                        }
                      ?>
                    </tbody>
                  </table>
                  <?php
                    }
                  ?>
                </div>



                <!-- All Modals -->
               
                <div class="modal  fade Pic<?= $counter; ?>" id="atmMedia" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-md" stylex="margin:100px 220px 220px 220px;" role="document">
                    <div class="modal-content">

                    <!-- selected atm terminal media will display here via ajax-->
                    <div class="atm_media"> AtM Media</div>

                    </div>
                  </div>
                </div>

              <!-- footer content -->
                
<script type="text/javascript">
 
    function load_atm_media (url) {
        $.get(url, function(data) {
            var results = JSON.parse(data);

//            if(typeof results !== 'undefined' && results.length > 0){
//                alert("No record found for the selected student");
//            }else{
                $(".atm_media").html('Loading...');
                $(".atm_media").html(results);
//            }
        });
}



</script>