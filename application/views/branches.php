<!-- page content -->
<div class="right_col" role="main">

                <div class="">

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>NEW BRANCH</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">

                                    <form class="form-horizontal form-label-left" novalidate action="Create_Branch" method="post" >

                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Branch Name <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input class="form-control col-md-7 col-xs-12 txtbxvisible"  name="branch_name" placeholder="Comm. 12" required="required" type="text" />
                                            </div>
                                        </div>
                                        <div class="item form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Select Group <span class="required">*</span>
                                            </label>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <select class="select2_single form-control txtbxvisible" tabindex="-1" name="group_id" required>
                                                    <option></option>
                                                    <?php 
                                                        $data = $this->EventsModel->atm_group_retrieve();
        
                                                        if(!empty($data)) {
                                                            #code
                                                           $counter = 1;
                                                            foreach ($data as $value) {
        
                                                    ?>
                                                    <option value="<?php print $value->atmGroupId; ?>"><?php print $value->GroupName; ?></option>
                                                    <?php
                                                                $counter++;
                                                            }
                                                            
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-md-offset-5">
                                                <button id="send" type="submit" class="btn btn-success" name="create_branch">Create</button>
                                                <a href="../Dashboard"><button type="button" class="btn btn-primary">Cancel</button></a>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- footer content -->
