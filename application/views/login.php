<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php
	/*
	$filepointer = @fopen("Logs/Trans 1.txt", "r");
	
	if ($filepointer) {
	    while (($buffer = fgets($filepointer, 4096)) !== false) {
	        echo $buffer."<br>";
	    }
	    if (!feof($filepointer)) {
	        echo "Error: unexpected fgets() fail\n";
	    }
	    fclose($filepointer);
	}
	*/
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Alpha Centauri</title>

    <!-- Bootstrap core CSS -->

    <link href="<?php echo base_url(); ?>resources/css/bootstrap.min.css" rel="stylesheet">

    <link href="<?php echo base_url(); ?>resources/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>resources/css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="<?php echo base_url(); ?>resources/css/custom.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>resources/css/icheck/flat/green.css" rel="stylesheet">


    <script src="<?php echo base_url(); ?>resources/js/jquery.min.js"></script>

    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>

<body style="background:#F7F7F7;">
    
    <div class="">
        <a class="hiddenanchor" id="toregister"></a>
        <a class="hiddenanchor" id="tologin"></a>

        <div id="wrapper">
            <div id="login" class="animate form">
                <section class="login_content">
                    <?php if(validation_errors()): ?>
                          <?php echo validation_errors('<div class="alert alert-danger">','</div>'); ?>
                        <?php elseif(isset($invalid)): ?>
                           <div class=''style="color:red"><?= $invalid ?></div>
                          
                        <?php endif;?>

                        <?php echo form_open('Access/login_validation'); ?>

                        <h1> Login</h1>
                        <div>
                            <input type="text" class="form-control" placeholder="Username" name="username" required />
                        </div>
                        <div>
                            <input type="password" class="form-control" placeholder="Password" name="password" required />
                        </div>
                        <div>
                            <input type="submit" class="btn btn-default" name="login" value="Log in" style="margin:10px 15px 0 0;font-size:12px" />
                            
                        </div>
                        <div class="clearfix"></div>
                        <div class="separator">

                            <div class="clearfix"></div>
                            <br />
                            <div>
                                <span class="lead"><img src="<?php print base_url(); ?>resources/images/logos.jpg"> GreenSun IT Engineering!</span> <br>All Rights Reserved 
                            </div>
                        </div>
                    </form>
                    <!-- form -->
                </section>
                <!-- content -->
            </div>
            <div id="register" class="animate form">
                <section class="login_content">
                    <form>
                        <h1>Create Account</h1>
                        <div>
                            <input type="text" class="form-control" placeholder="Username" required="" />
                        </div>
                        <div>
                            <input type="email" class="form-control" placeholder="Email" required="" />
                        </div>
                        <div>
                            <input type="password" class="form-control" placeholder="Password" required="" />
                        </div>
                        <div>
                            <a class="btn btn-default submit" href="index.html">Submit</a>
                        </div>
                        <div class="clearfix"></div>
                        <div class="separator">

                            <p class="change_link">Already a member ?
                                <a href="#tologin" class="to_register"> Log in </a>
                            </p>
                            <div class="clearfix"></div>
                            <br />
                            <div>
                                <span class="lead"><img src="<?php print base_url(); ?>resources/images/logos.jpg"> GreenSun IT Engineering!</span> All Rights Reserved 
                            </div>
                        </div>
                    </form>
                    <!-- form -->
                </section>
                <!-- content -->
            </div>
        </div>
    </div>

</body>

</html>