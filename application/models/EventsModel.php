<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class EventsModel extends CI_Model {

    /************************************ Data Insertion ********************************************/

   /***********************************************
         Duplication Search
   ************************************************/
   public function dup_atminfo($tablename,$field_to_search,$search_text) {

      $this->db->select($field_to_search);

      $this->db->where($field_to_search,$search_text);

      $query = $this->db->get($tablename);

      $result = $this->db->affected_rows();

      return (($result == 1) ? true : false);
   }

   /***********************************************
         General Search
   ************************************************/
   public function retrieve_info($tablename,$field_to_search,$search_text) {

      $this->db->where($field_to_search,$search_text);

      $query = $this->db->get($tablename);

      $result = $query->num_rows(); 
        
      return(($result > 0) ? $query->result() : false);
   }
   
   /***********************************************
         Duplication Search
   ************************************************/
   public function dup_branch($tablename,$data) {

      $this->db->where('name',$data['name']);
      
      $this->db->where('group_id',$data['group_id']);

      $query = $this->db->get($tablename);

      $result = $this->db->affected_rows();

      return (($result == 1) ? true : false);
   }
	
		
	/***********************************************
			ATM Create
	************************************************/
	
	public function atm_create($data) {
       
        $tablename = "infoatm";

        $query = $this->db->insert($tablename,$data);

        $result = $this->db->affected_rows();
        
        return(($result > 0) ? true : false);

	}


    /***********************************************
            ATM Group Create
    ************************************************/
    
    public function group_create($data) {
        
        //Data Assignment
        $tablename          = "infoatmgroup";
        
        $query = $this->db->insert($tablename,$data);

        $result = $this->db->affected_rows(); 
        
        return(($result > 0) ? true : false);
  
    }
    
     /***********************************************
            ATM Group Create
    ************************************************/
    
    public function branch_create($data) {
        
        //Data Assignment
        $tablename          = "branches";
        
        $query = $this->db->insert($tablename,$data);

        $result = $this->db->affected_rows(); 
        
        return(($result > 0) ? true : false);
  
    }

    /***********************************************
            ATM Swtich Files
    ************************************************/
    
    public function dbinsert($target,$tablename,$atmId) {
        
      $sql = "
         LOAD DATA LOCAL INFILE '".$target."' REPLACE
         INTO TABLE $tablename 
         FIELDS TERMINATED BY ',' 
         ENCLOSED BY '\"'
         LINES TERMINATED BY '\n'
         IGNORE 1 LINES
         (@rid,@starttime,@endtime,@cardnumber,@accountnumber,@amountwithdrawn,@seqnumber,@currency)
         SET 
         starttime = @starttime,
         endtime   = @endtime,
         cardnumber = @cardnumber,
         accountnumber = @accountnumber,
         amtwithdrawn  = @amountwithdrawn,
         atmid = $atmId"; 

        //print $sql;
        $result = $this->db->query($sql);

        return(($result > 0) ? true : false);
  
    }


    /********************************** Data Retrieval ********************************************/

    /***********************************************
            All ATM total
    ************************************************/
    
    public function all_atm() {
        
        $tablename = "infoatm";

        $query = "SELECT COUNT(atmId) As tot_atm FROM $tablename";
        
        $query_res = $this->db->query($query);
        
        return( $query_res->row('tot_atm'));  
    }

    /***********************************************
            All ATM total
    ************************************************/
    
    public function all_atms() {
        
        $tablename = "infoatm";

        $query_res = $this->db->get($tablename);
        
        return( ($query_res) ? $query_res->result() : FALSE );  
    }
    
    /***********************************************
            All ATM total
    ************************************************/
    
    public function all_atm_reg() {
        
        $tablename = "infoatmgroup";

        $query = "SELECT COUNT(atmGroupId) As tot_reg FROM $tablename";
        
        $query_res = $this->db->query($query);
        
        return( $query_res->row('tot_reg'));  
    }
    
    /***********************************************
            All ATM Retrieve
    ************************************************/
    
    public function atm_retrieve() {
        
        $tablename = "infoatm";

        $this->db->order_by('atmId','DESC');
        
        $query = $this->db->get($tablename);

        $result = $query->num_rows(); 
        
        return(($result > 0) ? $query->result() : false);  
    }
    
    /***********************************************
             ATM Retrieve
    ************************************************/
    public function atm_search($branch_id) 
    {
      $tablename = "infoatm";

      $this->db->where('branch_id',$branch_id);
        
      $query = $this->db->get($tablename);

      $result = $query->num_rows(); 
        
      return(($result > 0) ? $query->result() : false);  
    }
    
    /***********************************************
             Retrieve ATM Info
    ************************************************/
    
    public function atm_info_ret($atmid) {
        
        $tablename = "infoatm";
        
        $this->db->where('atmId',$atmid);
        
        $query = $this->db->get($tablename);

        $result = $query->num_rows(); 
        
        return(($result > 0) ? $query->result() : false);  
    }

    /***********************************************
            All ATM Group Retrieve
    ************************************************/
    
    public function atm_group_retrieve() {
        
        $tablename = "infoatmgroup";

        $this->db->order_by('atmGroupId','DESC');
        
        $query = $this->db->get($tablename);

        $result = $query->num_rows(); 
        
        return(($result > 0) ? $query->result() : false);  
    }

    /***********************************************
            All ATM Group Retrieve
    ************************************************/
    
    public function group_id_name_ret($group_id) {
        
        $tablename = "infoatmgroup";
        
        $this->db->select('GroupName');
        
        $this->db->where('atmGroupId',$group_id);
        
        $query = $this->db->get($tablename);

        $result = $query->num_rows(); 
        
        return(($result > 0) ? $query->row() : false);  
    }

    /***********************************************
            Terminal ID Retrieve
    ************************************************/
    public function terminal_id_ip_retrieve($data) 
    {
      $tablename = "infoatm"; 

      $this->db->where('atmName =',$data['search_text']);
      $this->db->or_where('terminalid =',$data['search_text']);
      $this->db->or_where('terminalip =',$data['search_text']);
      
      $query = $this->db->get($tablename);

      $result = $query->num_rows(); 
        
      return(($result > 0) ? $query->row() : FALSE);  
    }
    
    /***********************************************
            Retrieve All Branch
    ************************************************/
    
    public function all_branches() {
        
        $tablename = "branches"; 

        $query = $this->db->get($tablename);

        $result = $query->num_rows(); 
        
        return(($result > 0) ? $query->result() : false);  
    }
    
    /***********************************************
            Branch Retrieve
    ************************************************/
    
    public function search_branch($data) {
        
        $tablename = "branches"; 

        $this->db->where('group_id =',$data['group_id']);
        
        $query = $this->db->get($tablename);

        $result = $query->num_rows(); 
        
        return(($result > 0) ? $query->result() : false);  
    }
    
    /***********************************************
            Branch Retrieve
    ************************************************/
    
    public function branch_retrieve($data) {
        
        $tablename = "branches"; 

        $this->db->where('branch_id',$data['branch_id']);
        
        $query = $this->db->get($tablename);

        $result = $query->num_rows(); 
        
        return(($result > 0) ? $query->row() : false);  
    }

    /***********************************************
             ATM Retrieve
    ************************************************/
    public function ret_server_info() 
    {
      $tablename = "settings_server";
        
      $query = $this->db->get($tablename);

      $result = $query->num_rows(); 
        
      return(($result > 0) ? $query->row() : false);  
    }

    /***********************************************
            Transaction Data Info 
    ************************************************/
    
    public function transaction_retrieve($terminalip,$data) {
        
        //Assigning Variables
        $tablename = "transactions";

        $lower_date = $data['lower_date'];

        $upper_date = $data['upper_date'];

        $request_type = $data['request_type'];

        $this->db->select('hostStartTime,hostEndTime,hostCardNumber,terminalip,file');

        if ($request_type == "Log") {
            # code...

            $this->db->where('terminalip =',$terminalip);

            # Date range selected
            if (strcasecmp($data['lower_date'],$data['upper_date']) < 0) {
                # code...
                $this->db->where("hostStartTime BETWEEN CAST('$lower_date' AS DATE) AND CAST('$upper_date' AS DATE)");
            }

            # Same date
            elseif(strcasecmp($data['lower_date'],$data['upper_date']) == 0){
                #code
                $this->db->where('hostStartTime',$data['lower_date']);
            }
        }

        elseif ($request_type == "transaction") {
            
            //Checking if card number is not empty
            if (!empty($data['hostCardNumber'])) {
                
                //check if terminalip(retrieved from atm name input) is not empty, check date
                if (!empty($terminalip)) {

                    //if date range also not empty
                    if (!empty($lowerdate)) {

                        //if daterange are diff, search by card num + terminalip + daterange
                        if (strcasecmp($data['lower_date'],$data['upper_date']) < 0) {
                       
                            $this->db->where('hostCardNumber =',$data['hostCardNumber']);

                            $this->db->where('terminalip=',$terminalip);

                            $this->db->where("hostStartTime BETWEEN CAST('$lower_date' AS DATE) AND CAST('$upper_date' AS DATE)");
                        }

                        //if daterange is the same date, search with card num + terminalip + single date
                        elseif(strcasecmp($data['lower_date'],$data['upper_date']) == 0){
                            
                            $this->db->where('hostCardNumber =',$data['hostCardNumber']);

                            $this->db->where('terminalip=',$terminalip);

                            $this->db->where('hostStartTime',$data['lower_date']);
                        }

                    }

                    //if date/daterange empty, search by card num + terminalip only
                    else {

                        $this->db->where('hostCardNumber =',$data['hostCardNumber']);

                        $this->db->where('terminalip=',$terminalip);

                    }

                }

                //if terminalip empty but not date, search by card num + date only

                elseif (!empty($lower_date)) {

                    if (strcasecmp($data['lower_date'],$data['upper_date']) < 0) {
                       
                        $this->db->where('hostCardNumber =',$data['hostCardNumber']);

                        $this->db->where("hostStartTime BETWEEN CAST('$lower_date' AS DATE) AND CAST('$upper_date' AS DATE)");
                    }

                    //same date of daterange
                    elseif(strcasecmp($data['lower_date'],$data['upper_date']) == 0){
                            
                        $this->db->where('hostCardNumber =',$data['hostCardNumber']);

                        $this->db->where('hostStartTime',$data['lower_date']);
                    } 

                }

                //if terminalip / date are empty, search by card only
                else{

                   $this->db->where('hostCardNumber =',$data['hostCardNumber']); 

                }
            }
                    
        }

        //print $data['upper_date']; //$this->db->get_compiled_select();
        
        $query = $this->db->get($tablename);

        $result = $query->num_rows(); 
       
        return(($result > 0) ? $query->result() : false);

    }

    /***********************************************
            Retreieve Roles 
    ************************************************/
    public function Roles_Retreieve() {
        
        $tablename = "userroles"; 
        
        $query = $this->db->get($tablename);

        $result = $query->num_rows(); 
        
        return(($result > 0) ? $query->result() : false);  
    }

    /***********************************************
        Ajax Search Retrieve
    ************************************************/
    public function ret_cardno($cardnumber) {
        
        $tablename = "transactions"; 

        $this->db->select("hostCardNumber");

        $this->db->like('hostCardNumber',$cardnumber);
        
        $this->db->limit(5);
        //print $this->db->get_compiled_select($tablename);
        $query = $this->db->get($tablename);

        $result = $query->num_rows(); 
        
        return(($result > 0) ? $query->result() : false);
    }
    
    /***********************************************
            Retreieve Roles 
    ************************************************/
    
    public function Generate_Report($data) {
        
        $tablename = "report"; 
        
        # Query Combinations
        $this->db->select('AtmRegion,Branch_name,AtmName,AtmId,AtmIp,Atmtype,TransStat,TransType,AtmSTime,AtmETime');
            # ATM Region 
            if( !empty($data['AtmRegion']) )
        $this->db->where('AtmRegion',$data['AtmRegion']);
            # Branch 
            if( !empty($data['Branch_name']) )
        $this->db->where('Branch_name',$data['Branch_name']);
            # ATM Name 
            if( !empty($data['AtmName']) )
        $this->db->where('AtmName',$data['AtmName']);
            # ATM Type 
            if( !empty($data['atmtype']) )
        $this->db->where('Atmtype',$data['atmtype']);
            # TransStat 
            if( isset($data['TransStat']) )
        $this->db->where('TransStat',$data['TransStat']);
            # TransType 
            if( isset($data['TransType']) )
        $this->db->where('TransType',$data['TransType']);
            # Daterange 
            if( strcasecmp($data['lower_date'],$data['upper_date']) < 0 ) 
        $this->db->where("DATE(AtmSTime) BETWEEN CAST('{$data['lower_date']}' AS DATE) AND CAST('{$data['upper_date']}' AS DATE)");
            elseif(strcasecmp($data['lower_date'],$data['upper_date']) == 0)
        $this->db->where("DATE(AtmSTime)", $data['lower_date']);
        # End of Query
         //print $this->db->get_compiled_select($tablename);
        $query = $this->db->get($tablename);

        $result = $query->num_rows(); 
        
        return(($result > 0) ? $query->result() : false);
    }

     /***********************************************
            Retreieve atmGroup Name 
    ************************************************/
    
    public function atm_group_lookup($groupid) {
        
        $tablename = "infoatmgroup"; 

        $this->db->select('GroupName');

        $this->db->where('atmGroupId =',$groupid);
        
        $query = $this->db->get($tablename);

        $result = $query->num_rows(); 
        
        return(($result > 0) ? $query->result() : false);  
    }
    
    /***********************************************
            Retreieve ATM Time For IMG
    ************************************************/
    
    public function ret_time_form_report($data) {
        
        $tablename = "report"; 

        $this->db->select('AtmSTime,AtmETime');
        
        # Daterange 
        if( strcasecmp($data['lower_date'],$data['upper_date']) < 0 ) 
            $this->db->where("DATE(AtmSTime) BETWEEN CAST('{$data['lower_date']}' AS DATE) AND CAST('{$data['upper_date']}' AS DATE)");
            
        elseif(strcasecmp($data['lower_date'],$data['upper_date']) == 0)
            $this->db->where("DATE(AtmSTime)", $data['lower_date']);
        
        if(!empty($data['Atmtype']))
            $this->db->where('Atmtype',$data['Atmtype']); 
        
        $this->db->where('HostCardNo',$data['HostCardNo']);
        
        $this->db->where('AtmId',$data['AtmId']);
        //print $this->db->get_compiled_select($tablename);
        $query = $this->db->get($tablename);

        $result = $query->num_rows(); 
        
        return(($result > 0) ? $query->result() : false); 
    }
    


public function ret_imgpath_corousol($data) { 
        $tablename  = "media";

        $starttime  = $data['starttime'];

        $endtime    = $data['endtime']; // exit($endtime.'- - '.$starttime);

        $card_number    = $data['card_number'];
        $terminalid    = $data['terminalid']; 
        $query = $this->db->query("SELECT m.date,m.stillpictures_path,t.atmStartTime,t.atmEndTime 
FROM media m JOIN transactions t on m.terminalid = t.terminalid
WHERE m.date BETWEEN '$starttime' AND '$endtime'
 AND t.atmCardNumber= '$card_number' AND m.terminalid= '$terminalid' GROUP BY m.date,m.stillpictures_path");

         $result = $query->num_rows();

        return(($result > 0) ? $query->result() : false); 
    }


    /***********************************************
            Retreieve ATM Time For IMG
    ************************************************/
    
    public function ret_imgpath($data) { 
        $tablename  = "media";

        $starttime  = $data['starttime'];

        $endtime    = $data['endtime'];

        $this->db->select("{$data['mediatype']}");

        $this->db->where('terminalid',$data['terminalid']);
        
        $this->db->where("date(date)  BETWEEN date('".$starttime."') AND date('".$endtime."')"); 
        //print $this->db->get_compiled_select($tablename);
        $query = $this->db->get($tablename);

        $result = $query->num_rows(); //var_dump($query->result()); exit();
        
        return(($result > 0) ? $query->result() : false); 
    }
    
    /***********************************************
            Retreieve atmGroup Name 
    ************************************************/
    
    public function ret_table_fields($tablename) { 

        $result = $this->db->list_fields($tablename); 
        
        return(($result) ? $result : false);  
    }
    
    /***********************************************
            Retreieve Recon Result
    ************************************************/
    
    public function ret_recon_result($starttime,$endtime,$atmid) { 

        $tablename = "reconresults";
        
        $query = "SELECT * FROM $tablename WHERE atmstarttime < '$endtime' AND atmendtime > '$starttime' ";
        
        $queryres = $this->db->query($query);
        
        //echo $this->db->get_compiled_select();

        $result = $queryres->num_rows(); 
        
        return(($result > 0) ? $queryres->result() : false);   
    }
    
    /***********************************************
            Retreieve Recon Result
    ************************************************/
    
    public function hq_users() { 

        $tablename = "users";
        
        $query = "SELECT * FROM $tablename WHERE branch_id = '1'" ;
        
        $queryres = $this->db->query($query);
        
        //echo $this->db->get_compiled_select();

        $result = $queryres->num_rows(); 
        
        return(($result > 0) ? $queryres->result() : false);   
    }
    
    /***********************************************
            Retreieve All Transaction
    ************************************************/
    
    public function transact() { 

        $tablename = "transactions";
        
        $query = $this->db->get($tablename);

        $result = $query->num_rows(); 
        
        return(($result > 0) ? $query->result() : false);
    }
    
    /***********************************************
            Calling Transaction View
    ************************************************/
    
    public function transact_view() { 

        $tablename = "report";
        
        $query = $this->db->get($tablename);

        $result = $query->num_rows(); 
        
        return(($result > 0) ? $query->result() : false);
    }


    /***************************************** Data Update ******************************/

    /***********************************************
            ATM Update
    ************************************************/
    public function atm_update($data) {
        
        //Data Assignment
        $tablename          = "infoatm";
        
        $this->db->set('atmName',$data['atmName']);
        $this->db->set('atmType',$data['atmType']);
        $this->db->set('terminalid',$data['terminalid']);
        $this->db->set('terminalip',$data['terminalip']);
        $this->db->set('branch_id',$data['branch_id']);
        $this->db->set('location',$data['location']);
        $this->db->where('atmId',$data['atmId']);
        
        //echo $this->db->get_compiled_update($tablename). "<br>";
        
        $query = $this->db->update($tablename);

        $result = $this->db->affected_rows();
                
        return (($result >= 0) ? true : false );
  
    }

    /***********************************************
            ATM group Update
    ************************************************/
    public function group_update($data) {
        
        //Data Assignment
        $tablename          = "infoatmgroup";
        
        $this->db->set('GroupName',$data['GroupName']);
        $this->db->set('Description',$data['Description']);
        $this->db->where('atmGroupId',$data['atmGroupId']);
        
        //echo $this->db->get_compiled_update($tablename). "<br>";
        
        $query = $this->db->update($tablename);

        $result = $this->db->affected_rows();
                
        return (($result >= 0) ? true : false );
  
    }
    
    /***********************************************
            Branch Update
    ************************************************/
    public function branch_update($data) {
        
        //Data Assignment
        $tablename          = "branches";
        
        $this->db->set('name',$data['name']);
        $this->db->set('group_id',$data['group_id']);
        
        $this->db->where('branch_id',$data['branch_id']);
        
        //echo $this->db->get_compiled_update($tablename). "<br>";
        
        $query = $this->db->update($tablename);

        $result = $this->db->affected_rows();
                
        return (($result >= 0) ? true : false );
  
    }

    /***********************************************
      Server Settings Update
    ************************************************/
    public function server_register($data) 
    {
      $tablename  = "settings_server";
        
      $this->db->set('serverip',$data['serverip']);
        
      $this->db->set('serverport',$data['serverport']);

      $this->db->where('id','1');
        
      $query = $this->db->update($tablename);

      $result = $this->db->affected_rows();
                
      return (($result >= 0) ? true : false );
    }

    /***********************************************
            ATM Update
    ************************************************/
    public function ret_server($data) {
        
        $tablename  = "settings";
        
        $this->db->where('id','1');
        
        $query = $this->db->get($tablename);

        $result = $query->num_rows(); 
        
        return(($result > 0) ? $query->result() : false);
  
    }

     /******************************************* Data Delete ********************************/

     /***********************************************
            ATM Delete
     *********************************************/

    public function atm_delete($atmid){

        $tablename = "infoatm";

        $this->db->where('atmId',$atmid);

        $query = $this->db->delete($tablename);

        return(($query) ? true : false);
    }

    /***********************************************
            Branch - ATM Delete
     *********************************************/

    public function branch_atm_delete($data){

        $tablename = "infoatm";

        $this->db->where('branch_id',$data['branch_id']);

        $query = $this->db->delete($tablename);

        return(($query) ? true : false);
    }
    
     /***********************************************
            ATM Branch Delete
    ************************************************/

    public function branch_delete($data){

        $tablename = "branches";

        $this->db->where('branch_id',$data['branch_id']);

         $query = $this->db->delete($tablename);

        return(($query) ? true : false);
    }

     /***********************************************
            ATM Group Delete
    ************************************************/

    public function group_delete($data){

        $tablename = "infoatmgroup";

        $this->db->where('atmGroupId',$data['atmGroupId']);

         $query = $this->db->delete($tablename);

        return(($query) ? true : false);
    }


  /************* Added Function ******************/
  public function All_Info($tablename) 
  {
    $query = $this->db->get($tablename);
              
    return ( ($query->num_rows() > 0) ? $query->result() :false );
  }

    
}//End of class
