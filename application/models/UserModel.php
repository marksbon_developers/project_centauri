<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class UserModel extends CI_Model {

	/************************************ Data Retrieval **********************************/
	
	/***********************************************
			Username dblookup => Login Page
	************************************************/
	public function dblookup($username){
		
		$tablename = "users";
		
		$where = array('userName'=>$username);
		
		$query = $this->db->get_where($tablename,$where);
		
		if($query->num_rows() == 1) {
				
			$row = $query->row();
			
			//variables Setting 
			$data['userName'] = $row->userName;
			$data['fullName']  = $row->fullName;
			$data['userid']  = $row->userId;
			$data['status']  = $row->state;
            
            #encrypted password
			$data['encrypted_password'] = $row->passWord;
            
            #role
			$role_id = $row->roleId;

			if(!empty($role_id)) {
				
				#Role Lookup
				$tablename = "userroles";

				$where = array('roleId'=>$role_id);

				$query = $this->db->get_where($tablename,$where);

				if($query->num_rows() == 1) {
				
					$row = $query->row();
					
					$data['role'] = $row->ROLE;

				}
			}
            
            #branch
            $branch_id = @$row->branch_id;

			if(!empty($branch_id)) {
				
				#Role Lookup
				$tablename = "branches";

				$where = array('branch_id'=>$branch_id);

				$query = $this->db->get_where($tablename,$where);

				if($query->num_rows() == 1) {
				
					$row = $query->row();
					
                    #group name
					$data['branch_name'] = $row->name;
                    
                    #group_id
                    $group_id = $row->group_id;
				}
			}
            
            #group
            if(!empty($group_id)) {
				
				#Role Lookup
				$tablename = "infoatmgroup";

				$where = array('atmGroupId'=>$group_id);

				$query = $this->db->get_where($tablename,$where);

				if($query->num_rows() == 1) {
				
					$row = $query->row();
					
                    #Group name
					$data['group_name'] = $row->GroupName;
				}
			}

			return $data;
		}	

	}
    
    /***********************************************
			All Users Under Branch
	************************************************/
	public function branch_users($branch_id){
		
		$tablename = "users";
        
        $this->db->where('branch_id',$branch_id);
		
		$query = $this->db->get($tablename);
		
		return (($query->num_rows() > 0) ? $query->result() : false); 	

	}

	/***********************************************
			All Users
	************************************************/
	public function allmembers(){
		
		$tablename = "users";
		
		$query = $this->db->get($tablename);
		
		return (($query->num_rows() >= 1) ? $query->result() : false); 	

	}

	/***********************************************
			All Users From View 
	************************************************/
	public function allusers_view(){
		
		$tablename = "user_details";
		
		$query = $this->db->get($tablename);
		
		return (($query->num_rows() >= 1) ? $query->result() : false); 	

	}

	/***********************************************
			User Password
	************************************************/
	public function user_passwd($userId){
		
		$tablename = "users";

		$this->db->select('passWord');

		$this->db->where('userId', $userId);
		
		$query = $this->db->get($tablename);
		
		return (($query->num_rows() == 1) ? $query->result() : false); 	

	}

	/***********************************************
			User Logging
	************************************************/
	public function user_logging($userId,$condition,$task){
		
		$tablename = "audits";

		$this->db->select('passWord');
		
		$query = $this->db->get($tablename);
		
		return (($query->num_rows() == 1) ? $query->result() : false); 	

	}

	

	/************************************** Data Insertion ************************************/

	/***********************************************
			Registering New Employee
	************************************************/
	
	public function register_user($data) {
		
		//registering user data
		$tablename = "users";
		
		//echo $this->db->get_compiled_insert('users',$data);
		$query = $this->db->insert($tablename,$data);
				
		return (($query) ? true : false );
		
	}

	/***********************************************
			Logging Sign In
	************************************************/
	
	public function login_log($userid) {
		
		//registering user data
		$tablename = "login_audit";
		
		//echo $this->db->get_compiled_insert('users',$data);

		$data = array(
			'userid' => $userid,
			'online'          => '1');

		$query = $this->db->insert($tablename,$data);

		$loginid = $this->db->insert_id();
				
		return (($query) ? $loginid : false );
		
	}

	/***********************************************
			Logging Sign Out
	************************************************/
	
	public function logout_log($login_id) {
		
		//registering user data
		$tablename = "login_audit";

		$this->db->set('online',"0");
		$this->db->where('login_id', $login_id);

		$query = $this->db->update($tablename);
				
		return (($query) ? true : false );
		
	}

	/********************************* Data Update ******************************************/

	/***********************************************
			Updating Employee Info
	************************************************/
	
	public function update_user($data) {

		//updating employee data
		$tablename = "users";

		$this->db->set('fullName'	,$data['fullName']);
		$this->db->set('userName'   ,$data['userName']);
		$this->db->set('passWord' 	,$data['passWord']);
		$this->db->set('branch_id' ,$data['branch_id']);
		$this->db->set('roleId'   	,$data['roleId']);
		$this->db->set('roleId'   	,$data['roleId']);
		$this->db->where('userId'	,$data['userId']);
		//echo $this->db->get_compiled_update('employees'). "<br>";
		$query = $this->db->update($tablename);

		$result1 = $this->db->affected_rows();
				
		$this->db->reset_query();

		return (($result >= 0) ? true : false );

	}

	/***********************************************
			Updating User Password
	************************************************/
	public function changepwd($newpwd_encrypted,$userId) {

		//updating employee data
		$tablename = "users";

		$this->db->set('passWord',$newpwd_encrypted);
		
		$this->db->where('userId',$userId);
		
		$query = $this->db->update($tablename);

		$result = $this->db->affected_rows();
				
		return (($result > 0) ? true : false );

	}

	/***********************************************
		Updating User Status
	************************************************/
	public function user_status($userId,$state)
	{
		$tablename = "users";

		$this->db->set('state',$state);
		
		$this->db->where('userId',$userId);
		
		$query = $this->db->update($tablename);

		$result = $this->db->affected_rows();
				
		return (($result > 0) ? true : false );
	}
	

	/***************************************** Data Delete *************************************/

    /***********************************************
            Deleting User
    ************************************************/
    
    public function delete_user($userId)  {
        
        $tablename = "users";

        $this->db->where('userId',$userId );
        
        $this->db->or_where('branch_id',$userId);
        
        $query = $this->db->delete($tablename);

        $result = $this->db->affected_rows();
        
        return(($result > 0) ?  true :  false);
         
    }
	
}
