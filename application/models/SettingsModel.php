<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class SettingsModel extends CI_Model {

    /***********************************************************************************************************************************************
    ****************************************************** Data Insertion **************************************************************************
    ***********************************************************************************************************************************************/
		
	/***********************************************
			Register Company
	************************************************/
	
	public function register_comp($data) {
	   
       $tablename = "companyinfo";
        
        $query = $this->db->count_all($tablename);
        
        if($query <= 0 ) {

        $query = $this->db->insert('companyinfo',$data);
		
		return (($query) ? true : false );
        
        } else {
            
            $_SESSION['form_link'] = "Edit_Companyinfo";
            return false;
        }
        
	}

    /***********************************************
            Adding Department
    ************************************************/
    
    public function add_dept($data){
        
        //echo $this->db->set($data)->get_compiled_insert('departments');
        $query  = $this->db->insert('departments',$data);
        $result = $this->db->affected_rows(); 
        
        return(($result > 0) ? true : false);
    }

    
     /***********************************************
            Adding Position
    ************************************************/
    
    public function add_position($data){
        
        $query = $this->db->insert('position',$data);
        
        $result = $this->db->affected_rows(); 
        
        return(($result > 0) ? true : false);
        
    }

    

     /*********************************************************************************************************************************************
    ****************************************************** Data Retrival **************************************************************************
    ***********************************************************************************************************************************************/
	
    /***********************************************
			Retrieve Company Info
	************************************************/
    
    public function retrieve_companyinfo(){
        
        $query = $this->db->get('companyinfo');
        
        if($query->num_rows() > 0) {
            
            $_SESSION['button_name'] = "Edit Info";
            $_SESSION['form_link'] = "Edit_Company";
            
            return($query->result());
            
        }
        else {
            
            $_SESSION['button_name'] = "Register";
            $_SESSION['form_link'] = "Register_Company";
        }
    }

    /***********************************************
            Retrieving Last Department ID
    ************************************************/
    
    public function retrieve_dept_id(){
        
        $this->db->select('id');

        $this->db->order_by('id','DESC');

        $query = $this->db->get('departments');
        
        return ( ($query->num_rows() > 0) ? $query->row(0) :false );
    }

    /***********************************************
            Retrieving Last Site ID
    ************************************************/
    
    public function retrieve_site_id(){
        
        $this->db->select('id');

        $this->db->order_by('id','DESC');

        $query = $this->db->get('sites');
        
        return ( ($query->num_rows() > 0) ? $query->row(0) :false );
    }

    /***********************************************
            Retrieving Last Employee ID
    ************************************************/
    
    public function retrieve_emp_id(){
        
        $this->db->select('employee_id');

        $this->db->order_by('employee_id','DESC');

        $query = $this->db->get('employees');
        
        return ( ($query->num_rows() > 0) ? $query->row(0) :false );
    }

    /***********************************************
            Retrieving Department
    ************************************************/
    
    public function retrieve_dept(){
        
        $query = $this->db->get('departments');
        
        if($query->num_rows() > 0) {
            
            return $query->result();
        }
        else {
            
            return false;
        }
    }

    /***********************************************
            Retrieving Site
    ************************************************/
    
    public function retrieve_site(){
        
        $query = $this->db->get('sites');
        
        if($query->num_rows() > 0) {
            
            return $query->result();
        }
        else {
            
            return false;
        }
    }

    /***********************************************
            Retrieving Position ID
    ************************************************/
    
    public function retrieve_pos_id(){
        
        $this->db->select('id');

        $this->db->order_by('id','DESC');

        $query = $this->db->get('position');
        
        return ( ($query->num_rows() > 0) ? $query->row(0) :false );
    }

    /***********************************************
            Retrieving Position
    ************************************************/
    
    public function retrieve_position(){
        
        $query = $this->db->get('position');
        
        if($query->num_rows() > 0) {
            
            return $query->result();
        }
        else {
            
            return false;
        }
    }

     /***********************************************
            Retrieving Grade Category
    ************************************************/
    
    public function retrieve_grade_category(){
        
        $tablename = "questionnaires";
        
        $where = array('major_grade'=>'Main');
        
        $query = $this->db->get_where($tablename,$where);
        
        if($query->num_rows() > 0) {
            
            return $query->result();
        }
        else {
            
            return false;
        }
        
    }

    /***********************************************
            Retrieving Questionnaires
    ************************************************/
    
    public function retrieve_Questionnaires($criteria){
        
        $tablename = "questionnaires";
        
        $where = array('major_grade'=>$criteria);
        
        $query = $this->db->get_where($tablename,$where);
        
        if($query->num_rows() > 0) {
            
            return $query->result();
        }
        else {
            
            return false;
        }
        
    }

    /***********************************************
            Retrieving Dashboard Tabs
    ************************************************/
    
    public function dashboard_tabs(){
        
        $query = $this->db->get('tabs');
        
        return(($query->num_rows() > 0) ? $query->result() : false );
          
    }

    /***********************************************
            Retrieving Tabs Priviledges
    ************************************************/
    
    public function retrieve_priviledges($tabname){
        
        //$this->db->select('priviledges');
        
        $where = array('name'=>$tabname );
        
        $query = $this->db->get_where('tabs', $where);
        
        $result = $this->db->affected_rows();
        
        return(($result > 0) ? $query->result() :  false);      
    }

     /***********************************************************************************************************************************************
****************************************************** Data Edit+Update *********************************************************
***********************************************************************************************************************************************/
    
    /***********************************************
			Edit Company Info
	************************************************/
    
    public function edit_companyinfo(){
        
        $query = $this->db->get('companyinfo');
        
        if($query->num_rows() > 0) {
            
            $_SESSION['button_name'] = "Update Info";
            $_SESSION['button_sub_name'] = "Update_Info";
            $_SESSION['form_link'] = "Update_Company";
            return($query->result());
            
        }
        else {
            
            echo "Error From Systems_model -> edit_companyinfo";
        }
    }
    
    /***********************************************
			Update Company Info
	************************************************/
    
    public function update_companyinfo($data,$id){
        
        $this->db->where('id',$id );
        $query = $this->db->update('companyinfo', $data);
        
        if($query->affected_rows > 0) {
            
            return true;
        }
        else {
            
            return false;
        }
    }

    /***********************************************
            Update Department
    ************************************************/
    
    public function update_dept($data,$id){
        
        $this->db->where('id',$id );
        $query = $this->db->update('departments', $data);
        
        return(($query->affected_rows > 0) ?  true :  false);
         
    }

    /***********************************************
            Update Site
    ************************************************/
    
    public function update_site($data,$id){
        
        $this->db->where('id',$id );
        $query = $this->db->update('sites', $data);
        
        return(($query->affected_rows > 0) ?  true :  false);
         
    }

    /***********************************************
            Update Position
    ************************************************/
    
    public function update_position($data,$id){
        
        $this->db->where('id',$id );
        $query = $this->db->update('position', $data);
        
        ($query->affected_rows > 0) ?  true :  false;
         
    }

    /***********************************************
            Update Grade Category
    ************************************************/
    
    public function update_grade_category($data,$id){
        
        $this->db->where('id',$id );
        
        $query = $this->db->update('questionnaires', $data);
        
        $result = $this->db->affected_rows();
        
        return(($result > 0) ?   true :  false);      
    }

    /***********************************************
            Postion Roles & Priviledges
    ************************************************/
    
    public function update_postion_roles_priv($data,$condition){
        
        $this->db->where('name',$condition );
        
        $query = $this->db->update('position', $data);
        
        $result = $this->db->affected_rows();
        
        return(($result > 0) ?   true :  false);      
    }

    /*******************************************************************************************************************************************
    ********************************************************** Data Delete *********************************************************************
    ********************************************************************************************************************************************/

    /***********************************************
            Deleting Department
    ************************************************/
    
    public function delete_department($id){
        
        $this->db->where('id',$id );
        $query = $this->db->delete('departments');

        $result = $this->db->affected_rows();
        
        return(($result > 0) ?  true :  false);
         
    }

    /***********************************************
            Deleting Site
    ************************************************/
    
    public function delete_site($id){
        
        $this->db->where('id',$id );
        $query = $this->db->delete('sites');

        $result = $this->db->affected_rows();
        
        return(($result > 0) ?  true :  false);
         
    }

    /***********************************************
            Deleting Position
    ************************************************/
    
    public function delete_position($id){
        
        $this->db->where('id',$id );
        $query = $this->db->delete('position');

        $result = $this->db->affected_rows();
        
        return(($result > 0) ?  true :  false);
         
    }

    /*******************************************************************************************************************************************************/
    /*******************************************************************************************************************************************************/

    
	
}//End of class
