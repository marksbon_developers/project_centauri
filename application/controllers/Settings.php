<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller {
	
	/***********************************************
		Constructor + Initialization
	************************************************/

	public function index() 
	{
		//loading Settings view
		if ( isset($_SESSION['username']) ) {
			# Title
			$title['title'] = "Settings";
            #Loading Model
            $this->load->model('SettingsModel');
            #Retrieving Data
			$data['companyinfo'] = $this->SettingsModel->retrieve_companyinfo();
            #Main Interface
			$this->load->view('header',$title);
			$this->load->view('organization',$data);
			$this->load->view('footer');
		
		} else {
			# code...
			//loading login view
			redirect('Access');
		}
	}

/*************************************** Interfaces **************************/
	/***********************************************
				System Settings
	************************************************/
    
    public function Setting() {
		
		//loading register view
		if (isset($_SESSION['username']) && !empty($_SESSION['jobtitle'])) {
			# code...
			//loading model and functions
			$this->load->model('user_mgmt');
			$data['allmembers'] = $this->user_mgmt->allmembers();

			$title['title'] = "Settings";
			//$data['user'] = $_SESSION['username'];
			$this->load->view('header_view',$title);
			$this->load->view('system_settings',$data);
			$this->load->view('footer_view');
		
		} else {
			# code...
			//loading login view
			redirect('Access/Login');
		}
	}

	public function Register_Company() {
		
		if(isset($_POST['organization'])) {
			
		//Setting validation rules
			$this->load->library('form_validation');
			$this->form_validation->set_rules('comp_name','Company Name','required|trim|xss_clean');
			$this->form_validation->set_rules('comp_tel1','Telephone No 1','required|trim|xss_clean');
			$this->form_validation->set_rules('comp_email','Password','required|trim|xss_clean');
			$this->form_validation->set_rules('comp_tel2','Telephone No 2','trim|required');
			$this->form_validation->set_rules('comp_fax','Fax Number','trim|required');
            $this->form_validation->set_rules('comp_loc','Location','trim|required');
			$this->form_validation->set_rules('comp_address','Address','required|trim|xss_clean');
			
			$data['name'] 	 = $this->input->post('comp_name');
			$data['tel_1']   = $this->input->post('comp_tel1');
			$data['tel_2'] 	 = $this->input->post('comp_tel2');
			$data['fax']    = $this->input->post('comp_fax');
			$data['email']  = $this->input->post('comp_email');
            $data['location']  = $this->input->post('comp_loc');
			$data['postal_addr']= $this->input->post('comp_address');
            $data['logo']       = file_get_contents( $_FILES['comp_logo']['tmp_name']);

		//loading model register
			$this->load->model('SettingsModel');
		
		//Registering User
			$result = $this->SettingsModel->register_comp($data);
			
			if($result) {
				$_SESSION['success'] = "Company Registration Successful";
				redirect('Settings');
			}
				
			else {
					
				$_SESSION['success'] = "Company Registration Failed";
				redirect('Settings');
			}
		}
		
		else {
					
				redirect('Settings');
			}
	}
    
    /***********************************************
				Edit Company Info
	************************************************/

	public function Edit_Company() {
               
        if ( isset($_SESSION['username']) && isset($_POST['organization'])) {
			# code...
                         
			//loading model and functions
			$this->load->model('SettingsModel');

			$data['companyinfo'] = $this->SettingsModel->edit_companyinfo();

			//Setting Variables for display
           foreach($data['companyinfo'] as $companyinfo) {
               
                $_SESSION['compinfo_id']            = $companyinfo->id;
                $_SESSION['compinfo']['name']       = trim($companyinfo->name);
                $_SESSION['compinfo']['tel1']       = $companyinfo->tel_1;
                $_SESSION['compinfo']['email']      = $companyinfo->email;
                $_SESSION['compinfo']['tel2']       = $companyinfo->tel_2;
                $_SESSION['compinfo']['fax']        = $companyinfo->fax;
                $_SESSION['compinfo']['location']        = $companyinfo->location;
                $_SESSION['compinfo']['address']    = $companyinfo->postal_addr;
               
           }
                                    
            $title['title'] = "Settings";
			$this->load->view('header',$title);
			$this->load->view('organization',$data);
			$this->load->view('footer');
                        		
		} else {
			# code...
			//loading login view
			redirect('Dashboard/Settings');
		}
	}

	/***********************************************
				Update Company Info
	************************************************/

	public function Update_Company() {
		
		if( isset($_POST['Update_Info']) && isset($_SESSION['Role']['Organogram']) ) {
			
		//Setting validation rules
			$this->load->library('form_validation');
			$this->form_validation->set_rules('comp_name','Company Name','required|trim|xss_clean');
			$this->form_validation->set_rules('comp_tel1','Telephone No 1','required|trim|xss_clean');
			$this->form_validation->set_rules('comp_email','Password','required|trim|xss_clean');
			$this->form_validation->set_rules('comp_tel2','Telephone No 2','trim|required');
			$this->form_validation->set_rules('comp_fax','Fax Number','trim|required');
            $this->form_validation->set_rules('comp_loc','Location','trim|required');
			$this->form_validation->set_rules('comp_address','Address','required|trim|xss_clean');
			
			$data['name'] 	 = $this->input->post('comp_name');
			$data['tel_1']   = $this->input->post('comp_tel1');
			$data['tel_2'] 	 = $this->input->post('comp_tel2');
			$data['fax']    = $this->input->post('comp_fax');
			$data['email']  = $this->input->post('comp_email');
            $data['location']  = $this->input->post('comp_loc');
			$data['postal_addr']= $this->input->post('comp_address');
            $data['logo']   = $_FILES['comp_logo']['name'];
            
            $id = $_SESSION['compinfo_id'];
            
            //renaming uploaded file

		//loading model register and function
			$this->load->model('SettingsModel');
			$result = $this->SettingsModel->update_companyinfo($data,$id);
			
			if($result) {
				$_SESSION['success'] = "Company Info Update Successful";
				
				redirect('Settings');
			}
				
			else {
					
				$_SESSION['error'] = "Company Info Update Failed";
				//navtab activation
				redirect('Settings');
			}
		}
		
		else {
					
				echo "Update Button Not Clicked";
				$this->register();
			}
	}

}//End of Class
