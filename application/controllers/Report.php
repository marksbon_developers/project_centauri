<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Report extends CI_Controller 
	{
		/***********************************************
			Retrieving Report Function 
		************************************************/
		public function General_Report()
		{
			# Loading Required Models
			$this->load->model('UserModel');
			$this->load->model('EventsModel');

			# Total No of ATMs
      $data['tot_atm'] = $this->EventsModel->all_atm();
            
      # Total No of ATM Groups
      $data['tot_atm_reg'] = $this->EventsModel->all_atm_reg();
            
      # Total No of Transaction from report view in db
      $data['tot_no_trans'] = $this->EventsModel->transact_view();

			# Accra                              	# Tema 																		# Eastern  								
      $Ga_counter = 0;                     	$Tm_counter = 0;													$Er_counter = 0;
      $Ga_counter_suc = 0;                 	$Tm_counter_suc = 0; 											$Er_counter_suc = 0;
      $Ga_counter_unsuc = 0;               	$Tm_counter_unsuc = 0;										$Er_counter_unsuc = 0;
      $Ga_counter_suc_with = 0;            	$Tm_counter_suc_with = 0;									$Er_counter_suc_with = 0;
      $Ga_counter_suc_inq = 0;             	$Tm_counter_suc_inq = 0;									$Er_counter_suc_inq = 0;
      $Ga_counter_suc_trans = 0;           	$Tm_counter_suc_trans = 0;								$Er_counter_suc_trans = 0;
      $Ga_counter_suc_statmt = 0;          	$Tm_counter_suc_statmt = 0;								$Er_counter_suc_statmt = 0;
      $Ga_counter_unsuc_with = 0;          	$Tm_counter_unsuc_with = 0;								$Er_counter_unsuc_with = 0;
      $Ga_counter_unsuc_inq = 0;           	$Tm_counter_unsuc_inq = 0;								$Er_counter_unsuc_inq = 0;
      $Ga_counter_unsuc_trans = 0;         	$Tm_counter_unsuc_trans = 0;							$Er_counter_unsuc_trans = 0;
      $Ga_counter_unsuc_statmt = 0;        	$Tm_counter_unsuc_statmt = 0;							$Er_counter_unsuc_statmt = 0;
      $Ga_counter_unsuc_inc = 0;           	$Tm_counter_unsuc_inc = 0;								$Er_counter_unsuc_inc = 0;
      # Accra                              	# Tema 																		# Eastern 

      # Central 													 	# Western 																# Volta 
      $Cr_counter = 0;										 	$Wr_counter = 0;													$Vr_counter = 0;
      $Cr_counter_suc = 0;								 	$Wr_counter_suc = 0;											$Vr_counter_suc = 0;
      $Cr_counter_unsuc = 0;							 	$Wr_counter_unsuc = 0;										$Vr_counter_unsuc = 0;
      $Cr_counter_suc_with = 0;					   	$Wr_counter_suc_with = 0;									$Vr_counter_suc_with = 0;
      $Cr_counter_suc_inq = 0;						 	$Wr_counter_suc_inq = 0;									$Vr_counter_suc_inq = 0;
      $Cr_counter_suc_trans = 0;					 	$Wr_counter_suc_trans = 0;								$Vr_counter_suc_trans = 0;
      $Cr_counter_suc_statmt = 0;					 	$Wr_counter_suc_statmt = 0;								$Vr_counter_suc_statmt = 0;
      $Cr_counter_unsuc_with = 0;					 	$Wr_counter_unsuc_with = 0;								$Vr_counter_unsuc_with = 0;
      $Cr_counter_unsuc_inq = 0;					 	$Wr_counter_unsuc_inq = 0;								$Vr_counter_unsuc_inq = 0;
      $Cr_counter_unsuc_trans = 0;				 	$Wr_counter_unsuc_trans = 0;							$Vr_counter_unsuc_trans = 0;
      $Cr_counter_unsuc_statmt = 0;				 	$Wr_counter_unsuc_statmt = 0;							$Vr_counter_unsuc_statmt = 0;
      $Cr_counter_unsuc_inc = 0;					 	$Wr_counter_unsuc_inc = 0;								$Vr_counter_unsuc_inc = 0;
      # Central                           	# Western        													# Volta

      # Northern 													 	# Ashanti 																# Brong Ahafo 
      $Nr_counter = 0;										 	$Ar_counter = 0;													$Ba_counter = 0;
      $Nr_counter_suc = 0;								 	$Ar_counter_suc = 0;											$Ba_counter_suc = 0;
      $Nr_counter_unsuc = 0;							 	$Ar_counter_unsuc = 0;										$Ba_counter_unsuc = 0;
      $Nr_counter_suc_with = 0;					   	$Ar_counter_suc_with = 0;									$Ba_counter_suc_with = 0;
      $Nr_counter_suc_inq = 0;						 	$Ar_counter_suc_inq = 0;									$Ba_counter_suc_inq = 0;
      $Nr_counter_suc_trans = 0;					 	$Ar_counter_suc_trans = 0;								$Ba_counter_suc_trans = 0;
      $Nr_counter_suc_statmt = 0;					 	$Ar_counter_suc_statmt = 0;								$Ba_counter_suc_statmt = 0;
      $Nr_counter_unsuc_with = 0;					 	$Ar_counter_unsuc_with = 0;								$Ba_counter_unsuc_with = 0;
      $Nr_counter_unsuc_inq = 0;					 	$Ar_counter_unsuc_inq = 0;								$Ba_counter_unsuc_inq = 0;
      $Nr_counter_unsuc_trans = 0;				 	$Ar_counter_unsuc_trans = 0;							$Ba_counter_unsuc_trans = 0;
      $Nr_counter_unsuc_statmt = 0;				 	$Ar_counter_unsuc_statmt = 0;							$Ba_counter_unsuc_statmt = 0;
      $Nr_counter_unsuc_inc = 0;					 	$Ar_counter_unsuc_inc = 0;								$Ba_counter_unsuc_inc = 0;
      # Northern                           	# Ashanti        													# Brong Ahafo
            
      # Upper East 												 	# Upper West 
      $Ue_counter = 0;											$Uw_counter = 0; 
      $Ue_counter_suc = 0;									$Uw_counter_suc = 0;
      $Ue_counter_unsuc = 0;								$Uw_counter_unsuc = 0;
      $Ue_counter_suc_with = 0;							$Uw_counter_suc_with = 0;
      $Ue_counter_suc_inq = 0;							$Uw_counter_suc_inq = 0;
      $Ue_counter_suc_trans = 0;						$Uw_counter_suc_trans = 0;
      $Ue_counter_suc_statmt = 0;						$Uw_counter_suc_statmt = 0;
      $Ue_counter_unsuc_with = 0;						$Uw_counter_unsuc_with = 0; 
      $Ue_counter_unsuc_inq = 0;						$Uw_counter_unsuc_inq = 0;
      $Ue_counter_unsuc_trans = 0;					$Uw_counter_unsuc_trans = 0;
      $Ue_counter_unsuc_statmt = 0;					$Uw_counter_unsuc_statmt = 0; 
      $Ue_counter_unsuc_inc = 0;						$Uw_counter_unsuc_inc = 0; 
      # Upper East 													# Upper West
            
      # Looping through the data recieved 
      if(!empty($data['tot_no_trans']))
      {
        foreach($data['tot_no_trans'] As $trans)
        {
          #Total No of Successful Transactions
          @$data['total_trans'] .= + 1;
                  
          # Totaling Regions
          switch($trans->AtmRegion) 
          {
            case 'Greater Accra' :
              $data['total_ga'] = $Ga_counter + 1;
              $Ga_counter++;
              break;
            case 'Tema Region' :
              $data['total_tm'] = $Tm_counter + 1;
              $Tm_counter++;
              break;
            case 'Central Region' :
              $data['total_cr'] = $Cr_counter + 1;
              $Cr_counter++;
              break;
            case 'Western Region' :
              $data['total_wr'] = $Wr_counter + 1;
              $Wr_counter++;
              break;
            case 'Eastern Region' :
              $data['total_er'] = $Er_counter + 1;
              $Er_counter++;
              break;
            case 'Northern Region' :
              $data['total_nr'] = $Nr_counter + 1;
              $Nr_counter++;
              break;
            case 'Volta Region' :
              $data['total_vr'] = $Vr_counter + 1;
              $Vr_counter++;
              break;
            case 'Ashanti Region' :
              $data['total_ash'] = $Ar_counter + 1;
              $Ar_counter++;
              break;
            case 'Brong Ahafo Region' :
              $data['total_ba'] = $Ar_counter + 1;
              $Ar_counter++;
              break;
            case 'Upper East' :
              $data['total_ue'] = $Ue_counter + 1;
              $Ue_counter++;
              break;
            case 'Upper West' :
              $data['total_uw'] = $Uw_counter + 1;
              $Uw_counter++;
              break;
          }
                    
          # Successful Transactions
          if($trans->TransStat == 1)
          {
            $data['tot_no_suc'] = @$data['tot_no_suc'] + 1;
            $atm_reg_suc[] =  $trans->AtmRegion;
            $atm_branches[]  =  $trans->Branch_name."*".$trans->AtmRegion;
                   
            # Grouping into Regions
            switch($trans->AtmRegion) 
            {
              case 'Greater Accra' :
                $data['total_ga_suc'] = $Ga_counter_suc + 1;
                $Ga_counter_suc++;
                $accra_suc[] = $trans->Branch_name;
                $accra_atm[] = $trans->AtmName;
                break;
              case 'Tema Region' :
                $data['total_tm_suc'] = $Tm_counter_suc + 1;
                $Tm_counter_suc++;
                $tema_suc[] = $trans->Branch_name;
                $tema_atm[] = $trans->AtmName;
                break;
              case 'Ashanti Region' :
                $data['total_ash_suc'] = $Ar_counter_suc + 1;
                $Ar_counter_suc++;
                $ashanti_suc[] = $trans->Branch_name;
                $ash_atm[] = $trans->AtmName;
                break;
              case 'Western Region' :
                $data['total_wr_suc'] = $Wr_counter_suc + 1;
                $Wr_counter_suc++;
                $west_suc[] = $trans->Branch_name;
                $west_atm[] = $trans->AtmName;
                break;
              case 'Eastern Region' :
                $data['total_er_suc'] = $Er_counter_suc + 1;
                $Er_counter_suc++;
                $east_suc[] = $trans->Branch_name;
                $east_atm[] = $trans->AtmName;
                break;
              case 'Volta Region' :
                $data['total_vr_suc'] = $Vr_counter_suc + 1;
                $Vr_counter_suc++;
                $volt_suc[] = $trans->Branch_name;
                $volta_atm[] = $trans->AtmName;
                break;
              case 'Northern Region' :
                $data['total_nr_suc'] = $Nr_counter_suc + 1;
                $Nr_counter_suc++;
                $north_suc[] = $trans->Branch_name;
                $north_atm[] = $trans->AtmName;
                break;
              case 'Upper East' :
                $data['total_ue_suc'] = $Ue_counter_suc + 1;
                $Ue_counter_suc++;
                $upeast_suc[] = $trans->Branch_name;
                $upeast_atm[] = $trans->AtmName;
                break;
              case 'Upper West' :
                $data['total_uw_suc'] = $Uw_counter_suc + 1;
                $Uw_counter_suc++;
                $upwest_suc[] = $trans->Branch_name;
                $upwest_atm[] = $trans->AtmName;
                break;
              case 'Brong Ahafo Region' :
                $data['total_ba_suc'] = $Ba_counter_suc + 1;
                $Ba_counter_suc++;
                $brong_suc[] = $trans->Branch_name;
                $brong_atm[] = $trans->AtmName;
                break;
              case 'Central Region' :
                $data['total_cr_suc'] = $Cr_counter_suc + 1;
                $Cr_counter_suc++;
                $central_suc[] = $trans->Branch_name;
                $central_atm[] = $trans->AtmName;
                break;
            }
          } 
            
          #Total No of UnSuccessful Transactions
          elseif($trans->TransStat == 0)
          {
            $data['tot_no_unsuc'] = @$data['tot_no_unsuc'] + 1;
            
            # Grouping into Regions
            switch($trans->AtmRegion) 
            {
              case 'Greater Accra' :
                $data['total_ga_unsuc'] = $Ga_counter_unsuc + 1;
                $Ga_counter_unsuc++;
                break;
              case 'Tema Region' :
                $data['total_tm_unsuc'] = $Tm_counter_unsuc + 1;
                $Tm_counter_unsuc++;
                break;
              case 'Central Region' :
                $data['total_cr_unsuc'] = $Cr_counter_unsuc + 1;
                $Cr_counter_unsuc++;
                break;
              case 'Western Region' :
                $data['total_wr_unsuc'] = $Wr_counter_unsuc + 1;
                $Wr_counter_unsuc++;
                break;
              case 'Eastern Region' :
                $data['total_er_unsuc'] = $Er_counter_unsuc + 1;
                $Er_counter_unsuc++;
                break;
              case 'Volta Region' :
                $data['total_vr_unsuc'] = $Vr_counter_unsuc + 1;
                $Vr_counter_unsuc++;
                break;
              case 'Northern Region' :
                $data['total_nr_unsuc'] = $Nr_counter_unsuc + 1;
                $Nr_counter_unsuc++;
                break;
              case 'Brong Ahafo Region' :
                $data['total_ba_unsuc'] = $Ba_counter_unsuc + 1;
                $Ba_counter_unsuc++;
                break;
              case 'Upper East' :
                $data['total_ue_unsuc'] = $Ue_counter_unsuc + 1;
                $Ue_counter_unsuc++;
                break;
              case 'Upper West' :
                $data['total_uw_unsuc'] = $Uw_counter_unsuc + 1;
                $Uw_counter_unsuc++;
                break;
            }
          }
                
          # Total No of WITHDRAWALS 
          if($trans->TransType == "WITHDRAWAL")
            $data['tot_with'] = @$data['tot_with'] + 1;
                
          # Total No of INQUIRY 
          if($trans->TransType == "INQUIRY")
            $data['tot_inq'] = @$data['tot_inq'] + 1;
                    
          # Total No of TRANSFER 
          if($trans->TransType == "TRANSFER")
            $data['tot_trans'] = @$data['tot_trans'] + 1;
                
          # Total No of MINI STATEMENT 
          if($trans->TransType == "MINISTATEMENT")
            $data['tot_statmt'] = @$data['tot_statmt'] + 1;
                
          # Total No of INCOMPLETE 
          if($trans->TransType == "")
            $data['tot_inc'] = @$data['tot_inc'] + 1;
                
          #Total No of Successful Withdrawal
          if($trans->TransType == "WITHDRAWAL" && $trans->TransStat == 1) 
          {
            $data['tot_suc_with'] = @$data['tot_suc_with'] + 1;
            # Grouping into Regions
            switch($trans->AtmRegion) 
            {
             	case 'Greater Accra' :
                $data['total_ga_suc_with'] = $Ga_counter_suc_with + 1;
                $Ga_counter_suc_with++;
                break;
              case 'Tema Region' :
                $data['total_tm_suc_with'] = $Tm_counter_suc_with + 1;
                $Tm_counter_suc_with++;
                break;
              case 'Central Region' :
                $data['total_cr_suc_with'] = $Cr_counter_suc_with + 1;
                $Cr_counter_suc_with++;
                break;
              case 'Western Region' :
                $data['total_wr_suc_with'] = $Wr_counter_suc_with + 1;
                $Wr_counter_suc_with++;
                break;
              case 'Eastern Region' :
                $data['total_er_suc_with'] = $Er_counter_suc_with + 1;
                $Er_counter_suc_with++;
                break;
              case 'Northern Region' :
                $data['total_nr_suc_with'] = $Nr_counter_suc_with + 1;
                $Nr_counter_suc_with++;
                break;
              case 'Brong Ahafo Region' :
                $data['total_ba_suc_with'] = $Ba_counter_suc_with + 1;
                $Ba_counter_suc_with++;
                break;
              case 'Volta Region' :
                $data['total_vr_suc_with'] = $Vr_counter_suc_with + 1;
                $Vr_counter_suc_with++;
                break;
              case 'Upper East' :
                $data['total_ue_suc_with'] = $Ue_counter_suc_with + 1;
                $Ue_counter_suc_with++;
                break;
              case 'Upper West' :
                $data['total_uw_suc_with'] = $Uw_counter_suc_with + 1;
                $Uw_counter_suc_with++;
                break;
            }
          }
            
          #Total No of Successful INQUIRY
          elseif($trans->TransType == "INQUIRY" && $trans->TransStat == 1) 
          {
            $data['tot_suc_inq'] = @$data['tot_suc_inq'] + 1;
            # Grouping into Regions
            switch($trans->AtmRegion) 
            {
              case 'Greater Accra' :
                $data['total_ga_suc_inq'] = $Ga_counter_suc_inq + 1;
                $Ga_counter_suc_inq++;
                break;
              case 'Tema Region' :
                $data['total_tm_suc_inq'] = $Tm_counter_suc_inq + 1;
                $Tm_counter_suc_inq++;
                break;
              case 'Central Region' :
                $data['total_cr_suc_inq'] = $Cr_counter_suc_inq + 1;
                $Cr_counter_suc_inq++;
                break;
              case 'Western Region' :
                $data['total_wr_suc_inq'] = $Wr_counter_suc_inq + 1;
                $Wr_counter_suc_inq++;
                break;
              case 'Eastern Region' :
                $data['total_er_suc_inq'] = $Er_counter_suc_inq + 1;
                $Er_counter_suc_inq++;
                break;
              case 'Northern Region' :
                $data['total_nr_suc_inq'] = $Nr_counter_suc_inq + 1;
                $Nr_counter_suc_inq++;
                break;
              case 'Brong Ahafo Region' :
                $data['total_ba_suc_inq'] = $Ba_counter_suc_inq + 1;
                $Ba_counter_suc_inq++;
                break;
              case 'Volta Region' :
                $data['total_vr_suc_inq'] = $Vr_counter_suc_inq + 1;
                $Vr_counter_suc_inq++;
                break;
              case 'Upper East' :
                $data['total_ue_suc_inq'] = $Ue_counter_suc_inq + 1;
                $Ue_counter_suc_inq++;
                break;
              case 'Upper West' :
                $data['total_uw_suc_inq'] = $Uw_counter_suc_inq + 1;
                $Uw_counter_suc_inq++;
                break;
            }
          }
            
          #Total No of Successful TRANSFER
          elseif($trans->TransType == "TRANSFER" && $trans->TransStat == 1) 
          {
            $data['tot_suc_trans'] = @$data['tot_suc_trans'] + 1;
            # Grouping into Regions
            switch($trans->AtmRegion) 
            {
              case 'Greater Accra' :
                $data['total_ga_suc_trans'] = $Ga_counter_suc_trans + 1;
                $Ga_counter_suc_trans++;
                break;
              case 'Tema Region' :
                $data['total_tm_suc_trans'] = $Tm_counter_suc_trans + 1;
                $Tm_counter_suc_trans++;
                break;
              case 'Central Region' :
                $data['total_cr_suc_trans'] = $Cr_counter_suc_trans + 1;
                $Cr_counter_suc_trans++;
                break;
              case 'Western Region' :
                $data['total_wr_suc_trans'] = $Wr_counter_suc_trans + 1;
                $Wr_counter_suc_trans++;
                break;
              case 'Eastern Region' :
                $data['total_er_suc_trans'] = $Er_counter_suc_trans + 1;
                $Er_counter_suc_trans++;
                break;
              case 'Northern Region' :
                $data['total_nr_suc_trans'] = $Nr_counter_suc_trans + 1;
                $Nr_counter_suc_trans++;
                break;
              case 'Brong Ahafo Region' :
                $data['total_ba_suc_trans'] = $Ba_counter_suc_trans + 1;
                $Ba_counter_suc_trans++;
                break;
              case 'Volta Region' :
                $data['total_vr_suc_trans'] = $Vr_counter_suc_trans + 1;
                $Vr_counter_suc_trans++;
                break;
              case 'Upper East' :
                $data['total_ue_suc_trans'] = $Ue_counter_suc_trans + 1;
                $Ue_counter_suc_trans++;
                break;
              case 'Upper West' :
                $data['total_uw_suc_trans'] = $Uw_counter_suc_trans + 1;
                $Uw_counter_suc_trans++;
                break;
            }
          }
            
          #Total No of Successful MINI STATEMENT
          elseif($trans->TransType == "MINISTATEMENT" && $trans->TransStat == 1) 
          {
            $data['tot_suc_statmt'] = @$data['tot_suc_statmt'] + 1;
            # Grouping into Regions
            switch($trans->AtmRegion) 
            {
              case 'Greater Accra' :
                  $data['total_ga_suc_statmt'] = $Ga_counter_suc_statmt + 1;
                  $Ga_counter_suc_statmt++;
                  break;
              case 'Tema Region' :
                  $data['total_tm_suc_statmt'] = $Tm_counter_suc_statmt + 1;
                  $Tm_counter_suc_statmt++;
                  break;
              case 'Central Region' :
                  $data['total_cr_suc_statmt'] = $Cr_counter_suc_statmt + 1;
                  $Cr_counter_suc_statmt++;
                  break;
              case 'Western Region' :
                  $data['total_wr_suc_statmt'] = $Wr_counter_suc_statmt + 1;
                  $Wr_counter_suc_statmt++;
                  break;
              case 'Eastern Region' :
                  $data['total_er_suc_statmt'] = $Er_counter_suc_statmt + 1;
                  $Er_counter_suc_statmt++;
                  break;
              case 'Northern Region' :
                  $data['total_nr_suc_statmt'] = $Nr_counter_suc_statmt + 1;
                  $Nr_counter_suc_statmt++;
                  break;
              case 'Brong Ahafo Region' :
                  $data['total_ba_suc_statmt'] = $Ba_counter_suc_statmt + 1;
                  $Ba_counter_suc_statmt++;
                  break;
              case 'Volta Region' :
                  $data['total_vr_suc_statmt'] = $Vr_counter_suc_statmt + 1;
                  $Vr_counter_suc_statmt++;
                  break;
              case 'Upper East' :
                  $data['total_ue_suc_statmt'] = $Ue_counter_suc_statmt + 1;
                  $Ue_counter_suc_statmt++;
                  break;
              case 'Upper West' :
                  $data['total_uw_suc_statmt'] = $Uw_counter_suc_statmt + 1;
                  $Uw_counter_suc_statmt++;
                  break;
            }
          }
                  
          #Total No of Unsuccessful Withdrawal
          if($trans->TransType == "WITHDRAWAL" && $trans->TransStat == 0) 
          {
            $data['tot_unsuc_with'] = @$data['tot_unsuc_with'] + 1;
            # Grouping into Regions
            switch($trans->AtmRegion) 
            {
              case 'Greater Accra' :
                  $data['total_ga_unsuc_with'] = $Ga_counter_unsuc_with + 1;
                  $Ga_counter_unsuc_with++;
                  break;
              case 'Tema Region' :
                  $data['total_tm_unsuc_with'] = $Tm_counter_unsuc_with + 1;
                  $Tm_counter_unsuc_with++;
                  break;
              case 'Central Region' :
                  $data['total_cr_unsuc_with'] = $Cr_counter_unsuc_with + 1;
                  $Cr_counter_unsuc_with++;
                  break;
              case 'Western Region' :
                  $data['total_wr_unsuc_with'] = $Wr_counter_unsuc_with + 1;
                  $Wr_counter_unsuc_with++;
                  break;
              case 'Eastern Region' :
                  $data['total_er_unsuc_with'] = $Er_counter_unsuc_with + 1;
                  $Er_counter_unsuc_with++;
                  break;
              case 'Northern Region' :
                  $data['total_nr_unsuc_with'] = $Nr_counter_unsuc_with + 1;
                  $Nr_counter_unsuc_with++;
                  break;
              case 'Brong Ahafo Region' :
                  $data['total_ba_unsuc_with'] = $Ba_counter_unsuc_with + 1;
                  $Ba_counter_unsuc_with++;
                  break;
              case 'Volta Region' :
                  $data['total_vr_unsuc_with'] = $Vr_counter_unsuc_with + 1;
                  $Vr_counter_unsuc_with++;
                  break;
              case 'Upper East' :
                  $data['total_ue_unsuc_with'] = $Ue_counter_unsuc_with + 1;
                  $Ue_counter_unsuc_with++;
                  break;
              case 'Upper West' :
                  $data['total_uw_unsuc_with'] = $Uw_counter_unsuc_with + 1;
                  $Uw_counter_unsuc_with++;
                  break;
            }
          }
            
          #Total No of Unsuccessful INQUIRY
          elseif($trans->TransType == "INQUIRY" && $trans->TransStat == 0) 
          {
            $data['tot_unsuc_inq'] = @$data['tot_unsuc_inq'] + 1;
            # Grouping into Regions
            switch($trans->AtmRegion) 
            {
              case 'Greater Accra' :
                  $data['total_ga_unsuc_inq'] = $Ga_counter_unsuc_inq + 1;
                  $Ga_counter_unsuc_inq++;
                  break;
              case 'Tema Region' :
                  $data['total_tm_unsuc_inq'] = $Tm_counter_unsuc_inq + 1;
                  $Tm_counter_unsuc_inq++;
                  break;
              case 'Central Region' :
                  $data['total_cr_unsuc_inq'] = $Cr_counter_unsuc_inq + 1;
                  $Cr_counter_unsuc_inq++;
                  break;
              case 'Western Region' :
                  $data['total_wr_unsuc_inq'] = $Wr_counter_unsuc_inq + 1;
                  $Wr_counter_unsuc_inq++;
                  break;
              case 'Eastern Region' :
                  $data['total_er_unsuc_inq'] = $Er_counter_unsuc_inq + 1;
                  $Er_counter_unsuc_inq++;
                  break;
              case 'Northern Region' :
                  $data['total_nr_unsuc_inq'] = $Nr_counter_unsuc_inq + 1;
                  $Nr_counter_unsuc_inq++;
                  break;
              case 'Brong Ahafo Region' :
                  $data['total_ba_unsuc_inq'] = $Ba_counter_unsuc_inq + 1;
                  $Ba_counter_unsuc_inq++;
                  break;
              case 'Volta Region' :
                  $data['total_vr_unsuc_inq'] = $Vr_counter_unsuc_inq + 1;
                  $Vr_counter_unsuc_inq++;
                  break;
              case 'Upper East' :
                  $data['total_ue_unsuc_inq'] = $Ue_counter_unsuc_inq + 1;
                  $Ue_counter_unsuc_inq++;
                  break;
              case 'Upper West' :
                  $data['total_uw_unsuc_inq'] = $Uw_counter_unsuc_inq + 1;
                  $Uw_counter_unsuc_inq++;
                  break;
            }
          }
                  
          #Total No of Unsuccessful TRANSFER
          elseif($trans->TransType == "TRANSFER" && $trans->TransStat == 0) 
          {
            $data['tot_unsuc_trans'] = @$data['tot_unsuc_trans'] + 1;
            # Grouping into Regions
            switch($trans->AtmRegion) 
            {
              case 'Greater Accra' :
                  $data['total_ga_unsuc_trans'] = $Ga_counter_unsuc_trans + 1;
                  $Ga_counter_unsuc_trans++;
                  break;
              case 'Tema Region' :
                  $data['total_tm_unsuc_trans'] = $Tm_counter_unsuc_trans + 1;
                  $Tm_counter_unsuc_trans++;
                  break;
              case 'Central Region' :
                  $data['total_cr_unsuc_trans'] = $Cr_counter_unsuc_trans + 1;
                  $Cr_counter_unsuc_trans++;
                  break;
              case 'Western Region' :
                  $data['total_wr_unsuc_trans'] = $Wr_counter_unsuc_trans + 1;
                  $Wr_counter_unsuc_trans++;
                  break;
              case 'Eastern Region' :
                  $data['total_er_unsuc_trans'] = $Er_counter_unsuc_trans + 1;
                  $Er_counter_unsuc_trans++;
                  break;
              case 'Northern Region' :
                  $data['total_nr_unsuc_trans'] = $Nr_counter_unsuc_trans + 1;
                  $Nr_counter_unsuc_trans++;
                  break;
              case 'Brong Ahafo Region' :
                  $data['total_ba_unsuc_trans'] = $Ba_counter_unsuc_trans + 1;
                  $Ba_counter_unsuc_trans++;
                  break;
              case 'Volta Region' :
                  $data['total_vr_unsuc_trans'] = $Vr_counter_unsuc_trans + 1;
                  $Vr_counter_unsuc_trans++;
                  break;
              case 'Upper East' :
                  $data['total_ue_unsuc_trans'] = $Ue_counter_unsuc_trans + 1;
                  $Ue_counter_unsuc_trans++;
                  break;
              case 'Upper West' :
                  $data['total_uw_unsuc_trans'] = $Uw_counter_unsuc_trans + 1;
                  $Uw_counter_unsuc_trans++;
                  break;
            }
          }
            
          #Total No of Unsuccessful MINI STATEMENT
          elseif($trans->TransType == "MINISTATEMENT" && $trans->TransStat == 0)
          {
            $data['tot_unsuc_statmt'] = @$data['tot_unsuc_statmt'] + 1;
            # Grouping into Regions
            switch($trans->AtmRegion) 
            {
              case 'Greater Accra' :
                  $data['total_ga_unsuc_statmt'] = $Ga_counter_unsuc_statmt + 1;
                  $Ga_counter_unsuc_statmt++;
                  break;
              case 'Tema Region' :
                  $data['total_tm_unsuc_statmt'] = $Tm_counter_unsuc_statmt + 1;
                  $Tm_counter_unsuc_statmt++;
                  break;
              case 'Central Region' :
                  $data['total_cr_unsuc_statmt'] = $Cr_counter_unsuc_statmt + 1;
                  $Cr_counter_unsuc_statmt++;
                  break;
              case 'Western Region' :
                  $data['total_wr_unsuc_statmt'] = $Wr_counter_unsuc_statmt + 1;
                  $Wr_counter_unsuc_statmt++;
                  break;
              case 'Eastern Region' :
                  $data['total_er_unsuc_statmt'] = $Er_counter_unsuc_statmt + 1;
                  $Er_counter_unsuc_statmt++;
                  break;
              case 'Northern Region' :
                  $data['total_nr_unsuc_statmt'] = $Nr_counter_unsuc_statmt + 1;
                  $Nr_counter_unsuc_statmt++;
                  break;
              case 'Brong Ahafo Region' :
                  $data['total_ba_unsuc_statmt'] = $Ba_counter_unsuc_statmt + 1;
                  $Ba_counter_unsuc_statmt++;
                  break;
              case 'Volta Region' :
                  $data['total_vr_unsuc_statmt'] = $Vr_counter_unsuc_statmt + 1;
                  $Vr_counter_unsuc_statmt++;
                  break;
              case 'Upper East' :
                  $data['total_ue_unsuc_statmt'] = $Ue_counter_unsuc_statmt + 1;
                  $Ue_counter_unsuc_statmt++;
                  break;
              case 'Upper West' :
                  $data['total_uw_unsuc_statmt'] = $Uw_counter_unsuc_statmt + 1;
                  $Uw_counter_unsuc_statmt++;
                  break;
            }
          }
          elseif($trans->TransType == NULL && $trans->TransStat == 0) 
          {
            $data['tot_unsuc_inc'] = @$data['tot_unsuc_inc'] + 1;
            # Grouping into Regions
            switch($trans->AtmRegion) 
            {
              case 'Greater Accra' :
                  $data['total_ga_unsuc_inc'] = $Ga_counter_unsuc_inc + 1;
                  $Ga_counter_unsuc_inc++;
                  break;
              case 'Tema Region' :
                  $data['total_tm_unsuc_inc'] = $Tm_counter_unsuc_inc + 1;
                  $Tm_counter_unsuc_inc++;
                  break;
              case 'Central Region' :
                  $data['total_cr_unsuc_inc'] = $Cr_counter_unsuc_inc + 1;
                  $Cr_counter_unsuc_inc++;
                  break;
              case 'Western Region' :
                  $data['total_wr_unsuc_inc'] = $Wr_counter_unsuc_inc + 1;
                  $Wr_counter_unsuc_inc++;
                  break;
              case 'Eastern Region' :
                  $data['total_er_unsuc_inc'] = $Er_counter_unsuc_inc + 1;
                  $Er_counter_unsuc_inc++;
                  break;
              case 'Northern Region' :
                  $data['total_nr_unsuc_inc'] = $Nr_counter_unsuc_inc + 1;
                  $Nr_counter_unsuc_inc++;
                  break;
              case 'Brong Ahafo Region' :
                  $data['total_ba_unsuc_inc'] = $Ba_counter_unsuc_inc + 1;
                  $Ba_counter_unsuc_inc++;
                  break;
              case 'Volta Region' :
                  $data['total_vr_unsuc_inc'] = $Vr_counter_unsuc_inc + 1;
                  $Vr_counter_unsuc_inc++;
                  break;
              case 'Upper East' :
                  $data['total_ue_unsuc_inc'] = $Ue_counter_unsuc_inc + 1;
                  $Ue_counter_unsuc_inc++;
                  break;
              case 'Upper West' :
                  $data['total_uw_unsuc_inc'] = $Uw_counter_unsuc_inc + 1;
                  $Uw_counter_unsuc_inc++;
                  break;
           	}
          }
        }

        # Variables Percentages
        $data['tot_no_suc_cent'] = round((@$data['tot_no_suc'] * 100 ) / sizeof($data['tot_no_trans']),2);
        $data['tot_no_unsuc_cent'] = round((@$data['tot_no_unsuc'] * 100 ) / sizeof($data['tot_no_trans']),2);
        $data['tot_with_cent'] = round((@$data['tot_with'] * 100 ) / sizeof($data['tot_no_trans']),2);
        $data['tot_inq_cent'] = round((@$data['tot_inq'] * 100 ) / sizeof($data['tot_no_trans']),2);
        $data['tot_trans_cent'] = round((@$data['tot_trans'] * 100 ) / sizeof($data['tot_no_trans']),2);
        $data['tot_statmt_cent'] = round((@$data['tot_statmt'] * 100 ) / sizeof($data['tot_no_trans']),2);
        $data['tot_inc_cent'] = round(($data['tot_inc'] * 100 ) / sizeof($data['tot_no_trans']),2);
          
        $data['tot_succ_with_cent'] = round((@$data['tot_suc_with'] * 100 ) / $data['tot_no_suc'],2);
        $data['tot_succ_inq_cent'] = round((@$data['tot_suc_inq'] * 100 ) / $data['tot_no_suc'],2);
        $data['tot_succ_trans_cent'] = round((@$data['tot_suc_trans'] * 100 ) / $data['tot_no_suc'],2);
        $data['tot_succ_statmt_cent'] = round((@$data['tot_suc_statmt'] * 100 ) / $data['tot_no_suc'],2);
          
        $data['tot_unsucc_with_cent'] = round((@$data['tot_unsuc_with'] * 100 ) / $data['tot_no_unsuc'],2);
        $data['tot_unsucc_inq_cent'] = round((@$data['tot_unsuc_inq'] * 100 ) / $data['tot_no_unsuc'],2);
        $data['tot_unsucc_trans_cent'] = round((@$data['tot_unsuc_trans'] * 100 ) / $data['tot_no_unsuc'],2);
        $data['tot_unsucc_statmt_cent'] = round((@$data['tot_unsuc_statmt'] * 100 ) / $data['tot_no_unsuc'],2);
        $data['tot_unsucc_inc_cent'] = round((@$data['tot_unsuc_inc'] * 100 ) / $data['tot_no_unsuc'],2);
          
        $data['atm_reg_suc'] = array_count_values(@$atm_reg_suc);
        $reg_suc = array_count_values(@$atm_reg_suc);
        $bra_suc = array_count_values(@$atm_branches); 
          
        @$accra_atm = array_count_values(@$accra_atm);
        @$west_atm = array_count_values(@$west_atm);
        @$tema_atm = array_count_values(@$tema_atm);
        @$east_atm = array_count_values(@$east_atm);
        @$volta_atm = array_count_values(@$volta_atm);
        @$north_atm = array_count_values(@$north_atm);
        @$brong_atm = array_count_values(@$brong_atm);
        @$upeast_atm = array_count_values(@$upeast_atm);
        @$upwest_atm = array_count_values(@$upwest_atm);
        @$ash_atm = array_count_values(@$ash_atm);
        @$central_atm = array_count_values(@$central_atm);

        # Returning all Necesary Data
        return $data;
      }

      else
        $_SESSION['warning'] = "No Transactions Found.";
		}

	/********************************* Begin of Interfaces **********************************/

		/***********************************************
			Constructor + Initialization
		************************************************/
		public function index() 
		{
			if (isset($_SESSION['username'])) 
			{
				if ( $_SESSION['role'] == 'Administrator' || $_SESSION['role'] == 'GreenSunAdmin' ) 
				{
					redirect('Report/Overview');
				}
				else
				{
					redirect('Dashboard');
				}
			} 
			else 
			{
				# Not Logged In
				redirect('Access');
			}
		
		}

		/***********************************************
			Report Dashboard
		************************************************/
		public function Overview()
		{
			if (isset($_SESSION['username'])) 
			{
				if ( $_SESSION['role'] == 'Administrator' || $_SESSION['role'] == 'GreenSunAdmin' ) 
				{
					# Calling Reports
					$data = $this->General_Report();

					$data['title'] = "Report - Overview";
          $this->load->view('header',$data);
          $this->load->view('reports',$data);
          $this->load->view('footer');
				}
				else
				{
					$_SESSION['error'] = "Permission Denied. Contact Administrator";
					redirect('Dashboard');
				}
			} 
			else 
			{
				# Not Logged In
				redirect('Access');
			} 
		}

		/***********************************************
			Generate Report
		************************************************/
		public function Generate_Report()
		{
			if (isset($_SESSION['username'])) 
			{
				if ( $_SESSION['role'] == 'Administrator' || $_SESSION['role'] == 'GreenSunAdmin' ) 
				{
					# Loading Required Models
					$this->load->model('UserModel');
					$this->load->model('EventsModel');

					# Loading Interface
	        $title['title'] = "Generate Report";
          $this->load->view('header',$title);
					$this->load->view('nav');
	        $this->load->view('generate_report');
					$this->load->view('footer');
				}
				else
				{
					$_SESSION['error'] = "Permission Denied. Contact Administrator";
					redirect('Dashboard');
				}
			} 
			else 
			{
				# Not Logged In
				redirect('Access');
			} 
		}

		/***********************************************
			Downloading Report Dashboard
		************************************************/
		public function Download()
		{
			if (isset($_SESSION['username'])) 
			{
				if ( $_SESSION['role'] == 'Administrator' || $_SESSION['role'] == 'GreenSunAdmin' ) 
				{
					# Calling Reports
					$data = $this->General_Report();

					$this->load->view('dashboard_report',$data);
					$this->load->library('dompdf_gen');
					$html = $this->output->get_output();
             
          # Convert to PDF
          //$this->dompdf->set_base_path(base_url()."resources/css/bootstrap.min.css");
          
          $this->dompdf->load_html($html);
          
          $this->dompdf->render();
          
          $this->dompdf->stream("dashboard_report.pdf",array('Attachment'=>0));
          //or without preview in browser
          
          //$this->dompdf->stream("dashboard_report.pdf");
				}
				else
				{
					$_SESSION['error'] = "Permission Denied. Contact Administrator";
					redirect('Dashboard');
				}
			} 
			else 
			{
				# Not Logged In
				redirect('Access');
			} 
		}

		/***********************************************
			 Send Feedback
		************************************************/

		public function Feedback() 
		{
			if (isset($_SESSION['username'])) 
			{
				if ( $_SESSION['role'] == 'Administrator' || $_SESSION['role'] == 'GreenSunAdmin' ) 
				{
					# Loading Model...
		  	  $this->load->model('EventsModel');
					
					$title['title'] = "Send Us A Feedback";
					$this->load->view('header',$title);
					$this->load->view('feedback');
					$this->load->view('footer');
				}
				else
				{
					$_SESSION['error'] = "Permission Denied. Contact Administrator";
					redirect('Dashboard');
				}
			} 
			else 
			{
				# Not Logged In
				redirect('Access');
			} 
		}

	/********************************* End of Interfaces **********************************/



	/************************ Begin of POST Variables Processing **************************/

		/***********************************************
			Generate Report Process
		************************************************/
		public function Generating_Report()
		{
			if (isset($_SESSION['username'])) 
			{
				if ( $_SESSION['role'] == 'Administrator' || $_SESSION['role'] == 'GreenSunAdmin' ) 
				{
					if( isset($_POST['gen_report']) ) 
	        {
	          # Setting validation rules
	    			$this->form_validation->set_rules('group_id','ATM Region','required|trim');
	          $this->form_validation->set_rules('branch_id','Branch','trim');
	          $this->form_validation->set_rules('atm_id','ATM Name','trim');
	          $this->form_validation->set_rules('atmtype','ATM Type','trim');
	          $this->form_validation->set_rules('report_type','Report Type','required|trim');
	          $this->form_validation->set_rules('daterange','Date Range','required|trim');
	          
	          # If Failed
	          if ($this->form_validation->run() === FALSE) {
	              #Redirecting With Error
	              $_SESSION['error'] = "Atm Region / Date / Report Type Required";
	              redirect('Report/Generate_Report');
	          }
	          
	          # If Passed
	          else
	          {
	            # Loading Models 
	            $this->load->model('EventsModel');
	            
	            # Variable Declaration
	            $data['group_id']       = $this->input->post('group_id');
	            $data['branch_id']      = $this->input->post('branch_id');
	            $data['atmId']          = $this->input->post('atm_id');
	            $data['atmtype']        = $this->input->post('atmtype');
	            $data['report_type']    = $this->input->post('report_type');
	            $date 					= explode(' to ', $this->input->post('daterange'));
	            $data['lower_date']		= $date[0];
	            $data['upper_date']		= @$date[1];
	            
	            # Retrieveing Names From ID
	            	# ATM Region 
	            	if( ($data['group_id'] != "All") && !empty($data['group_id']) )
	            	{
	            	    $Reg = $this->EventsModel->group_id_name_ret($data['group_id']);
	            	    foreach($Reg As $Res)
	            	    {
	            	        $data['AtmRegion'] = $Res->GroupName;
	            	    }
	            	}
	            	else 
	            	{
	            	    // Making it empty 
	            	    $data['AtmRegion'] = "";
	            	    $_SESSION['region'] = "All";
	            	}
	            	# End Of ATM Region Name
	            	
	            	# Branch Name 
	            	if( ($data['branch_id'] != "All") && !empty($data['branch_id']) )
	            	{
	            	    $bran = $this->EventsModel->branch_retrieve($data);
	            	    foreach($bran As $branch)
	            	    {
	            	        $data['Branch_name'] = $branch->name;
	            	    }
	            	}
	            	else 
	            	{
	            	    // Making it empty 
	            	    $data['Branch_name'] = "";
	            	}
	            	#Branch Name Retrieve
	            	
	            	# ATM Name Retrieve
	            	if( ($data['atmId'] != "All") && !empty($data['atmId']) )
	            	{
	            	    $atm = $this->EventsModel->atm_info_ret($data['atmId']);
	            	    foreach($atm As $atminfo)
	            	    {
	            	        $data['AtmName'] = $atminfo->atmName;
	            	    }
	            	}
	            	else 
	            	{
	            	    // Making it empty 
	            	    $data['AtmName'] = "";
	            	}
	            	# End Of Atm Name Retrieve
	              
	              # ATM Type 
	              if( empty($data['atmtype']) || (strcasecmp($data['atmtype'],'All') == 0) )
	              {   
	              	$data['atmtype'] = "";     
	              }
	                    
	              # Report Type 
	              if( !empty($data['report_type']) )
	              {    
	                # Checking dual selected i.e. Successful & Withdrawal
	                if( strpos($data['report_type'],'/') )
	                {
	                  # TransStat~1/TransType~Withdrawal
	                  $stattype = explode('/',$data['report_type']);
	                  # [0] => TransStat~1 , [1] => TransType~Withdrawal
	                  $stat = explode('~',$stattype[0]);
	                  # [0] => TransStat [1] => 1
	                  $type = explode('~',$stattype[1]);
	                  # [0] => TransType [1] => Withdrawal
	                  $data['TransStat'] = $stat[1];
	                  $data['TransType'] = $type[1];
	                }
	                # Single Report Type Select 
	                else
	                {
	                  # TransStat~(0,1) || TransType~Withdrawal || file~1
	                    # TransStat
	                    if( strpos($data['report_type'],'Stat') )
	                    {
	                       $stat = explode('~',$data['report_type']);
	                       # [0] => TransStat, [1] => (0/1/'')
	                       $data['TransStat'] = $stat[1];
	                    }
	                    elseif( strpos($data['report_type'],'Type') )
	                    {
	                       $type = explode('~',$data['report_type']);
	                       # [0] => TransType, [1] => (Withdrawal/Inquiry)
	                       $data['TransType'] = $type[1];
	                    }
	                    else
	                    {
	                       $file = explode('~',$data['report_type']);
	                       # [0] => file, [1] => (0/1)
	                       $data['file'] = $file[1];
	                    }
	                  # End   
	                }
	              }
	              else
	              {
	                  $_SESSION['error'] = "Report Type Empty";
	                  redirect('Report/Generate_Report');
	              }
	              # End Report Type
	                    
	              # Function Call and Response
	              $data['report_result'] = $this->EventsModel->Generate_Report($data);
	              //print "<br/><br/>";
	              //print_r($data);
	               
	              if($data['report_result']) 
	              {
	                 # Passing Data through Session
	                 $this->session->set_flashdata('report_result',$data);
	              }
	              else
	              {
	                  $_SESSION['warning'] = "No Records Found";
	              }
	              
	              ### Loading Interface ####
	              redirect('Report/Generate_Report');                 
	          }
	    		}
				}
				else
				{
					$_SESSION['error'] = "Permission Denied. Contact Administrator";
					redirect('Dashboard');
				}
			} 
			else 
			{
				# Not Logged In
				redirect('Access');
			} 
		}
		# End of Class

    /***********************************************
      Generate Report Process
    ************************************************/
    public function Send_Feedback()
    {
      if (isset($_SESSION['username'])) 
      {
        if ( $_SESSION['role'] == 'Administrator' || $_SESSION['role'] == 'GreenSunAdmin' ) 
        {
          if( isset($_POST['send_complain']) ) 
          {
            # Setting validation rules
            $this->form_validation->set_rules('companyname','ATM Region','required|trim');
            $this->form_validation->set_rules('complaintname','Branch','required|trim');
            $this->form_validation->set_rules('complain','ATM Name','required|trim');
            
            # If Failed
            if ($this->form_validation->run() === FALSE) {
                #Redirecting With Error
                $_SESSION['error'] = "All Fields Required";
                redirect('Report/Feedback');
            }
            
            # If Passed
            else
            {
              # Variable Assignment
              $email    = "ivanpaitoo@gmail.com";
              $subject  = "Alpha Centauri Complain From ".$this->input->post('companyname');
              $message  = $this->input->post('complain');

              # Sending Mail
              $result = mail($email, $subject, $message);
              
              if($result) 
              {
                $_SESSION['success'] = "Email Sent. We Would Get To U As Soon As Possible";
                redirect('Report/Feedback');
              }
              else
              {
                $_SESSION['error'] = "Email Sending Failed.";
                redirect('Report/Feedback');
              }
            } # End of Validation Rule
          }
        } 
        else
        {
          $_SESSION['error'] = "Permission Denied. Contact Administrator";
          redirect('Dashboard');
        } # End of Permission
      } 
      else 
      {
        # Not Logged In
        redirect('Access');
      } 
    }
    # End of Class

	/************************** End of POST Variables Processing **************************/

  

  /*********************************** Ajax Methods *************************************/
    /***********************************************
        AJAx
    ************************************************/
    public function ajax_branches() 
    {
      # Loading Model
      $this->load->model('EventsModel');
          
      # Assigning Data For Query
      $data['group_id'] = $this->input->post('groupid'); 

      $result = $this->EventsModel->search_branch($data);

      array_unshift($result,'<option value="">ALL</option>');

      print "<option></option><option> All </option>";

      foreach($result As $res)
      {
          print "<option value='".@$res->branch_id."'>".@$res->name."</option>";
      }
      
      exit;
    }
      
    public function ajax_atms() 
    {
      # Loading Model
      $this->load->model('EventsModel');
      
      # Assigning Data For Query
      $data['branch_id'] = $this->input->post('branchid'); 

      $result = $this->EventsModel->atm_search($data['branch_id']);

      array_unshift($result,'<option value="">ALL</option>');

      print "<option></option><option> All </option>";
      
      foreach($result As $res)
      {
          print "<option value='{$res->atmId}'>".$res->atmName."</option>";
      }
      
      exit;
    }
  /*********************************** Ajax Methods *************************************/

	}
	# End of Class
?>