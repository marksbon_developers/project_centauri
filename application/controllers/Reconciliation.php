<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class ReConciliation extends CI_Controller 
	{
		
	 /********************************* Begin of Interfaces **********************************/

		/***********************************************
			Constructor + Initialization
		************************************************/
		public function index() 
		{
			if (isset($_SESSION['username'])) 
			{
				if ( $_SESSION['role'] == 'Administrator' || $_SESSION['role'] == 'GreenSunAdmin' ) 
				{
					redirect('Reconciliation/Main');
				}
				else
				{
					redirect('Dashboard');
				}
			} 
			else 
			{
				# Not Logged In
				redirect('Access');
			}
		}

    /***********************************************
      Main Recon Interface 
    ************************************************/
    public function Main() 
    {
      if (isset($_SESSION['username'])) 
      {
        if ( $_SESSION['role'] == 'Administrator' || $_SESSION['role'] == 'GreenSunAdmin' ) 
        {
          # Loading Models
          $this->load->model('UserModel');
          $this->load->model('EventsModel');

          $title['title'] = "ReConciliation";
          $this->load->view('header',$title);
          $this->load->view('nav-ej');
          $this->load->view('recon');
          $this->load->view('footer');
        }
        else
        {
          $_SESSION['error'] = "Permission Denied. Contact Administrator";
          redirect('Dashboard');
        }
      } 
      else 
      {
        # Not Logged In
        redirect('Access');
      }
    }

    /***********************************************
      File Upload Interface
    ************************************************/
    public function Upload() 
    {
      if (isset($_SESSION['username'])) 
      {
        if ( $_SESSION['role'] == 'Administrator' || $_SESSION['role'] == 'GreenSunAdmin' ) 
        {
          # Loading Models
          $this->load->model('UserModel');
          $this->load->model('EventsModel');

          $title['title'] = "Recon File Upload";
          $this->load->view('header',$title);
          $this->load->view('nav-ej');
          $this->load->view('file_upload');
          $this->load->view('footer');
        }
        else
        {
          $_SESSION['error'] = "Permission Denied. Contact Administrator";
          redirect('Dashboard');
        }
      } 
      else 
      {
        # Not Logged In
        redirect('Access');
      }
    }

    /***********************************************
      2 Way Reconciliation
    ************************************************/
    public function D_Comparism() 
    {
      if (isset($_SESSION['username'])) 
      {
        if ( $_SESSION['role'] == 'Administrator' || $_SESSION['role'] == 'GreenSunAdmin' ) 
        {
          # Loading Models
          $this->load->model('UserModel');
          $this->load->model('EventsModel');

          $title['title'] = "2 Way Recon";
          $this->load->view('header',$title);
          $this->load->view('nav-ej');
          $this->load->view('2way');
          $this->load->view('footer');
        }
        else
        {
          $_SESSION['error'] = "Permission Denied. Contact Administrator";
          redirect('Dashboard');
        }
      } 
      else 
      {
        # Not Logged In
        redirect('Access');
      }
    }

    /***********************************************
      2 Way Reconciliation
    ************************************************/
    public function T_Comparism() 
    {
      if (isset($_SESSION['username'])) 
      {
        if ( $_SESSION['role'] == 'Administrator' || $_SESSION['role'] == 'GreenSunAdmin' ) 
        {
          # Loading Models
          $this->load->model('UserModel');
          $this->load->model('EventsModel');

          $title['title'] = "3 Way Recon";
          $this->load->view('header',$title);
          $this->load->view('nav-ej');
          $this->load->view('3way');
          $this->load->view('footer');
        }
        else
        {
          $_SESSION['error'] = "Permission Denied. Contact Administrator";
          redirect('Dashboard');
        }
      } 
      else 
      {
        # Not Logged In
        redirect('Access');
      }
    }

	/********************************* End of Interfaces **********************************/

	/************************ Begin of POST Variables Processing **************************/

		/***********************************************
      Uploading Recon Files
    ************************************************/
    public function Uploading() 
    {
      if (isset($_SESSION['username'])) 
      {
        if ( $_SESSION['role'] == 'Administrator' || $_SESSION['role'] == 'GreenSunAdmin' ) 
        {
          # Loading Models
          $this->load->model('EventsModel');

          # ATM Switch File Upload
          if(isset($_POST['atm']))
          {
            $this->form_validation->set_rules('atmId','ATM Name','required|trim');
            
            if ($this->form_validation->run() === FALSE) 
            {
              $_SESSION['error'] = "Atm Field Required";
              
              redirect('Reconciliation/Upload');
            }
            
            else
            {
              $atmId = $this->input->post('atmId');

              $this->file_restriction($_FILES['csvfile'],"ATMH",$atmId);
            }
          }
          # ATM Switch File Upload

          # BANK Switch File Upload
          elseif(isset($_POST['bank']))
          {
            $this->form_validation->set_rules('atmId','ATM Name','required|trim|xss_clean');
            
            if ($this->form_validation->run() === FALSE) 
            {
              $_SESSION['error'] = "Atm Field Required";
              
              redirect('Reconciliation/Upload');
            }
            
            else
            {
              $atmId = $this->input->post('atmId');

              $this->file_restriction($_FILES['csvfile'],"BANKH",$atmId);
            }
          }
          # BANK Switch File Upload

          # Page Refresh 
          else
          {
            redirect('Reconciliation/Upload');
          }
        }
        else
        {
          $_SESSION['error'] = "Permission Denied. Contact Administrator";
          redirect('Dashboard');
        }
      } 
      else 
      {
        # Not Logged In
        redirect('Access');
      }
    }

    /***********************************************
      File Upload
    ************************************************/ 
    public function Start() 
    {
      if( $_SESSION['username'])
      {
        if( isset($_POST['startrecon']) )
        {
          # Sanitizing Var
          $this->form_validation->set_rules('atmId','ATM Name','required|trim');
          $this->form_validation->set_rules('comparism','Comparism','required|trim');
          $this->form_validation->set_rules('searchtime','Comparism','required|trim');
          $this->form_validation->set_rules('recon_type','Comparism','required|trim');
          
          if ($this->form_validation->run() === FALSE) 
          {
            $_SESSION['error'] = "All Field Required";
            
            redirect('Reconciliation/Main');
          }
          
          else
          {                
            $resulturl = $this->input->post('resulturl');
            $atmId  = $this->input->post('atmId');
            $comparism   = $this->input->post('comparism');
            $searchtime = explode('To',$this->input->post('searchtime'));
            $recon_type = $this->input->post('recon_type');
            
            #Changing Date To SQL Format
            $SearchStartTime = $searchtime[0];
                          
            $SearchEndTime = $searchtime[1];
                          
            $this->load->model('EventsModel');
                          
            $data['header'] = "hide";
            
            //$data['recon_res'] = $this->EventsModel->ret_recon_result($SearchStartTime,$SearchEndTime,$atmId);
            
            #Creating Socket
            if(!($sock = socket_create(AF_INET,SOCK_STREAM,0))) 
            {
              //$errorcode = socket_last_error();
              //$error_msg = socket_strerror($errorcode);
              #Error
              $_SESSION['error'] = "Could Not Create Socket To Alpha Server";
            } 
            else 
            {
              #Connecting To Socket 
              if(@!socket_connect($sock, $_SESSION['ipaddress'], $_SESSION['port'] )) 
              {
                #Connection Failed
                $_SESSION['error'] = "Could Not Connect To Alpha Server";
                //$errorcode = socket_last_error();
                //$error_msg = socket_strerror($errorcode);
                redirect($resulturl);
              }   
              else 
              {
                #Connection Established
                $Message = $atmId.",".$SearchStartTime.",".$SearchEndTime."\r\n\r\n";
                
                if(!socket_send($sock, $Message, strlen($Message), 0)) 
                {
                    #Connection Failed
                    $_SESSION['error'] = "Could Not Send Message To Alpha Server";
                    //$errorcode = socket_last_error();
                    //$error_msg = socket_strerror($errorcode);
                }   
                else 
                {
                  #Keep Listening to Socket Unitil Reply return 
                  //Now receive reply from server
                  
                  if(socket_recv ($sock , $replyfromserver , 1024 , MSG_WAITALL ) === FALSE)
                  {
                    //$errorcode = socket_last_error();
                    //$errormsg = socket_strerror($errorcode);
                    $_SESSION['error'] = "Could Not Recieve Reply From Alpha Server";
                    //die("Could not receive data: [$errorcode] $errormsg \n");
                  }  
                  else 
                  {
                    if (isset($replyfromserver) && (strcasecmp($replyfromserver,"complete") == 0))   
                    {
                      #Recon Complete
                      #Retrieving Result 
                      $data['header'] = "hide";
                      
                      $data['recon_res'] = $this->EventsModel->ret_recon_result($SearchStartTime,$SearchEndTime,$atmId);
                      if(empty($data['recon_res']))
                        @$_SESSION['error'] .= "No Records Foound";
                    }
                    elseif (isset($replyfromserver) && (strcasecmp($replyfromserver, "error") == 0))
                      $_SESSION['error'] = "Error While Processing Reconciliation";
                  }
                  #print the received message
                } 
                #End of Message Sending
              }
              #End of Socket Connection
              socket_close($sock);
            }
            #End of Socket Creation*/
                          
            if($recon_type == "3Way") 
            {
              $title['title'] = "ReConciliation";
              $this->load->view('header',$title);
              $this->load->view('3way',@$data);
              $this->load->view('footer');
            }
            # End of 3 Way
                          
            if($recon_type == "2Way") 
            {
              if(strcasecmp($comparism,"EJournal") == 0)
                $data['EJ'] = "Set";
                                  
              if(strcasecmp($comparism,"Bank") == 0)
                $data['Bank'] = "Set";
                            
              $title['title'] = "ReConciliation";
              $this->load->view('header',$title);
              $this->load->view('2way',@$data);
              $this->load->view('footer');
            }
            #End of 2 Way 
          }   
        }
      }
      else
      {
        redirect('Access');
      }
    }

	/************************** End of POST Variables Processing **************************/

  /***************************** Other Function Definition *****************************/
    /***********************************************
      File Upload Function
    ************************************************/
    public function file_restriction($file_array,$upload_type,$atmId) 
    {
      if( $_SESSION['username'])
      {
        #Loading Events Model
        $this->load->model('EventsModel');
       
        # File Upload Restrictions
        $csv_mimetypes = array('csv');
        
        $extension = explode('.',$file_array['name']);
        
        $filetype = $extension[1];
        
        if(in_array($filetype,$csv_mimetypes )) 
        {
          if($file_array['size'] > 0 && $file_array['size'] < 10000000) 
          {
            #Opening File into a Resource Variable
            $handle = fopen($file_array['tmp_name'], "r");
       
            #Performing File / Database Field Comparism
            $upload_fields = fgetcsv($handle, 1000, ",");
              
            foreach($upload_fields As $fields) 
            {
              $find = array(" " , ".");
              
              $replace_with = array("_" , "_");
              
              $file_fields[] = str_replace($find,$replace_with,$fields);
            }
              
            fclose($handle);
            
            # ATM Switch File   
            if($upload_type == "ATMH") 
            {
              $db_fields = $this->EventsModel->ret_table_fields('atmswitch');
            }

            if( count($db_fields) == count(array_intersect($file_fields, $db_fields)) )
            {
              $this->upload_file($file_array,$upload_type,$atmId);
            }
            else
            {
              $_SESSION['error'] = "File Fields and Database Fields Mismatch";
              redirect('Reconciliation/Upload');
            }
          } 

          else 
          {
            $_SESSION['error'] = "File Size Too Large.<br>Please Check File Again";
            redirect('Reconciliation/Upload');
          }                    
        } 

        else 
        {
          $_SESSION['error'] =  "File Type Not Supported.<br>Please Upload a CSV File  ";
          redirect('Reconciliation/Upload');
        }
      }
      else
      {
        redirect('Access');
      }                    
    }
    
    /***********************************************
      File Upload
    ************************************************/ 
    public function upload_file(&$file_array,$upload_type,$atmId) 
    {
      if( $_SESSION['username'])
      {
        # Loading Model
        $this->load->model('EventsModel');

        # Retrieving info about atm 
        $result = $this->EventsModel->retrieve_info('full_atminfo','TerminalId',$atmId);

        foreach($result As $res)
        {
          $AtmName  = $res->AtmName;
        }
        
        $target_dir = "Recon Files Upload/".$upload_type;
        # target_dir = Recon Files Upload/( ATMH/ BANKH )
        
        if(file_exists($target_dir)) 
        {
          ContinueUpload:
          
          if (file_exists($target_dir)) 
          {
            $target_dir .= "/".$AtmName.".csv"; 
            #target_dir = Recon Files Upload/( ATMH/ BANKH )/AtmName.csv
                  
            if(!move_uploaded_file($file_array['tmp_name'], $target_dir))
            {
              $_SESSION['error'] = "Error uploading file - Check Destination Directory is Writeable.')";
            } 
            else 
            {
              $_SESSION['success'] = "File Uploaded";
                       
              if($upload_type == "ATMH")
                $result = $this->EventsModel->dbinsert($target_dir,"atmswitch",$atmId);
              else if($upload_type == "BANKH") 
                $result = $this->EventsModel->dbinsert($target_dir,"bankswitch",$atmId);
                          
              if ($result) 
              {
                # code...
                $_SESSION['success'] .= "<br><br>Database Updated" ;
              }  
              else 
              {
                #code
                $_SESSION['error'] = $_SESSION['success']."<br><br>Database Update Failed";
              }
          
              redirect('Reconciliation/Upload');
            }
          } 
          else 
          {
            $_SESSION['error'] = "Upload Directory Creation Failed.Contact Administrator";
            redirect('ReConciliation/Upload');
          }
        }  
        else
        {
          //Send Confirmation For 
          if(mkdir($target_dir,0755))
            goto ContinueUpload;
        }
      }
      else
      {
        redirect('Access');
      }
    }

  /***************************** Other Function Definition *****************************/

	}
	# End of Class
?>