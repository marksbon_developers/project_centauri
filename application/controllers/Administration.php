<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Administration extends CI_Controller 
{
	/***************************************** Interfaces 	***********************************/
		/***********************************************
			Constructor + Initialization
		************************************************/
		public function index() 
		{
			if (isset($_SESSION['username'])) 
				redirect('Dashboard');
			
			else 
				redirect('Access');
		}
    
	  /***********************************************
				Adding New ATM
		************************************************/
		public function New_ATM() 
		{
			if(isset($_SESSION['username']) && ($_SESSION['role'] == 'Administrator' || $_SESSION['role'] == 'GreenSunAdmin'))  
			{
				# Loading Models
	      $this->load->model('EventsModel');
				
				/******* interface *********/
				$title['title'] = "Add New ATM";
				$this->load->view('header',$title);
				$this->load->view('nav-md');
				$this->load->view('atm_add');
				$this->load->view('footer');
				/******* interface *********/
			} 
			else 
				redirect('Dashboard');
		}

		/***********************************************
				 Manage All ATM Information
		************************************************/
		public function Manage_ATM() 
		{
			if(isset($_SESSION['username']) && ($_SESSION['role'] == 'Administrator' || $_SESSION['role'] == 'GreenSunAdmin'))  
			{
				# Loading Model
				$this->load->model('EventsModel');
	            
				/******* interface *********/
				$title['title'] = "Manage All ATMs";
				$this->load->view('header',$title);
				$this->load->view('nav-md');
				$this->load->view('atm_manage');
				$this->load->view('footer');
				/******* interface *********/
			} 
			else 
				redirect('Dashboard');
		}

		/***********************************************
				Adding New Branch
		************************************************/
		public function New_Branch() 
		{
			if ( isset($_SESSION['username']) && ($_SESSION['role'] == 'Administrator' || $_SESSION['role'] == 'GreenSunAdmin'))  
			{
				# Loading Model
	      $this->load->model('EventsModel');
				
				/******* interface *********/
				$title['title'] = "Add New Branch";
				$this->load->view('header',$title);
				$this->load->view('nav-md');
				$this->load->view('branches');
				$this->load->view('footer');
				/******* interface *********/
			} 
			else 
				redirect('Dashboard');
		}

		/***********************************************
				 Managing Branch Information
		************************************************/
		public function Manage_Branch() 
		{
			if(isset($_SESSION['username']) && ($_SESSION['role'] == 'Administrator' || $_SESSION['role'] == 'GreenSunAdmin'))  
			{
				# Loading Model
				$this->load->model('EventsModel');

				/******* interface *********/
				$title['title'] = "Manage All Branches";
				$this->load->view('header',$title);
				$this->load->view('nav-md');
				$this->load->view('branches_manage');
				$this->load->view('footer');
				/******* interface *********/
			} 
			else 
				redirect('Dashboard');
		}

		/***********************************************
				 ATM Regions
		************************************************/
		public function New_Group() 
		{
			if(isset($_SESSION['username']) && ($_SESSION['role'] == 'Administrator' || $_SESSION['role'] == 'GreenSunAdmin'))  
	    {
	    	/******* interface *********/
	    	$title['title'] = "Add ATM Group";
				$this->load->view('header',$title);
				$this->load->view('nav-md');
				$this->load->view('atm_group');
				$this->load->view('footer');
				/******* interface *********/
			}
			else
				redirect('Dashboard');
		}

		/***********************************************
				 Manage ATM Group
		************************************************/
		public function Manage_Groups() 
		{
			if(isset($_SESSION['username']) && ($_SESSION['role'] == 'Administrator' || $_SESSION['role'] == 'GreenSunAdmin'))  
	    {
	    	# Loading Model
	    	$this->load->model('EventsModel');

				/******* interface *********/
				$title['title'] = "Manage ATM Group";
				$this->load->view('header',$title);
				$this->load->view('nav-md');
				$this->load->view('atm_group_manage');
				$this->load->view('footer');
				/******* interface *********/
			}
			else 
				redirect('Dashboard');
		}

		/***********************************************
			User Registration
		************************************************/
		public function User_Registration() 
		{
			if(isset($_SESSION['username']) && ($_SESSION['role'] == 'Administrator' || $_SESSION['role'] == 'GreenSunAdmin'))  
	    {
	    	# Loading Model
				$this->load->model('EventsModel');

				$data['roles'] = $this->EventsModel->Roles_Retreieve();
				$data['atmgroups'] = $this->EventsModel->atm_group_retrieve();

				/******* interface *********/
				$title['title'] = "User Registration";
				$this->load->view('header',$title);
				$this->load->view('nav-md');
				$this->load->view('new_user',$data);
				$this->load->view('footer');
				/******* interface *********/
			}
			else 
				redirect('Access');
		}

		/***********************************************
				Manage Users
		************************************************/
		public function User_Management() 
		{
			if(isset($_SESSION['username']) && ($_SESSION['role'] == 'Administrator' || $_SESSION['role'] == 'GreenSunAdmin' ))  
	    {
	    	#Loading Model 
	    	$this->load->model('EventsModel');
				$this->load->model('UserModel');

				$data['allusers'] = $this->UserModel->allusers_view();
				$data['atmgroups'] = $this->EventsModel->atm_group_retrieve();
				$data['roles'] = $this->EventsModel->Roles_Retreieve();
				$data['bran_grp_rln'] = [];
				
				$allbranches = $this->EventsModel->all_branches();
                      
        if(!empty($allbranches)) :
        	$counter = 0;
          foreach ($allbranches as $val) :
          	$group = $this->EventsModel->group_id_name_ret($val->group_id);

          	if(array_key_exists($group->GroupName, $data['bran_grp_rln'])) :
          		$data['bran_grp_rln'][$group->GroupName][$counter]['branchname'] = $val->name;
          		$data['bran_grp_rln'][$group->GroupName][$counter]['branchid'] 	 = $val->branch_id;
          	
          	else :
          		$data['bran_grp_rln'][$group->GroupName]=[];
          		$data['bran_grp_rln'][$group->GroupName][$counter]['branchname'] = $val->name;
          		$data['bran_grp_rln'][$group->GroupName][$counter]['branchid']   = $val->branch_id;
          	endif;
          	$counter++;
          endforeach;
        endif;
//print_r($data['bran_grp_rln']);
				/******* interface *********/
				$title['title'] = "Manage Users";
				$this->load->view('header',$title);
				$this->load->view('nav-md');
				$this->load->view('user_manage',$data);
				$this->load->view('footer');
				/******* interface *********/
			}
			
			else
				redirect('Access');
		}

		/***********************************************
			Recon Server Settings
		************************************************/
		public function Settings() 
		{
			if(isset($_SESSION['username']) && ($_SESSION['role'] == 'Administrator' || $_SESSION['role'] == 'GreenSunAdmin'))  
	    {
	    	# Load model
				$this->load->model('EventsModel');

				$data['serverinfo'] = $this->EventsModel->ret_server_info(); 

				/****** interface *******/
				$title['title'] = "Recon Server Settings";
				$this->load->view('header',$title);
				$this->load->view('nav');
				$this->load->view('settings',$data);
				$this->load->view('footer');
				/****** interface *******/
			}
			else 
				redirect('Dashboard');
		}

	/***************************************** Interfaces 	***********************************/

	/***************************************** Post Methods 	*********************************/

	/************************* Insertions 	************************/
		/***********************************************
					Create ATM 
		************************************************/
	  public function ATM_Create() 
	  {
	  	if (isset($_SESSION['username'])) 
			{
				if(isset($_POST['addatm']))
	  		{
			    //Setting validation rules
					$this->form_validation->set_rules('atm_name','ATM Name','required|trim');
					$this->form_validation->set_rules('atmtype','ATM Type','required|trim');
					$this->form_validation->set_rules('terminalid','Terminal ID','required|trim');
					$this->form_validation->set_rules('terminalip','Terminal IP','required|trim');
		      $this->form_validation->set_rules('branch_id','Location','required|trim');
					$this->form_validation->set_rules('location','Location','required|trim');
				
					# If Failed
			    if ($this->form_validation->run() === FALSE) 
			    {
			        #Redirecting With Error
			        $_SESSION['error'] = validation_errors();
			        //redirect('Administration/New_ATM');
			    }
			    
			    # If Passed
			    else
			    {
			    	# Variables Assignment
						$data['atmName'] 		= $this->input->post('atm_name');
						$data['atmType'] 		= $this->input->post('atmtype');
						$data['terminalid'] 	= $this->input->post('terminalid');
						$data['terminalip']		= $this->input->post('terminalip');
			      $data['branch_id']		= $this->input->post('branch_id');
						$data['location']		= $this->input->post('location');

						# Loading model
						$this->load->model('EventsModel');

						# Duplicate ATM Name Search
						$atmresult = $this->EventsModel->dup_atminfo('infoatm','atmName',$data['atmName']);
						if ($atmresult) 
						{
							$_SESSION['error'] .= "ATM Name Already Exists<br>";
						}
						# Duplicate ATM Name Search

						# Duplicate Terminal ID Check 
						$idresult = $this->EventsModel->dup_atminfo('infoatm','terminalid',$data['terminalid']);
						if ($idresult) 
						{
							$_SESSION['error'] .= "Terminal Id Already Exists<br>";
						}
						# Duplicate Terminal ID Check 

						# Duplicate Terminal IP Check 
						$ipresult = $this->EventsModel->dup_atminfo('infoatm','terminalip',$data['terminalip']);
						if ($ipresult) 
						{
							$_SESSION['error'] .= "Terminal IP Already Exists<br>";
						}
						# Duplicate Terminal IP Check 

						# All Checks Passed
						if (!isset($_SESSION['error'])) 
						{
							$result = $this->EventsModel->atm_create($data);
							if($result) 
							{
								$_SESSION['success'] = "ATM Successfully Created";
								redirect('Administration/New_ATM');
							} 
							else 
							{
								$_SESSION['error'] = "Failed to Create ATM";
								redirect('Administration/New_ATM');
							}
						}
						# All Checks Passed

						# If Any Failed
						redirect('Administration/New_ATM');
						# If Any Failed
			    }
			  }
			  # End of Post
			  else
			  {
			  	$_SESSION['error'] = "Permission Denied. Contact Admin";
			  	redirect('Administration/New_ATM');
			  }
			}
			else 
			{
				redirect('Access');
			}
	  }

	  /***********************************************
					Create Branch 
		************************************************/
	  public function Create_Branch() 
	  {
	  	if (isset($_SESSION['username'])) 
			{
				if(isset($_POST['create_branch']))
	  		{
			    //Setting validation rules
					$this->form_validation->set_rules('branch_name','Branch Name','required|trim');
					$this->form_validation->set_rules('group_id','Group Name','required|trim');
				
					# If Failed
			    if ($this->form_validation->run() === FALSE) 
			    {
			        #Redirecting With Error
			        $_SESSION['error'] = "All Fields Required";
			        redirect('Administration/New_Branch');
			    }
			    
			    # If Passed
			    else
			    {
			    	# Variables Assignment
						$data['name'] 		= $this->input->post('branch_name');
						$data['group_id'] 	= $this->input->post('group_id');

						# Loading model
						$this->load->model('EventsModel');

						//ATM Group Name Duplicate Search
						$nameresult = $this->EventsModel->dup_branch('branches',$data);

						if ($nameresult) 
						{
							$_SESSION['error'] .= "Duplicate Combination Found<br>";
						}

						if (!isset($_SESSION['error'])) 
						{
							# code...
	            $result = $this->EventsModel->group_id_name_ret($data['group_id']);
	                
	            foreach ($result as $rep) 
	            {
	              $group_name = $rep->GroupName;
	            }
	                
							$result = $this->EventsModel->branch_create($data);
	                
	            if($result) 
	            {
	            	/*$upload_folder = "Uploads/".$group_name; 
	              
	              $logs_folder = "Logs/".$group_name; 

	              if(file_exists($upload_folder) && file_exists($logs_folder)) 
	              {
	                #Logic
	                $upload_folder = "Uploads/".$group_name."/".$data['name']; 

	                $logs_folder = "Logs/".$group_name."/".$data['name'];
	                        
	                # Creating Branch Folder
	                if(!file_exists($upload_folder) && !file_exists($logs_folder)) 
	                {
	                  if(mkdir($upload_folder,'755')) 
	                  {
	                    $ATMF = $upload_folder ."/ATMH";
	                    
	                    mkdir($ATMF,'755');
	                                
	                    $BANKF = $upload_folder ."/BANKH";
	                                
	                    mkdir($BANKF,'755');
	                                
	                    if(mkdir($logs_folder,'755')) 
	                    {
	                      $_SESSION['success'] = "Branch Successfully Created";

	                      redirect('Administration/New_Branch');
	                    }
	                  }
	                }
	                # End of Creating Branch Folder
	              }
	              else 
	              {
	                $_SESSION['error'] = "File {$data['name']} Already Exists";
	                redirect('Administration/New_Branch');
	              }
	              # */
	              $_SESSION['success'] = "Branch Successfully Created";

	              redirect('Administration/New_Branch');
	            } 
	            else 
	            {
								$_SESSION['error'] = "Failed to Create Branch";
								redirect('Administration/New_Branch');
							}
						}
						# If Any Failed
						else
							redirect('Administration/New_Branch');
						# If Any Failed
			    }
			  }
			  # End of Post
			  else
			  {
			  	$_SESSION['error'] = "Permission Denied. Contact Admin";
			  	redirect('Administration/New_Branch');
			  }
			}
			else 
			{
				redirect('Access');
			}
	  }

	  /***********************************************
			Server Setting
		************************************************/
	  public function Set_Server() 
	  {
	  	if (isset($_SESSION['username'])) 
			{
				if(isset($_POST['serverupdate']))
	  		{
			    //Setting validation rules
					$this->form_validation->set_rules('serverip','ATM Name','required|trim');
					$this->form_validation->set_rules('serverport','ATM Type','required|trim');
				
					# If Failed
			    if ($this->form_validation->run() === FALSE) 
			    {
			      $_SESSION['error'] = "All Fields Required";
			      redirect('Administration/Settings');
			    }
			    else
			    {
						$data['serverip'] 		= $this->input->post('serverip');
						$data['serverport'] 		= $this->input->post('serverport');

						$this->load->model('EventsModel');

						$result = $this->EventsModel->server_register($data);

						if($result) 
						{
							$_SESSION['success'] 		= "Server Info Updated";
							$_SESSION['ipaddress'] 	= $data['serverip'];
							$_SESSION['port'] 			= 	$data['serverport'];
							redirect('Administration/Settings');
						} 
						else 
						{
							$_SESSION['error'] = "Server Info Update Failed";
							redirect('Administration/Settings');
						}
			    }
			  }
			  # End of Post
			  else
			  {
			  	$_SESSION['error'] = "Permission Denied. Contact Admin";
			  	redirect('Administration/Settings');
			  }
			}
			else 
			{
				redirect('Access');
			}
	  }

  /************************* Insertions 	************************/

  /************************* Updating 	************************/
	  /***********************************************
					ATM Update
		************************************************/
	  public function ATM_Update() 
	  {
	  	if (isset($_SESSION['username'])) 
			{
				if(isset($_POST['update_atm']))
	  		{
			    //Setting validation rules
					$this->form_validation->set_rules('atm_id','ATM Name','required|trim');
					$this->form_validation->set_rules('atm_type','ATM Type','required|trim');
					$this->form_validation->set_rules('terminalid','Terminal ID','required|trim');
					$this->form_validation->set_rules('terminalip','Terminal IP','required|trim');
	        $this->form_validation->set_rules('branch_id','Branch Name','required|trim');
					$this->form_validation->set_rules('location','Location','required|trim');
				
					# If Failed
			    if ($this->form_validation->run() === FALSE) 
			    {
			        #Redirecting With Error
			        //$_SESSION['error'] = "All Fields Required";
			        redirect('Administration/Manage_ATM');
			    }
			    
			    # If Passed
			    else
			    {
			    	# Variables Assignment
						$data['atmName'] 		= $this->input->post('atm_name');
						$data['atmType'] 		= $this->input->post('atm_type');
						$data['terminalid'] 	= $this->input->post('terminalid');
						$data['terminalip']		= $this->input->post('terminalip');
	          $data['branch_id']		= $this->input->post('branch_id');
						$data['location']		= $this->input->post('location');
						$data['atmId']			= $this->input->post('atm_id');

						# Loading model
						$this->load->model('EventsModel');

						//ATM Group Name Duplicate Search
						$result = $this->EventsModel->atm_update($data);

						if($result) 
							$_SESSION['success'] = "ATM Info Updated";
						
						else 
							$_SESSION['error'] = "ATM Info Update Failed";
						
						redirect('Administration/Manage_ATM');
						# If Any Failed
			    }
			  }
			  # End of Post
			  else
			  {
			  	$_SESSION['error'] = "Permission Denied. Contact Admin";
			  	redirect('Administration/Manage_ATM');
			  }
			}
			else 
			{
				redirect('Access');
			}
	  }

	  /***********************************************
					ATM Banch Update
		************************************************/
	  public function Update_Branch() 
	  {
	  	if (isset($_SESSION['username'])) 
			{
				if(isset($_POST['update_branch'])) //
	  		{
			    //Setting validation rules
					$this->form_validation->set_rules('branch_name','Branch Name','required|trim');
					$this->form_validation->set_rules('group_id','Group Name','required|trim');
	        $this->form_validation->set_rules('branch_id','Branch ID','required|trim');
				
					# If Failed
			    if ($this->form_validation->run() === FALSE) 
			    {
			        #Redirecting With Error
			        $_SESSION['error'] = "All Fields Required";
			        redirect('Administration/Manage_Branch');
			    }
			    
			    # If Passed
			    else
			    {
			    	# Variables Assignment
						$data['name'] 		= $this->input->post('branch_name');
	          $data['branch_id'] 	= $this->input->post('branch_id');
						$data['group_id'] 	= $this->input->post('group_id');

						# Loading model
						$this->load->model('EventsModel');

						//ATM Group Name Duplicate Search
						$result = $this->EventsModel->branch_update($data);;

						if ($result) 
						{
							$_SESSION['success'] = "Branch Update successful";
							redirect('Administration/Manage_Branch');
						}
						else 
						{
							$_SESSION['error'] = "Branch Update Failed";
							redirect('Administration/Manage_Branch');
						}
						# If Any Failed
			    }
			  }
			  # End of Post
			  else
			  {
			  	$_SESSION['error'] = "Permission Denied. Contact Admin";
			  	redirect('Administration/Manage_Branch');
			  }
			}
			else 
			{
				redirect('Access');
			}
	  }

  /************************* Updating 	************************/

	/***************************************** Post Insert 	*********************************/

	/***************************************** Post Delete 	*********************************/
	 /***********************************************
				Delete ATM 
		************************************************/
    public function ATM_Delete() 
    {
    	if( isset($_SESSION['username']) )  
    	{
    		if ( $_SESSION['role'] == 'Administrator' || $_SESSION['role'] == 'GreenSunAdmin' ) 
				{
    			if(isset($_POST['delete_atm'])) //
    			{
	    			# Validation rules
						$this->form_validation->set_rules('atmId','ATM ID','required|trim');
			
						# If Failed
	          if ($this->form_validation->run() === FALSE) 
	          {
	            $_SESSION['error'] = "Atm Required";
	            redirect('Administration/Manage_ATM');
	          }
	          
						$atmId		= $this->input->post('atmId');
						
						# Loading Model 
						$this->load->model('EventsModel');

						$result = $this->EventsModel->atm_delete($atmId);

						if($result) 
						{
							$_SESSION['success'] = "ATM Deleted";
							redirect('Administration/Manage_ATM');
						} 
						else 
						{
							$_SESSION['error'] = "ATM Delete Failed";
							redirect('Administration/Manage_ATM');
						}
					} # End of Post
				}
				else
				{
					$_SESSION['error'] = "Permission Denied. Contact Administrator";
					redirect('Administration/Manage_ATM');
				} # End of permission
			} 
		 	else
		 	{
		 		# Not Logged IN
				redirect('Access');
			}
    }

    /***********************************************
				Delete Branch 
		************************************************/
    public function Delete_Branch() 
    {
    	if( isset($_SESSION['username']) )  
    	{
    		if ( $_SESSION['role'] == 'Administrator' || $_SESSION['role'] == 'GreenSunAdmin' ) 
				{
    			if(isset($_POST['delete_branch'])) 
    			{
	    			# Validation rules
						$this->form_validation->set_rules('branch_id','Branch ID','required|trim');
			
						# If Failed
	          if ($this->form_validation->run() === FALSE) 
	          {
	            $_SESSION['error'] = "Branch Required";
	            redirect('Administration/Manage_Branch');
	          }
	          else
	          { 
		          $data['branch_id']	= $this->input->post('branch_id');
							
							# Loading Model 
	            $this->load->model('EventsModel');

	            $branch_res = $this->EventsModel->branch_delete($data);
	            
	            if($branch_res) 
							{
                $_SESSION['success'] = "Branch Deleted<br/>";

                $result = $this->EventsModel->branch_atm_delete($data);

                if($result) 
                	$_SESSION['success'] .= "ATM Deleted<br/>";
                else
                	$_SESSION['success'] .= "No ATMs Were Registed";
								
								redirect('Administration/Manage_Branch');
							} 
							else 
							{
								$_SESSION['error'] = "Branch Delete Failed";
								redirect('Administration/Manage_Branch');
							}
						} # End of Validation
					} # End of Post
				}
				else
				{
					$_SESSION['error'] = "Permission Denied. Contact Administrator";
					redirect('Administration/Manage_Branch');
				} # End of permission
			} 
		 	else
		 	{
		 		# Not Logged IN
				redirect('Access');
			}
    }

    /***********************************************
				Delete Branch 
		************************************************/
    public function Group_Delete() 
    {
    	if( isset($_SESSION['username']) )  
    	{
    		if ( $_SESSION['role'] == 'Administrator' || $_SESSION['role'] == 'GreenSunAdmin' ) 
				{
    			if(isset($_POST['delete_group']))
    			{
	    			# Validation rules
						$this->form_validation->set_rules('atmGroupId','ATM Group ID','required|trim');
			
						# If Failed
	          if ($this->form_validation->run() === FALSE) 
	          {
	            $_SESSION['error'] = "Region Required";
	            redirect('Administration/Manage_Groups');
	          }
	          else
	          { 
		          $data['atmGroupId']		= $this->input->post('atmGroupId');
							
							# Loading Model 
	            $this->load->model('EventsModel');

	            $group_res = $this->EventsModel->group_delete($data);
	            
	            if($group_res) 
							{
                $_SESSION['success'] = "ATM Region Deleted<br/>";

                //$result = $this->EventsModel->branch_atm_delete($data['branch_id']);

                //if($result) 
                	//$_SESSION['success'] .= "ATM Deleted<br/>";
                //else
                	//$_SESSION['success'] .= "No ATMs Were Registed";
								
								redirect('Administration/Manage_Groups');
							} 
							else 
							{
								$_SESSION['error'] = "Atm Region Delete Failed";
								redirect('Administration/Manage_Groups');
							}
						} # End of Validation
					} # End of Post
				}
				else
				{
					$_SESSION['error'] = "Permission Denied. Contact Administrator";
					redirect('Administration/Manage_Groups');
				} # End of permission
			} 
		 	else
		 	{
		 		# Not Logged IN
				redirect('Access');
			}
    }

	/***************************************** Post Delete 	*********************************/



	/***************************************** Post Retrieve 	******************************/
		/************************************************
				Delete Branch 
		************************************************/
    public function Branch_Search() 
    {
    	if( isset($_SESSION['username']) )  
    	{
    		if ( $_SESSION['role'] == 'Administrator' || $_SESSION['role'] == 'GreenSunAdmin' ) 
				{
    			if(isset($_POST['branch_search']))
    			{
	    			# Validation rules
						$this->form_validation->set_rules('group_id','ATM Region','required|trim');
			
						# If Failed
	          if ($this->form_validation->run() === FALSE) 
	          {
	            $_SESSION['error'] = "ATM Region Required";
	            redirect('Administration/Manage_Branch');
	          }
	          else
	          { 
		          $data['group_id']		= $this->input->post('group_id');
							
							# Loading Model 
	            $this->load->model('EventsModel');

	            $branch_res = $this->EventsModel->search_branch($data);
	            
	            if($branch_res) 
							{
                $this->session->set_flashdata('result',$branch_res);
								redirect('Administration/Manage_Branch');
								//print_r($branch_res);
							} 
							else 
							{
								$_SESSION['error'] = "No Record Found";
								redirect('Administration/Manage_Branch');
							}
						} # End of Validation
					} 
					else
						redirect('Administration/Manage_Branch');
					# End of Post
				}
				else
				{
					$_SESSION['error'] = "Permission Denied. Contact Administrator";
					redirect('Administration/Manage_Branch');
				} # End of permission
			} 
		 	else
		 	{
		 		# Not Logged IN
				redirect('Access');
			}
    }


	/***************************************** Post Retrieve 	******************************/
}