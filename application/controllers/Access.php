<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Access extends CI_Controller 
	{
	
	/************************ Other Functions ***********************/
		/***********************************************
				Contructor
		************************************************/
		public function index() 
		{
			$this->login();
		}

		/***********************************************
			Password Encryption
		************************************************/
		public function encrypt_password($password) 
		{
			$encrypted_passwd = password_hash($password,PASSWORD_BCRYPT);
				
			return ($encrypted_passwd);
		}
	
		/***********************************************
			Password Decryption
		************************************************/
		public function verify_password($password,$encrypted_password) 
		{
			$encrypted_password = str_replace('"',"''",$encrypted_password);
			
			return((password_verify($password,$encrypted_password)) ? TRUE : FALSE);
		}

		/***********************************************
		  Login Validation 
		************************************************/
		public function login_validation() 
		{
			if(isset($_POST['login'])) 
			{
				$this->form_validation->set_rules('username', 'username', 'required|trim');
	      $this->form_validation->set_rules('password', 'Password', 'required|trim');

	      if($this->form_validation->run() === FALSE) 
        {
          $_SESSION['error'] = 'All Fields Required';
          redirect('Access');
        }
        else
        {
        	$username = $this->input->post('username');
	        $passwd = $this->input->post('password');

	        # Loading model / username lookup
	        $this->load->model('UserModel');
	        $data = $this->UserModel->dblookup($username);

	        # Username Verified => password verification 
					if($this->verify_password($passwd, $data['encrypted_password'])) 
					{
						if($data['status'] == 1)
						{
							# Session Variables Setting
							$_SESSION['username'] 	= $data['userName'];
							$_SESSION['fullname'] 	= $data['fullName'];
							$_SESSION['role']		= $data['role'];
							$_SESSION['userid']		= $data['userid'];

							# Logging sign-in
							$loginID = $this->UserModel->login_log($_SESSION['userid']);
							$_SESSION['LoginID']		= $loginID;

							# Loading Server Info
							$this->load->model('EventsModel');

							$res = $this->EventsModel->ret_server_info();

							$_SESSION['ipaddress'] = $res->serverip;
							$_SESSION['port'] = 	$res->serverport;
							
							# Login  Successful ==> Dashboard
							redirect('Dashboard');
						}
						else
						{
							$data['title'] = "Login";
							$data['invalid'] = "Account Deactivated. Please Contact Administrator ";
							$this->load->view('login',$data);
						}	
					}
					else 
					{
            $data['title'] = "Login";
						$data['invalid'] = "Invalid Username and Password Combination<br/>Authorized Personnel Only";
						$this->load->view('login',$data);
					}
        }
	    }	
	    else 
	      redirect('Access/login');
	  }

	  /***********************************************
		  Logout 
		************************************************/
		public function logout() 
		{	
			$this->load->model('UserModel');
			$result = $this->UserModel->logout_log($_SESSION['LoginID']);

			if($result) 
			{
				session_destroy();
				redirect('Access/login');
			}
			else 
			{
				$_SESSION['error'] = "Log Out Failed";
				redirect('Dashboard');
			}
		}

		/***********************************************
      Update User Password
		************************************************/
		public function Update_Password() 
		{
			if(isset($_SESSION['username'])) 
			{
				# Setting validation rules
				$this->form_validation->set_rules('CurrentPwd','Current Password','required|trim');
				$this->form_validation->set_rules('password','New Password','required|trim');
				$this->form_validation->set_rules('confirm_password','Confirm Password','required|trim');
			
			$currentpwd 	= $this->input->post('CurrentPwd');
			$newpwd 		= $this->input->post('password');
			$confirmpwd 	= $this->input->post('confirm_password');
			$userId 		= $this->input->post('userid');

			if (strcasecmp($newpwd, $confirmpwd) == 0 ) {
				
				//loading model / username lookup
	        	$this->load->model('UserModel');
				$data = $this->UserModel->dblookup($_SESSION['username']);

				if ($this->verify_password($currentpwd, $data['encrypted_password'])) {
					
					//Encrypting New password
					$newpwd_encrypted = $this->encrypt_password($newpwd);

					//Update passwd in users
					$result = $this->UserModel->changepwd($newpwd_encrypted,$userId);
					
					if ($result) {
						# code...
						$_SESSION['success'] = "Password Changed";
						redirect('Dashboard');
					
					} else {
						$_SESSION['error'] = "Password Changing Failed";
						redirect('Dashboard');
					}
				
				} else {
					$_SESSION['error'] = "Invalid Current Password";
					redirect('Dashboard');
				}

			} else {
				$_SESSION['error'] = "Password Does not Match";
				redirect('Dashboard');
			}
			}
			else {
					$_SESSION['error'] = "Change Password Command Failed";
					redirect('Dashboard');
				}
		}

		/***********************************************
      Activate User
		************************************************/
		public function Activate_User() 
		{
			if(isset($_SESSION['username'])) 
			{
				# Setting validation rules
				$this->form_validation->set_rules('userId','User','required|trim');

				if($this->form_validation->run() === FALSE) 
        {
          $_SESSION['error'] = 'No User Selected';
          redirect('Administration/User_Management');
        }
        else
        {
        	$this->load->model('UserModel');

        	$result = $this->UserModel->user_status(base64_decode($this->input->post('userId')),1);

        	if($result)
        		$_SESSION['success'] = "User Activated";
        	else
        		$_SESSION['error'] = "User Activation Failed";

        	redirect('Administration/User_Management');
        }
      }
      else
      	redirect('Access');
    }

    /***********************************************
      Deactivate User
		************************************************/
		public function Deactivate_User() 
		{
			if(isset($_SESSION['username'])) 
			{
				# Setting validation rules
				$this->form_validation->set_rules('userId','User','required|trim');

				if($this->form_validation->run() === FALSE) 
        {
          $_SESSION['error'] = 'No User Selected';
          redirect('Administration/User_Management');
        }
        else
        {
        	$this->load->model('UserModel');

        	$result = $this->UserModel->user_status(base64_decode($this->input->post('userId')),0);

        	if($result)
        		$_SESSION['success'] = "User Activated";
        	else
        		$_SESSION['error'] = "User Activation Failed";

        	redirect('Administration/User_Management');
        }
      }
      else
      	redirect('Access');
    }
			
	/************************ Other Functions ***********************/

	/************************ Interfaces  ***************************/
		/***********************************************
			Login Page 
		************************************************/
		public function login() 
		{
			if(isset($_SESSION['username'])) 
				redirect('Dashboard');
			
			else
			{
				//loading login view
				$title['title'] = "Login";
				$this->load->view('login',$title);
			} 
		}
	/************************ Interfaces  ***************************/

	/************************ POST INSERTION  ***********************/
		/***********************************************
			New User Registration
		************************************************/
    public function Register_User() 
    {
    	if(isset($_SESSION['username'])) 
    	{
				# Setting validation rules
				$this->load->library('form_validation');
				$this->form_validation->set_rules('fullname','fullname','required|trim');
				$this->form_validation->set_rules('branch_id','Bank Branch','required|trim');
				$this->form_validation->set_rules('username','Username','trim|required');
				$this->form_validation->set_rules('password','Password','trim|required');
				$this->form_validation->set_rules('password_repeat','Confirm Password','required|trim');
				$this->form_validation->set_rules('roleid','Role','required|trim');
				
				if($this->form_validation->run() === FALSE) 
        {
          $_SESSION['error'] = 'All Fields Required';
          redirect('Administration/User_Registration');
        }
        else
        {

					if(strcasecmp($this->input->post('password'), $this->input->post('password_repeat')) == 0 ) 
					{
						# Loading Model
						$this->load->model('UserModel');

						//Variable initializations
						$data['fullName']	 	= $this->input->post('fullname');
						$data['userName']	 	= $this->input->post('username');
						$data['passWord']	 	= $this->input->post('password');
						$data['branch_id']	 	= $this->input->post('branch_id');
						$data['roleId']	 		= $this->input->post('roleid');
						$data['userCreatedBy']	= $_SESSION['fullname'];
						$data['dateCreated']	= gmdate('Y-m-d H:i:s');
						$data['passWord'] = $this->encrypt_password($data['passWord']);
			
						//loading model register
						$result = $this->UserModel->register_user($data);

						if($result) 
							$_SESSION['success'] = "User Registration Successful";
					
						else 
							$_SESSION['error'] = "Employee Registering Failed";
						
						redirect('Administration/User_Registration');
					}
					else 
						$_SESSION['error'] = "Passwords Do Not Match";
					
					redirect('Administration/User_Registration');
				}
			}
			else 
				redirect('Access');
		}
	/************************ POST INSERTION  ***********************/

	/************************ POST UPDATE     ***********************/
		/***********************************************
			Updating User Info
		************************************************/
    public function Update_User() 
    {
			if(isset($_SESSION['username'])) 
			{
				# Setting validation rules
				$this->form_validation->set_rules('fullname','fullname','required|trim');
				$this->form_validation->set_rules('branch_id','Bank Branch','required|trim');
				$this->form_validation->set_rules('username','Username','trim|required');
				$this->form_validation->set_rules('password','Password','trim|required');
				$this->form_validation->set_rules('password_repeat','Confirm Password','required|trim');
				$this->form_validation->set_rules('roleid','Role','required|trim');
				$this->form_validation->set_rules('userId','Role','required|trim');

				if($this->form_validation->run() === FALSE) 
        {
          $_SESSION['error'] = 'All Fields Required';
          redirect('Administration/User_Management');
        }
        else
        {
					if (strcasecmp($this->input->post('password'), $this->input->post('password_repeat')) == 0 ) 
					{
						# Variable initializations
						$data['fullName']	 	= $this->input->post('fullname');
						$data['userName']	 	= $this->input->post('username');
						$data['passWord']	 	= $this->input->post('password');
						$data['branch_id']	 	= $this->input->post('branch_id');
						$data['department']	 	= $this->input->post('dept');
						$data['roleId']	 		= $this->input->post('roleid');
						$data['userId']			= base64_decode($this->input->post('userId'));

						//loading model
						$this->load->model('UserModel');

						$result = $this->UserModel->user_passwd($data['userId']);

						foreach ($result as $ue) 
						{
							$dbpasswd = $ue->passWord;
						}

						if (strcasecmp($dbpasswd, $data['passWord']) == 0) 
							$data['passWord'] = $dbpasswd;
						
						else
							$data['passWord'] = $this->encrypt_password($data['passWord']);
				
						$result = $this->UserModel->update_user($data);

						if($result)
							$_SESSION['success'] = "User Info Update Successful";
						
						else 
							$_SESSION['error'] = "User Info Update Failed";
						
						redirect('Administration/User_Management');
					}
					else 
						$_SESSION['error'] = "Passwords Do Not Match";
					
					redirect('Administration/User_Management');
				}
			}
			else 
				redirect('Access');
		}
	/************************ POST UPDATING   ***********************/
		
  /************************ POST DELETION   ***********************/
		/***********************************************
	            Delete User
		************************************************/
		public function Delete_User() 
		{
			if(isset($_SESSION['username'])) 
			{
				# Setting validation rules
				$this->form_validation->set_rules('userId','User','required|trim');

				if($this->form_validation->run() === FALSE) 
        {
          $_SESSION['error'] = 'No User Selected';
          redirect('Administration/User_Management');
        }
        else
        {
					$this->load->model('UserModel');
					$userId = base64_decode($this->input->post('userId'));
					$result = $this->UserModel->delete_user($userId);

					if($result) 
						$_SESSION['success'] = "User Deleted";
					
					else 
						$_SESSION['error'] = "User Deletion Failed";
				
					redirect('Administration/User_Management');
				}	
			}	
			else 
				redirect('Administration/User_Management');
		}
	/************************ POST DELETION   ***********************/

}//End of Class
