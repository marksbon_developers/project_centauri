<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	
	/***********************************************
		Constructor + Initialization
	************************************************/
	public function index() 
	{
		if (isset($_SESSION['username'])) 
		{
			//loading model and functions
			$this->load->model('UserModel');

			/******** interface *******/
			$title['title'] = "Dashboard";
			$this->load->view('header',$title);
			//$this->load->view('nav-md',$title);
			$this->load->view('grcon');
			$this->load->view('footer');
			/******** interface *******/
		
		} 
		else 
		{
			redirect('Access');
		}
	}
    
  /***********************************************
		Log
	************************************************/
	public function logout() 
	{
		#code
		session_destroy();
		redirect('Access');
	}

	/***************************************** Interfaces 	***********************************/
    
	  /***********************************************
			E-Journal
		************************************************/
		public function E_Journal() 
		{
			if ( isset($_SESSION['username']) ) 
			{
				# code...
				$title['title'] = "E-Journal";
				
				$this->load->view('header',$title);
				$this->load->view('nav-ej');
				$this->load->view('ejournal');
				$this->load->view('footer');
			
			} 
			else 
			{
				# code...
				//loading login view
				redirect('Access/Login');
			}
		}
    
    /***********************************************
			Dispute Manager
		************************************************/
		public function Dispute_Manager() 
		{
			if ( isset($_SESSION['username']) ) {
				# code...
				$title['title'] = "Dispute Manager";
	            $this->load->model('EventsModel');
				//$data['user'] = $_SESSION['username'];
				$this->load->view('header',$title);
				$this->load->view('dispute');
				$this->load->view('footer');
			
			} else {
				# code...
				//loading login view
				redirect('Access');
			}
		}
    
    /***********************************************
			 ATM Eye
		************************************************/
		public function ATM_Eye() {
			
			//loading register view


			if ( isset($_SESSION['username']) ) { 
				# code... 
				$title['title'] = "ATM Eye";
	            $this->load->model('EventsModel');
				//$data['user'] = $_SESSION['username'];
				$this->load->view('header',$title);

				$this->load->view('atm_eye');

				$this->load->view('footer');
			
			} else {
				# code...
				//loading login view
				redirect('Access');
			}
		}
    
    /***********************************************
				Transactions
		************************************************/
		public function Transaction() {
			
			//loading register view

			if ( isset($_SESSION['username']) ) {
				# code...
				$this->load->model('EventsModel');
				$this->load->model('EventsModel');

				# Data Retrieval
				$data['allatms'] = $this->EventsModel->all_atms();
				$allatminfo = $this->EventsModel->All_Info('full_atminfo');

				foreach($allatminfo As $atmipid) :
					@$display .= $atmipid->AtmName.",".$atmipid->TerminalId.",".$atmipid->TerminalIp.",";
				endforeach;

				$data['atminfo_display'] = explode(',', $display);

				$title['title'] = "Transactions";
				
				$this->load->view('header',$title);
				$this->load->view('nav');
				$this->load->view('transaction',$data);
				$this->load->view('footer');

			
			} else {
				# code...
				//loading login view
				redirect('Access/Login');
			}
		}

		/***********************************************
				 Logs
		************************************************/
		public function Logs() {
			
			//loading register view

			if ( isset($_SESSION['username']) ) {
				# code...
				$title['title'] = "Logs";

				# Loading Model 
				$this->load->model('EventsModel');

				$allatminfo = $this->EventsModel->All_Info('full_atminfo');

				foreach($allatminfo As $atmipid) :
					@$display .= $atmipid->AtmName.",".$atmipid->TerminalId.",".$atmipid->TerminalIp.",";
				endforeach;

				$data['atminfo_display'] = explode(',', $display);


				//$data['user'] = $_SESSION['username'];
				$this->load->view('header',$title);
				$this->load->view('nav');
				$this->load->view('logs',$data);
				$this->load->view('footer');
			
			} else {
				# code...
				//loading login view
				redirect('Access/Login');
			}
		}
   
    /***********************************************
			 User Logs
	************************************************/

	public function User_Logs() {
		
		//loading register view

		if ( isset($_SESSION['username']) ) {
			# code...
			$title['title'] = "User Logs";
			//$data['user'] = $_SESSION['username'];

			//Retrieving Roles
			$this->load->model('UserModel');
			$this->load->model('EventsModel');
			
			$this->load->view('header',$title);
			$this->load->view('userlogs');
			$this->load->view('footer');
		
		} else {
			# code...
			//loading login view
			redirect('Access/Login');
		}
	}
    /***********************************************
			 User Logs
	************************************************/

	public function System_Logs() {
		
		//loading register view

		if ( isset($_SESSION['username']) ) {
			# code...
			$title['title'] = "User Logs";
			//$data['user'] = $_SESSION['username'];

			//Retrieving Roles
			$this->load->model('UserModel');
			$this->load->model('EventsModel');
			
			$this->load->view('header',$title);
			$this->load->view('syslogs');
			$this->load->view('footer');
		
		} else {
			# code...
			//loading login view
			redirect('Access/Login');
		}
	}


	/******************************************************	Data Insertion	****************************************/
    /*******************************
				Create ATM Group
		*******************************/
    public function Create_Group() {

    	if( isset($_POST['create_grp']) && isset($_SESSION['username'])) {
	    	//Setting validation rules
			$this->form_validation->set_rules('group_name','Group Name','required|trim|xss_clean');
			$this->form_validation->set_rules('desc','Description','required|trim|xss_clean');
			
			//Variables Assignment
			$data['GroupName'] 		= $this->input->post('group_name');
			$data['Description'] 	= $this->input->post('desc');

			//load model
			$this->load->model('EventsModel');

			//ATM Group Name Duplicate Search
			$nameresult = $this->EventsModel->dup_atminfo('infoatmgroup','GroupName',$data['GroupName']);

			if ($nameresult) {
				# code...
				$_SESSION['error'] .= "Group Name Already Exists<br>";
			}

			if (!isset($_SESSION['error'])) {
				# code...
				$result = $this->EventsModel->group_create($data);

				if($result) {

                    //Creating Folders in Uploads and Logs
                    $upload_folder = "Uploads/".$data['GroupName']; 
                    $logs_folder = "Logs/". $data['GroupName']; 
                    if(mkdir($upload_folder,'755')) {
                        #Create Logs
                        if(mkdir($logs_folder,'755')) {
                            $_SESSION['success'] = "ATM Group Successfully Created";
                            redirect('Dashboard/ATM_Groups');
                        }
                    }
				
				} else {

					$_SESSION['error'] = "Failed to Create ATM Group";
					redirect('Dashboard/ATM_Groups');
				}
			
			}	else {
				
				redirect('Dashboard/ATM_Groups');
			}
		} 
		 else{
		 	$_SESSION['error'] = "Create Group Command Failed";
			redirect('Dashboard/ATM_Groups');
		}
    
    }

    /*********************************
			 Search Transaction
		**********************************/
    public function Transaction_Search() 
    {
    	if(isset($_POST['search']) && isset($_SESSION['username'])) 
    	{
	    	# Setting validation rules
				$this->form_validation->set_rules('search_text','ATM Name','trim');
				$this->form_validation->set_rules('CardNo','Card Number','required|trim');
				$this->form_validation->set_rules('daterange','Date Range','required|trim');
				$this->form_validation->set_rules('request_type','Request Type','required|trim');

				if($this->form_validation->run() === FALSE) 
	      {
	        $_SESSION['error'] = "All Fields Required";
	        redirect('Dashboard/transaction');
	      }
	      else 
	      {
					# Loading Model
					$this->load->model('EventsModel');

					# Variables Assignment
					$data['request_type'] 	= $this->input->post('request_type');
					$data['search_text'] 	= $this->input->post('search_text');
					$data['hostCardNumber'] = $this->input->post('CardNo');
					$date 								= explode(' to ', $this->input->post('daterange'));
					$data['lower_date']		= $date[0];
					$data['upper_date']		= @$date[1];

					if(!empty($data['search_text'])) 
					{
						$result = $this->EventsModel->terminal_id_ip_retrieve($data);

   	        if(!empty($result))
   	        {
    					$terminalip = $result->terminalip;
    					$data['atmName'] = $result->atmName;
            }
            else 
            	$terminalip = "";
          }    
          else 
            $terminalip = "";
                    
					# Retrieving data from transaction table 
					$result = $this->EventsModel->transaction_retrieve($terminalip,$data);

					if($result) 
					{
						# Variables Assigning
						$_SESSION['result'] 	= $result;
						$_SESSION['name'] 		= $data['search_text'];
						$_SESSION['cardno'] 	= $data['hostCardNumber'];
						$_SESSION['date'] 		= $this->input->post('daterange');
        
						redirect('Dashboard/Transaction');
					} 
					else 
					{
						$_SESSION['warning'] = "No Record Found";
						redirect('Dashboard/transaction');
					}
				}
			} 
			else
			{
			 	unset($_SESSION['name']);
			 	unset($_SESSION['cardno']);
			 	unset($_SESSION['date']);
				redirect('Dashboard/transaction');
			}
    }

    /************************
				Log Search
		*************************/
    public function Log_Search($search_text,$daterange) 
    {
    	if(!empty($search_text) && !empty($daterange))
    	{
	    	# load model
				$this->load->model('EventsModel');

				# Variables Assignment
				$data['search_text'] 	= $search_text;
				$data['request_type'] = "Log";
				$date 					= explode('xtox', $daterange);
				$data['lower_date']		= $date[0];
				$data['upper_date']		= $date[1];

				# Retrieving infoatm for atm Terminal IP & Terminal ID
				$terminal = $this->EventsModel->terminal_id_ip_retrieve($data);

				if(!empty($terminal))
				{
					$terminalip = $terminal->terminalip;

					$result = $this->EventsModel->transaction_retrieve($terminalip,$data);

					if($result) 
						print_r(json_encode($result));
					 
				}

				else
					print FALSE;
			}
    }

    /************************
				Log Search
		*************************/
    public function CardSearch() 
    {
    	$cardnumber = $_GET['query'];
    	
    	if(!empty($cardnumber))
    	{
	    	# load model
				$this->load->model('EventsModel');

				$terminal = $this->EventsModel->ret_cardno($cardnumber);

				if(!empty($terminal))
				{
					foreach ($terminal as $value) {
						echo "<p class='form-control'>" . $value->hostCardNumber . "</p>";
					}
				}
				else
					print "<p class='form-control'>No Suggestion Found</p>";
			}
    }

    /***********************************************
				File Upload
	************************************************/
    
    public function File_Upload() {

    	if (isset($_POST['upload'])) {
			# code...
    		print $_FILES['csvfile']['name'];


			if ($_FILES['csvfile']['size'] > 0 && $_FILES['csvfile']['size'] < 50000) {

				$csv_mimetypes = array('text/csv', 'application/csv', 'text/comma-separated-values', 'application/excel', 'application/vnd.ms-excel', 'application/vnd.msexcel', 'application/txt');

				$filetype = $_FILES['csvfile']['type'];

				if(in_array($filetype,$csv_mimetypes )) {

					$name = explode("_", $_FILES['csvfile']['name']);

					$mainfolder = $name[0];

					if ($mainfolder == "ATMH" && file_exists($mainfolder)) {

						MainLogic:

						$target_dir = $mainfolder."/".gmdate('F Y');
						//$target_dir = ATMH/January 2016

						MonthLogic:

						if (file_exists($target_dir)) {

							$target_dir .= "/"; 
							//$target_dir = ATMH/January 2016/

							$name = explode(".", $_FILES['csvfile']['name']);

							$filename = gmdate('d_m_Y').".".$name[1];

							$target_dir .= $filename;
							//$target_dir = ATMH/January 2016/04_04_2016.csv 

							if(!move_uploaded_file($_FILES['csvfile']['tmp_name'], $target_dir)){
	    				
	    						print "Error uploading file - check destination directory is writeable OR created.')";
							
							} else {
								print "File Uploaded";
								$_SESSION['success'] = "File Uploaded";
								//now call file and read to screen
								echo "<html><body><table border='1'>\n\n";
								$f = fopen($target_dir, "r");
								while (($line = fgetcsv($f)) !== false) {
								        echo "<tr>";
								        foreach ($line as $cell) {
								                echo "<td>" . htmlspecialchars($cell) . "</td>";
								        }
								        echo "</tr>\n";
								}
								fclose($f);
								echo "\n</table></body></html>";
							}

						}	else {

							//creating //$target_dir = ATMH/January 2016
							if(mkdir($target_dir,0755))
								goto MonthLogic;
						
						}

					} elseif ($mainfolder == "BANKH" && file_exists($mainfolder)) {
						# code...

						goto MainLogic;
					
					}	else{

						print "Main Folders Creation Failed";
					}

				}	else {
						
						print "File Type Not Supported.<br>Please Upload a CSV or Excel File ";
						//redirect('');
				}

			} else {
					
					print "File Size Too Big";
					//redirect('');
			}

		}

    }

    /***********************************************
				New Dispute
	************************************************/
    
    public function New_Dispute() {

    	if (isset($_POST['upload'])) {
			# code...
    	    print "its Workiglkin";
		}

    }


    // load media in the atm eye modal
		public function load_atm_media($url){ 

			$this->load->model('EventsModel');

			$media['terminalid']	=	$term = $this->uri->segment(5);

			$media['card_number']	=	base64_decode($term = $this->uri->segment(6));
		        	
        	$media['starttime'] 	= $term = rawurldecode($this->uri->segment(3));//$starttime[$i];
        	
        	$media['endtime']		= $term = rawurldecode($this->uri->segment(4));//$endtime[$i];

        	$media['mediatype'] = $this->session->userdata('mediatype');

        	$data['media_res'] = $this->EventsModel->ret_imgpath_corousol($media);

        	$filter_view = $this->load->view('atm_media_view',$data,True);

        	//echo var_dump($media); exit;
		
		    echo json_encode($filter_view);
		}

    
    
  /***********************************************
			 Search ATM Pics/Video
	************************************************/
  public function ATM_Eye_Search() 
  {
	  if (isset($_SESSION['username'])) 
		{
			// if(isset($_POST['search_image'])) 
			if(1==1) 
	  	{
			  # Setting validation rules
				$this->form_validation->set_rules('atmid','ATM Name','required|trim');
		    $this->form_validation->set_rules('atmtype','ATM Type','required|trim');
		    $this->form_validation->set_rules('mediatype','Media Type','required|trim');
				$this->form_validation->set_rules('CardNo','Card Number','required|trim');
				$this->form_validation->set_rules('daterange','Date Range','required|trim');

				if(trim($this->input->post('mediatype')) == 'Pictures'){
					$this->session->set_userdata('mediatype','stillpictures_path');
			    }
					
				# If Failed
				if ($this->form_validation->run() === FALSE) 
				{
				  # Redirecting With Error
				  $_SESSION['error'] = "All Fields Required";
				  redirect('Dashboard/ATM_Eye');
				}
				    
				# If Passed
				else
				  {
					# Variables Assignment
					$data['AtmId'] = $this->input->post('atmid');
		      
		      if($this->input->post('atmtype') != "All")
		      	$data['Atmtype'] 	= $this->input->post('atmtype');
		     	else
		     		$data['Atmtype'] = "";

		      # Check MEdia type Condition
					$data['HostCardNo'] 	= $this->input->post('CardNo');
					$date 								= explode(' to ', $this->input->post('daterange'));
					$data['lower_date']		= $date[0];
					$data['upper_date']		= @$date[1];

					# load model
					$this->load->model('EventsModel');
		                    
					# Retrieving data from transaction table 
					$result = $this->EventsModel->ret_time_form_report($data); 

					if($result) 
		      {
						foreach($result As $time)
		        {
		          $starttime[] 	= $time->AtmSTime;
		          $endtime[] 		= $time->AtmETime;
		        }

		        for ($i=0; $i < sizeof($starttime) ; $i++) 
		        { 
		        	# code...
		        	if ($this->input->post('mediatype') == "Pictures") 
		        		
		        		$media['mediatype']	= "stillpictures_path";
		        	elseif ($this->input->post('mediatype') == "video")
		        		
		        		//$media['mediatype']	= "motionpictures_path";
		        	
		        	$media['mediatype']	= "stillpictures_path";
		        	
		        	$media['terminalid']	=	$data['AtmId'];
		        	
		        	$media['starttime'] 	= $data['lower_date'];//$starttime[$i];
		        	
		        	$media['endtime']		= $data['upper_date'];//$endtime[$i];

		        	$media_res[$i] = $this->EventsModel->ret_imgpath($media);
//var_dump($media); var_dump($media_res[$i]);exit('here');
		        	
		        }

		        $oth_rev_info['terminalid'] = $data['AtmId'];

		        $oth_rev_info['CardNo'] = $data['HostCardNo'];

		        $oth_rev_info['mediatype'] = $this->input->post('mediatype');
		        
		        $this->session->set_flashdata('trans_result',$result);

		        $this->session->set_flashdata('oth_rev_info',$oth_rev_info);

		        $this->session->set_flashdata('media_result',$media_res);
		        
		        /*print_r($this->session->flashdata('trans_result'));
		        print "<br/><br/><br/>";
		        print_r($this->session->flashdata('oth_rev_info'));
		        print "<br/><br/><br/>";
		        foreach($media_res As $loop_one)
		        {
		        	foreach($loop_one As $loop_two)
		        	{
		        		print_r($loop_two->stillpictures_path);
		        		print "<br/>";
		        	}
		        	print "<br/><br/><br/>";
		        }*/
		        redirect('Dashboard/ATM_Eye');
					} 
		      else 
		      {
			      $_SESSION['error'] = "No Records Found";
			      redirect('Dashboard/ATM_Eye');
					}
				}        
			}
  		# End of Post
		  else
		  {
		  	$_SESSION['error'] = "Permission Denied. Contact Admin";
		  	redirect('Dashboard/ATM_Eye');
		  }
		}
		else 
		{
			redirect('Access');
		}
  }

    /******************************************************	Data Update	****************************************/

    /***********************************************
				ATM Group Update
	************************************************/
    
    public function ATM_Group_Update() {

    	if( isset($_POST['update_atm_group']) ) {
	    	//Setting validation rules
			$this->form_validation->set_rules('group_name','Group Name','required|trim|xss_clean');
			$this->form_validation->set_rules('desc','Description','required|trim|xss_clean');
			$this->form_validation->set_rules('atmGroupId','Description','required|trim|xss_clean');
            $this->form_validation->set_rules('oldFolder','Old Folder Names','required|trim|xss_clean');
			
			//Variables Assignment
			$data['GroupName'] 		= $this->input->post('group_name');
			$data['Description'] 	= $this->input->post('desc');
			$data['atmGroupId'] 	= $this->input->post('atmGroupId');
            $folder_name            = $this->input->post('oldFolder');

			//load model
			$this->load->model('EventsModel');
			$result = $this->EventsModel->group_update($data);

			if($result) {
			     $_SESSION['success'] = "ATM Group Update successful";
                 
			     #Checking if Name was Changed
                 if(strcasecmp($folder_name,$data['GroupName']) != 0) {
                    #Checking for existence of file and updating 
                    $old_uploads = "Uploads/".$folder_name;
                    $old_logs = "Logs/".$folder_name;
                    $new_uploads = "Uploads/".$data['GroupName']; 
                    $new_logs = "Logs/". $data['GroupName']; 
                    if(file_exists($upload_folder)) {
                        #Create Logs
                        if(file_exists($logs_folder)) {
                            //rename file codes
                            if(rename($old_uploads,$new_uploads) && rename($old_logs,$new_logs))
                            $_SESSION['success'] .= "<br>Folders Renamed";
                        }
                    }
                }
                
                redirect('Dashboard/ATM_Manage_Groups');
			
			} else {

				$_SESSION['error'] = "ATM Group Update Failed";
				redirect('Dashboard/ATM_Manage_Groups');
			}
		} 
		 else{
		 	$_SESSION['error'] = "Update Group Command Failed";
			redirect('Dashboard/ATM_Add');
		}
    }
    
    /***********************************************
				ATM Group Update
	************************************************/
 

    /************************************	Data Delete	****************************************/

    /************************************	Data Delete	****************************************/

}//End of Class
