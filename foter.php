<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h2>Log Search</h2></br> <hr/>
            <div class="clearfix"></div>
            <form class="form-horizontal form-label-left" action="#" method="post">
              <div class="row">
                <div class="col-md-4">
                  <div class="item form-group">
                    <label  for="name">ATM Name <span class="required">*</span></label>
                      <input class="form-control txtbxvisible"  name="search_text" placeholder="ATM Name / Terminal ID / Termianl IP" type="text" value="<?php print @$_SESSION['name']; ?>" required>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="item form-group">
                    <label  for="name">Date <span class="required">*</span></label>
                    <div class="input-prepend input-group" >
                      <span class="add-on input-group-addon" style="background: none;"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                      <input type="text" name="daterange" class="form-control twowaydate" required value="<?php print @$_SESSION['date']; ?>">
                    </div>
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="item form-group">
                    <label  for="name"> <span class="required">*</span></label>
                    <div class="input-prepend input-group" >
                      <input id="log_retrieve" class="btn btn-success" type="button" value="Submit Query" onclick="LogRetrieve()">
                    </div>
                  </div>   
                </div>
              </div>
              <input type="hidden" name="request_type" value="Log">  
            </form>
          <div class="clearfix"></div>
        </div>
        
        <?php if(!empty($_SESSION['retrieve_logs'])) { $result = $_SESSION['retrieve_logs']; print($result); ?>;

        <div class="x_content">
          <table id="datatable-buttons logstbl" class="table table-striped table-bordered jambo_table" style="font-size: 14px;" >
            <thead>
              <tr class="headings">
                <th><input type="checkbox" class="tableflat"></th>
                <th>Start Time </th>
                <th>End Time </th>
                <th>Card Number </th>
                <th class=" no-link last"><span class="nobr">Action</span>
                </th>
              </tr>
            </thead>
            <tbody>
            
            <?php $counter = 1; foreach ($result as $value) { ?>

              <tr class="even pointer">
                <td class="a-center "><input type="checkbox" class="tableflat"></td>
                <td class=""><?php print $value->hostStartTime; ?></td>
                <td class=""><?php print $value->hostEndTime; ?></td>
                <td class="">
                  <?php
                    if(!empty($value->hostCardNumber))
                      
                  ?>
                </td>
                <td style="padding:6px">
                  <a href="" data-toggle="modal" data-target=".View<?php print $counter?>" title="Edit">
                    <button class="btn btn-primary" style="margin:0px;padding:3px;font-size:12px">
                      <i class="fa fa-pencil"></i> View
                    </button>
                  </a>
                </td>
              </tr>

            <?php $counter++; } ?>

            </tbody>
          </table>
        </div>
        
        <?php } ?>

        <!-- **************** View Details Modal ********************** -->
        <div id="viewlogmodal" class="modal fade View" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog modal-sm">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="myModalLabel2">Electronic Journal</h4>
              </div>
              <div class="modal-body">
                <form enctype="multipart/form-data">
                  
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <br/><br/><br/>
    </div>
  </div>
  <!-- footer content -->













    
    <!-- gauge js -->
    <script type="text/javascript" src="<?php print base_url(); ?>resources/js/gauge/gauge.min.js"></script>
    

    <!--  Other Javascript Functions -->
    <script type="text/javascrip"> 
      function change(obj) 
      {
        var selectBox = document.getElementById("comparism");
        var selected = selectBox.options[selectBox.selectedIndex].value;
            
        if(selected === 'atm'){
            $('#atmfield').css("display","block");
            $('#bankfield').css("display","none");
            $('#uploadnotice').css("display","none");
            /*Activating Modal*/
        }
        else if(selected === 'bank'){
            $('#bankfield').css("display","block");
            $('#atmfield').css("display","none");
            $('#uploadnotice').css("display","none");
        }
        else {
            $('#atmfield').css("display","none");
            $('#bankfield').css("display","none");
            $('#uploadnotice').css("display","block");
        }
      }
    </script>
    <script src="<?php print base_url(); ?>resources/js/addedjsfunc.js"></script>
    <!--  Other Javascript Functions -->

    print substr_replace($value->hostCardNumber, str_repeat("*", 8), 4, 8);













    <script type="text/javascript" src="<?php print base_url(); ?>resources/js/gauge/gauge_demo.js"></script>
    
    <!-- flot js -->
    <script> 
      $(function () {
        $(".files").on('change', function () {
            var selectedValue = $(this).val();
            var otherDropdowns = $('.files').not(this);
            otherDropdowns.find('option').prop('disabled', false); //reset all previous selection
            otherDropdowns.find('option[value=' + selectedValue + ']').prop('disabled', true);
        });
      });
      
      $("select.files").change(function () {
    $("select.files option[value='" + $(this).data('index') + "']").prop('disabled', false); //reset others on change everytime
    $(this).data('index', this.value);
    $("select.files option[value='" + this.value + "']:not([value=''])").prop('disabled', true);
    $(this).find("option[value='" + this.value + "']:not([value=''])").prop('disabled', false); // Do not apply the logic to the current one
}); 
   </script>


     
     <!-- ########################################## Date Range Picker ##################################### -->
    
    
    <script>
      $(document).ready(function () 
      {
                $('#demo').daterangepicker({
    "showDropdowns": true,
    "autoApply": true,
    "timePicker": true,
    "timePicker24Hour": true,
    "timePickerSeconds": true,
    "locale": {
        "format": "YYYY-MM-DD HH:mm:ss",
        "separator": " To ",
        "applyLabel": "Apply",
        "cancelLabel": "Cancel",
        "fromLabel": "From",
        "toLabel": "To",
        "customRangeLabel": "Custom",
        "daysOfWeek": [
            "Su",
            "Mo",
            "Tu",
            "We",
            "Th",
            "Fr",
            "Sa"
        ],
        "monthNames": [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"
        ],
        "firstDay": 1
    },
    "linkedCalendars": false,
    "alwaysShowCalendars": true
    }, function(start, end, label) {
      console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
    });
                $('#2waydate').daterangepicker({
    "showDropdowns": true,
    "autoApply": true,
    "timePicker": true,
    "timePicker24Hour": true,
    "timePickerSeconds": true,
    "locale": {
        "format": "YYYY-MM-DD HH:mm:ss",
        "separator": " To ",
        "applyLabel": "Apply",
        "cancelLabel": "Cancel",
        "fromLabel": "From",
        "toLabel": "To",
        "customRangeLabel": "Custom",
        "daysOfWeek": [
            "Su",
            "Mo",
            "Tu",
            "We",
            "Th",
            "Fr",
            "Sa"
        ],
        "monthNames": [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"
        ],
        "firstDay": 1
    },
    "linkedCalendars": false,
    "alwaysShowCalendars": true
}, function(start, end, label) {
  console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
});
                $('input[name="daterange"]').daterangepicker({
                    locale: {
                        format: 'YYYY-MM-DD',
                        "separator": " to ",
                        "applyLabel": "Apply",
                        "cancelLabel": "Cancel",
                        "fromLabel": "From",
                        "toLabel": "To",
                        "customRangeLabel": "Custom",
                        "daysOfWeek": [
                            "Su",
                            "Mo",
                            "Tu",
                            "We",
                            "Th",
                            "Fr",
                            "Sa"
                        ],
                        "monthNames": [
                            "January",
                            "February",
                            "March",
                            "April",
                            "May",
                            "June",
                            "July",
                            "August",
                            "September",
                            "October",
                            "November",
                            "December"
                        ],
                        "firstDay": 1,
                        
                    },
                    "linkedCalendars": false,
                    "showDropdowns": true,
                    "autoApply": true
                });
                $('#multiple').daterangepicker({
    "showDropdowns": true,
    "autoApply": true,
    "locale": {
        "format": "YYYY-MM-DD",
        "separator": " To ",
        "applyLabel": "Apply",
        "cancelLabel": "Cancel",
        "fromLabel": "From",
        "toLabel": "To",
        "customRangeLabel": "Custom",
        "daysOfWeek": [
            "Su",
            "Mo",
            "Tu",
            "We",
            "Th",
            "Fr",
            "Sa"
        ],
        "monthNames": [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"
        ],
        "firstDay": 1
    },
    "linkedCalendars": false,
    "alwaysShowCalendars": true
}, function(start, end, label) {
  console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
});
            $('#cantrans').daterangepicker({
    "singleDatePicker": true,
    "showDropdowns": true,
    "showWeekNumbers": true,
    "autoApply": true,
    "locale": {
        "format": "YYYY-MM-DD",
        "separator": " - ",
        "applyLabel": "Apply",
        "cancelLabel": "Cancel",
        "fromLabel": "From",
        "toLabel": "To",
        "customRangeLabel": "Custom",
        "daysOfWeek": [
            "Su",
            "Mo",
            "Tu",
            "We",
            "Th",
            "Fr",
            "Sa"
        ],
        "monthNames": [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"
        ],
        "firstDay": 1
    },
    "linkedCalendars": false,
    "opens": "left"
}, function(start, end, label) {
  console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
});
            $('#credit').daterangepicker({
    "singleDatePicker": true,
    "showDropdowns": true,
    "showWeekNumbers": true,
    "autoApply": true,
    "locale": {
        "format": "YYYY-MM-DD",
        "separator": " - ",
        "applyLabel": "Apply",
        "cancelLabel": "Cancel",
        "fromLabel": "From",
        "toLabel": "To",
        "customRangeLabel": "Custom",
        "daysOfWeek": [
            "Su",
            "Mo",
            "Tu",
            "We",
            "Th",
            "Fr",
            "Sa"
        ],
        "monthNames": [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"
        ],
        "firstDay": 1
    },
    "linkedCalendars": false,
    "opens": "left"
}, function(start, end, label) {
  console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
});
$('#atm').daterangepicker({
    "singleDatePicker": true,
    "showDropdowns": true,
    "showWeekNumbers": true,
    "autoApply": true,
    "locale": {
        "format": "YYYY-MM-DD",
        "separator": " - ",
        "applyLabel": "Apply",
        "cancelLabel": "Cancel",
        "fromLabel": "From",
        "toLabel": "To",
        "customRangeLabel": "Custom",
        "daysOfWeek": [
            "Su",
            "Mo",
            "Tu",
            "We",
            "Th",
            "Fr",
            "Sa"
        ],
        "monthNames": [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"
        ],
        "firstDay": 1
    },
    "linkedCalendars": false,
    "opens": "left"
}, function(start, end, label) {
  console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
});
$('#atmdis').daterangepicker({
    "singleDatePicker": true,
    "showDropdowns": true,
    "showWeekNumbers": true,
    "autoApply": true,
    "locale": {
        "format": "YYYY-MM-DD",
        "separator": " - ",
        "applyLabel": "Apply",
        "cancelLabel": "Cancel",
        "fromLabel": "From",
        "toLabel": "To",
        "customRangeLabel": "Custom",
        "daysOfWeek": [
            "Su",
            "Mo",
            "Tu",
            "We",
            "Th",
            "Fr",
            "Sa"
        ],
        "monthNames": [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"
        ],
        "firstDay": 1
    },
    "linkedCalendars": false,
    "opens": "left"
}, function(start, end, label) {
  console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
});
            });
            
        </script>
    
    

    
    
     <!-- ########################################## Process Done ##################################### -->
    <script>
        NProgress.done();
    </script>
    <!-- /datepicker -->

     <!-- ########################################## Form Validation ######################## -->
    
    
   
   
    
     <!-- ########################################## Notification ##################################### -->
    <!-- echart -->
    <script src="<?php print base_url(); ?>resources/js/echart/echarts-all.js"></script>
    <script src="<?php print base_url(); ?>resources/js/echart/green.js"></script>
    <script>
        var myChart = echarts.init(document.getElementById('echart_pie'), theme);
        myChart.setOption({
            tooltip: {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            legend: {
                orient: 'vertical',
                //x: 'left',
                x: 'right',
                y: 'top',
                data: ['Accra', 'Tema',  'Eastern', 'Western', 'Central','Northern','Volta', 'Ashanti', 'Brong Ahafo', 'Upper East', 'Upper West']
            },
            toolbox: {
                show: true,
                x:'left',
                y: 'bottom',
                feature: {
                    restore: {
                        show: true
                    },
                    saveAsImage: {
                        show: true
                    }
                }
            },
            calculable: true,
            series: [
                {
                    name: 'ATM Group',
                    type: 'pie',
                    radius: '50%',
                    center: ['40%', '40%'], //left,top
                    data: [
                        <?php
                            if(!empty($total_ga)) {
                                print "
                                    {
                                        value: $total_ga,
                                        name: 'Accra'
                                    }
                                ";
                            }
                            if(!empty($total_tm)) {
                                print "
                                    ,{
                                        value: $total_tm,
                                        name: 'Tema'
                                    }
                                ";
                            }
                            if(!empty($total_er)) {
                                print "
                                    ,{
                                        value: $total_er,
                                        name: 'Eastern'
                                    }
                                ";
                            }
                            if(!empty($total_wr)) {
                                print "
                                    ,{
                                        value: $total_wr,
                                        name: 'Western'
                                    }
                                ";
                            }
                            if(!empty($total_cr)) {
                                print "
                                    ,{
                                        value: $total_cr,
                                        name: 'Central'
                                    }
                                ";
                            }
                            if(!empty($total_nr)) {
                                print "
                                    ,{
                                        value: $total_nr,
                                        name: 'Northern'
                                    }
                                ";
                            }
                            if(!empty($total_vr)) {
                                print "
                                    ,{
                                        value: $total_vr,
                                        name: 'Volta'
                                    }
                                ";
                            }
                            if(!empty($total_ash)) {
                                print "
                                    ,{
                                        value: $total_ash,
                                        name: 'Ashanti'
                                    }
                                ";
                            }
                            if(!empty($total_ba)) {
                                print "
                                    ,{
                                        value: $total_ba,
                                        name: 'Brong Ahafo'
                                    }
                                ";
                            }
                            if(!empty($total_ue)) {
                                print "
                                    ,{
                                        value: $total_ue,
                                        name: 'Upper East'
                                    }
                                ";
                            }
                            if(!empty($total_uw)) {
                                print "
                                    ,{
                                        value: $total_uw,
                                        name: 'Upper West'
                                    }
                                ";
                            }
                        ?>
                ]
            }
        ]
        });


        var dataStyle = {
            normal: {
                label: {
                    show: false
                },
                labelLine: {
                    show: false
                }
            }
        };
        var placeHolderStyle = {
            normal: {
                color: 'rgba(0,0,0,0)',
                label: {
                    show: false
                },
                labelLine: {
                    show: false
                }
            },
            emphasis: {
                color: 'rgba(0,0,0,0)'
            }
        };
    </script>
     <!-- ########################################## Ajax ##################################### -->
    
    
    <!-- flot -->
    


















    
                
